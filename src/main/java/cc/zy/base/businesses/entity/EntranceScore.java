package cc.zy.base.businesses.entity;


import com.wuwenze.poi.annotation.Excel;
import com.wuwenze.poi.annotation.ExcelField;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 *  Entity
 *
 * @author Jiangjinlin
 * @date 2021-01-23 23:11:18
 */
@Data
@TableName("t_entrance_score")
@Excel("用户信息表")
public class EntranceScore {

    /**
     * 
     */
    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;
    /**
     *
     */
    @TableField("BATCH_NAME")
    @ExcelField(value = "批次")
    private String batchName;
    /**
     *
     */
    @TableField("STU_NAME")
    @ExcelField(value = "姓名")
    private String stuName;
    /**
     * 
     */
    @TableId(value = "IDENTITY", type = IdType.AUTO)
    @ExcelField(value = "身份证号")
    private String identity;

    /**
     * 
     */
    @TableId(value = "BATCH_ID", type = IdType.AUTO)
    private String batchId;

    /**
     * 
     */
    @TableId(value = "STUDY_TYPE_ID", type = IdType.AUTO)
    private String studyTypeId;

    /**
     * 
     */
    @TableField("SUBTYPE_ID")
    private Integer subtypeId;


    /**
     * 
     */
    @TableField("SUBJECT_ID")
    private Integer subjectId;
    /**
     *
     */
    @TableField("collegeName")
    @ExcelField(value = "院校")
    private String collegeName;
    /**
     *
     */
    @TableField("ALIAS")
    private String alias;
    /**
     *
     */
    @TableField("NUM")
    private Integer num;
    /**
     * 
     */
    @TableField("SCORE")
    private Integer score;

    /**
     * 
     */
    @TableField("STATUS")
    private String status;
    /**
     *名称字段
     */
    @TableField("name")
    private String name;

    /**
     *学院ID
     */
    @TableField("COLLEGE_ID")
    private Integer collegeId;
    /**
     *层次ID
     */
    @TableField("LEVEL")
    private Integer levelId;
    /**
     *层次名
     */
    @TableField("levelName")
    @ExcelField(value = "层次")
    private String levelName;

    /**
     *科目类型
     */
    @TableField("FULL_NAME")
    @ExcelField(value = "科目类型")
    private String fullName;
    /**
     * 总成绩是否合格(1为合格，2为不合格)
     */
    @ExcelField(value = "考试状态")
    private String isPass;
    /*****************************************/
    /**
     *政治
     */
    @TableField("o")
    @ExcelField(value = "政治")
    private String o;
    /**
     *大学语文
     */
    @TableField("chinese")
    @ExcelField(value = "大学语文")
    private String chinese;
    /**
     *高数一
     */
    @TableField("b")
    @ExcelField(value = "高数一")
    private String b;
    /**
     *高数二
     */
    @TableField("c")
    @ExcelField(value = "高数二")
    private String c;
    /**
     *民法
     */
    @TableField("d")
    @ExcelField(value = "民法")
    private String d;
    /**
     *教育理论
     */
    @TableField("e")
    @ExcelField(value = "教育理论")
    private String e;
    /**
     *生态学基础
     */
    @TableField("f")
    @ExcelField(value = "生态学基础")
    private String f;
    /**
     *英语(高起专)
     */
    @TableField("g")
    @ExcelField(value = "英语(高起专)")
    private String g;
    /**
     *艺术概论
     */
    @TableField("h")
    @ExcelField(value = "艺术概论")
    private String h;
    /**
     *数学(文)
     */
    @TableField("i")
    @ExcelField(value = "数学(文)")
    private String i;
    /**
     *数学(理)
     */
    @TableField("j")
    @ExcelField(value = "数学(理)")
    private String j;
    /**
     *史地综合
     */
    @TableField("k")
    @ExcelField(value = "史地综合")
    private String k;
    /**
     *理化综合
     */
    @TableField("l")
    @ExcelField(value = "理化综合")
    private String l;
    /**
     *术科
     */
    @TableField("m")
    @ExcelField(value = "术科")
    private String m;
    /**
     *英语(专升本)
     */
    @TableField("n")
    @ExcelField(value = "英语(专升本)")
    private String n;

    /**
     *医学综合
     */
    @TableField("p")
    @ExcelField(value = "医学综合")
    private String p;
    /**
     *语文
     */
    @TableField("q")
    @ExcelField(value = "语文")
    private String q;

}
