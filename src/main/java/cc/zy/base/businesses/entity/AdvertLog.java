package cc.zy.base.businesses.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AdvertLog {
    @TableField("ID")
    private Integer id;
    @TableField("ADVERT_ID")
    private Integer adverId;
    @TableField("USER_ID")
    private Integer userId;
    /**
     * 点击广告的用户
     */
    private String stuName;
    @TableField("ENTER_TIME")
    private Timestamp enterTime;
    @TableField("EXIT_TIME")
    private Timestamp exitTime;
    /**
     * 广告标题
     */
    private String title;
    /**
     * 做广告的公司
     */
    private String company;
    /**
     * 广告创建时间
     */
    private Timestamp createTime;
    /**
     * 创建广告的用户id
     */
    private Integer createUserId;
    /**
     * 创建广告的人
     */
    private String userName;

    /**
     * 阅读时间
     */
    private Long time;

}
