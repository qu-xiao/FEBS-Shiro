package cc.zy.base.businesses.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 *  Entity
 *
 * @author Jiangjinlin
 * @date 2021-01-26 12:01:27
 */
@Data
@TableName("t_batch")
public class Batch {

    /**
     * 批次主键
     */
    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;

    /**
     * 批次名称
     */
    @TableField("BATCH_NAME")
    private String batchName;

    /**
     * 批次状态
     */
    @TableField("STATUS")
    private String status;

}
