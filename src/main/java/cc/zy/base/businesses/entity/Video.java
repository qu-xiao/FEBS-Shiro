package cc.zy.base.businesses.entity;

import java.util.Date;

import lombok.Data;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 *  Entity
 *
 * @author Jiangjinlin
 * @date 2021-01-23 16:12:40
 */
@Data
@TableName("c_video")
public class Video {

    /**
     *
     */
    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;

    /**
     *
     */
    @TableField("SORT")
    private Integer sort;

    /**
     *
     */
    @TableField("NAME")
    private String name;

    /**
     *
     */
    @TableField("VIDEO_URL")
    private String videoUrl;

    /**
     *
     */
    @TableField("IMG_URL")
    private String imgUrl;

    /**
     *
     */
    @TableField("INFO")
    private String info;

    /**
     *
     */
    @TableField("CREATE_USER_ID")
    private Integer createUserId;

    /**
     *
     */
    @TableField("CREATE_TIME")
    private Date createTime;

    /**
     *
     */
    @TableField("UPDATE_USER_ID")
    private Integer updateUserId;

    /**
     *
     */
    @TableField("UPDATE_TIME")
    private Date updateTime;

    /**
     *
     */
    @TableField("STATUS")
    private Integer status;
    private String typeName;
    private String updateName;
    private String courseName;
    private Integer typeId;
    private Integer courseId;
}
