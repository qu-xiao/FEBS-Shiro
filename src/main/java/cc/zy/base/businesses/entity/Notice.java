package cc.zy.base.businesses.entity;

import java.util.Date;

import com.wuwenze.poi.annotation.ExcelField;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * 通知表 Entity
 *
 * @author Jiangjinlin
 * @date 2021-01-26 11:52:14
 */
@Data
@TableName("t_notice")
public class Notice {

    /**
     * 主键
     */
    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;

    /**
     * 标题
     */
    @TableField("TITLE")
    private String title;

    /**
     * 创建人id
     */
    @TableField("CREATE_USER_ID")
    private Integer createUserId;

    /**
     * 创建时间
     */
    @TableField("CREATE_TIME")
    private Date createTime;

    /**
     * 通知内容
     */
    @TableField("CONTENT")
    private String content;

    /**
     * 通知封面图片
     */
    @TableField("IMG_URL")
    private String imgUrl;

    /**
     * 视频
     */
    @TableField("VIDEO_URL")
    private String videoUrl;

    /**
     * 文件
     */
    @TableField("FILE")
    private String file;

    /**
     * 状态,参考t_dic,通知状态，草稿，已发布
     */
    @TableField("STATUS")
    private String status;

    /**
     * 课程ID
     */
    @TableField("CLASS_ID")
    private String classId;

    /**
     * 班级唯一编码
     */
    @ExcelField(value = "班级唯一编码")
    @TableField(exist = false)
    private String className;

    /**
     * 创建人
     */
    @ExcelField(value = "创建人")
    @TableField(exist = false)
    private String UserName;


    /**
     * 批次
     */
    @ExcelField(value = "批次")
    @TableField(exist = false)
    private String batchId;

    /**
     * 院校名字
     */
    @ExcelField(value = "院校")
    @TableField(exist = false)
    private String collegeName;

    /**
     * 专业名字
     */
    @ExcelField(value = "院校")
    @TableField(exist = false)
    private String majorName;

    /**
     * 层次名字
     */
    @ExcelField(value = "层次")
    @TableField(exist = false)
    private String levelName;

}
