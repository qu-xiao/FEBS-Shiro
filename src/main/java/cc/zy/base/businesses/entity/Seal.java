package cc.zy.base.businesses.entity;


import com.wuwenze.poi.annotation.ExcelField;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 *  Entity
 *
 * @author Jiangjinlin
 * @date 2021-01-27 09:56:39
 */
@Data
@TableName("t_seal")
public class Seal {

    /**
     * 
     */
    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;

    /**
     * 
     */
    @TableField("COLLEGE_ID")
    private Integer collegeid;

    /**
     * 
     */
    @TableField("IMG_URL")
    private String imgurl;



}
