package cc.zy.base.businesses.entity;

import java.util.Date;

import lombok.Data;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * 现场确认地点 Entity
 *
 * @author Jiangjinlin
 * @date 2021-01-26 14:24:49
 */
@Data
@TableName("t_confirm_address")
public class ConfirmAddress {

    /**
     * 主键id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 省
     */
    @TableField("province")
    private String province;

    /**
     * 市
     */
    @TableField("city")
    private String city;

    /**
     * 区
     */
    @TableField("area")
    private String area;

    /**
     * 详细地点
     */
    @TableField("specific_location")
    private String specificLocation;

    /**
     * 状态
     */
    @TableField("status")
    private String status;

}
