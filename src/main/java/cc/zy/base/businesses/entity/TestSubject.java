package cc.zy.base.businesses.entity;

import java.io.Serializable;
import lombok.Data;

/**
 * t_test_subject
 * @author 
 */
@Data
public class TestSubject implements Serializable {
    private Integer id;

    private String subject;

    private String alias;

    private static final long serialVersionUID = 1L;
}