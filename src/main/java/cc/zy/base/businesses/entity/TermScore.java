package cc.zy.base.businesses.entity;


import lombok.Data;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 *  Entity
 *
 * @author Jiangjinlin
 * @date 2021-01-28 22:44:33
 */
@Data
@TableName("t_term_score")
public class TermScore {

    /**
     * 主键自增
     */
    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;

    /**
     * 身份证号
     */
    @TableField("IDENTITY")
    private String identity;

    /**
     * 批次ID
     */
    @TableField("BATCH_ID")
    private String batchId;
    /**
     * 批次名
     */
    @TableField("BATCH_NAME")
    private String batchName;
    /**
     * 学生姓名
     */
    @TableField("STU_NAME")
    private String stuName;
    /**
     * 学校名
     */
    @TableField("collegeName")
    private String collegeName;
    /**
     * 学校名
     */
    @TableField("collegeId")
    private String collegeId;
    /**
     * 层次
     */
    @TableField("LEVEL_NAME")
    private String levelName;
    /**
     * 层次id
     */
    @TableField("levelId")
    private String levelId;
    /**
     * 专业名
     */
    @TableField("majorName")
    private String majorName;
    /**
     * 专业名Id
     */
    @TableField("majorId")
    private String majorId;
    /**
     * 学期
     */
    @TableField("termName")
    private String termName;
    /**
     * 电话
     */
    @TableField("TEL")
    private String tel;

    /**
     * 学习形式
     */
    @TableField("STUDY_TYPE_ID")
    private String studyTypeId;

    /**
     * 入学考试科目类别ID
     */
    @TableField("SUBTYPE_ID")
    private Integer subtypeId;

    /**
     * 学期id
     */
    @TableField("TERM_ID")
    private Integer termId;

    /**
     * 课程名称
     */
    @TableField("COURSE_NAME")
    private String courseName;

    /**
     * 
     */
    @TableField("ISEXAM")
    private String isexam;

    /**
     * 课程成绩
     */
    @TableField("SCORE")
    private Integer score;

    /**
     * 考试状态
     */
    @TableField("STATUS")
    private String status;

}
