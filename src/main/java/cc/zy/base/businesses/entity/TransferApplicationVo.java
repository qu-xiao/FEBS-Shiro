package cc.zy.base.businesses.entity;


import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * 学籍异动申请表 Entity
 *
 * @author Liuheng
 * @date 2021-01-25 16:13:27
 */
@Data
public class TransferApplicationVo  {

    /**
     *
     */

    private Integer id;

    /**
     * 班主任id
     */
    private Long userId;
    /**
     * 班主任姓名
     */
    private String userName;
    /**
     * 学生id
     */
    private Integer stuId;
    /**
     * 学籍异动类型id
     */
    private Integer transferTypeId;
    /**
     * 休学年限
     */
    private Integer yearLimit;
    /**
     * 复学后新批次名称
     */
    private String newBatchName;
    /**
     * 更换的新学习形式名称
     */
    private String newStudyTypeName;
    /**
     * 更换的新专业名称
     */
    private String newMajorName;
    /**
     * 申请时间
     */
    private Date applicationTime;
    /**
     * 审批意见
     */
    private String opinion;

    /**
     * 审批状态id
     */

    private Integer approvalStatusId;

    /**
     * 审批时间
     */

    private Date approvalTime;

    /**
     * 异动申请原因id
     */

    private Integer applicationReasonId;

    /**
     * 其他原因的补充说明
     */

    private String otherReason;
    /**
     *附件
     */
    private List<String> attachmentList;
    /**
     *
     */

    private String identity;

    /**
     *
     */

    private Integer level;

    /**
     *
     */

    private String stuNum;

    /**
     *
     */

    private Integer collegeId;

    /**
     *
     */

    private Integer batchId;

    /**
     *
     */

    private Integer majorId;

    /**
     *
     */
    private Integer studyTypeId;
    /**
     *学习形式
     */
    private String studyType;

    /**
     *
     */

    private String classId;

    /**
     *
     */

    private String stuName;

    /**
     *
     */

    private Integer sexId;

    /**
     *
     */

    private String nation;

    /**
     *
     */

    private Integer nativeCity;

    /**
     *
     */

    private Integer nativeDistrict;

    /**
     *
     */

    private Integer nativeProvince;

    /**
     *
     */

    private Integer politicsId;

    /**
     *
     */

    private String diplomaNum;

    /**
     *
     */

    private Date graduDate;

    /**
     *
     */

    private Date enrolDate;

    /**
     *
     */

    private String tel;

    /**
     *
     */

    private String urgencyTel;

    /**
     *
     */

    private String email;

    /**
     *
     */

    private String address;

    /**
     *
     */

    private Integer examLocationId;

    /**
     *
     */

    private Date idenBeginDate;

    /**
     *
     */

    private Date idecEndDate;

    /**
     *
     */

    private Integer wxAppOpenid;

    /**
     *
     */

    private Integer statusId;

    /**
     *
     */

    private Integer stageId;

    /**
     *
     */

    private Integer tracsaction;

    /**
     *
     */

    private Integer allowEssay;

    /**
     *
     */

    private String diplomaImgUrl;

    /**
     *
     */

    private String headImgUrl;

    /**
     *
     */

    private String idFrontImgUrl;

    /**
     *
     */

    private String idBackImgUrl;

    /**
     *
     */

    private String examStuNum;

    /**
     *
     */

    private String examRegritNum;
    /**
     *批次名称
     */

    private String batchName;
    /**
     *院校简称
     */

    private String collegeSimpleName;
    /**
     *层次名称
     */

    private String levelName;
    /**
     *专业简称
     */

    private String majorSimpleName;
}
