package cc.zy.base.businesses.entity;

import java.util.Date;

import com.wuwenze.poi.annotation.ExcelField;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 *  Entity
 *
 * @author lijian
 * @date 2021-01-25 16:12:53
 */
@Data
@TableName("t_advert")
public class Advert {

    /**
     * 
     */
    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;

    /**
     * 
     */
    @TableField("TITLE")
    private String title;

    /**
     * 
     */
    @TableField("COMPANY")
    private String company;

    /**
     * 
     */
    @TableField("CREATE_USER_ID")
    private Integer createUserId;

    /**
     * 
     */
    @TableField("CREATE_TIME")
    private Date createTime;

    /**
     * 
     */
    @TableField("IMG_URL")
    private String imgUrl;

    /**
     * 
     */
    @TableField("CONTENT")
    private String content;

    /**
     * 
     */
    @TableField("VIDEO_URL")
    private String videoUrl;

    /**
     * 
     */
    @TableField("STATUS_ID")
    private Integer statusId;

    /**
     * 创建人名称
     */
    @ExcelField(value = "创建人")
    @TableField(exist = false)
    private String userName;


}
