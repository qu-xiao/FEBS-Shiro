package cc.zy.base.businesses.entity;


import lombok.Data;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 *  Entity
 *
 * @author Jiangjinlin
 * @date 2021-01-25 16:06:07
 */
@Data
@TableName("t_req_result_extension")
public class ReqResultExtension {

    /**
     * 
     */
    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;

    /**
     * 
     */
    @TableField("REQ_INFO_ID")
    private Integer reqInfoId;

    /**
     * 
     */
    @TableField("REQ_RESULT_ID")
    private Integer reqResultId;

    /**
     * 
     */
    @TableField("EX1")
    private String ex1;

    /**
     * 
     */
    @TableField("EX2")
    private String ex2;

    /**
     * 
     */
    @TableField("EX3")
    private Integer ex3;

    /**
     * 
     */
    @TableField("EX4")
    private String ex4;

    /**
     * 
     */
    @TableField("EX5")
    private String ex5;

    /**
     * 
     */
    @TableField("EX6")
    private String ex6;

    /**
     * 
     */
    @TableField("EX7")
    private String ex7;

    /**
     * 
     */
    @TableField("EX8")
    private String ex8;

    /**
     * 
     */
    @TableField("EX9")
    private String ex9;

    /**
     * 
     */
    @TableField("EX10")
    private String ex10;

    /**
     * 
     */
    @TableField("EX11")
    private String ex11;

    /**
     * 
     */
    @TableField("EX12")
    private String ex12;

    /**
     * 
     */
    @TableField("EX13")
    private String ex13;

    /**
     * 
     */
    @TableField("EX14")
    private String ex14;

    /**
     * 
     */
    @TableField("EX15")
    private String ex15;

    /**
     * 
     */
    @TableField("EX16")
    private String ex16;

    /**
     * 
     */
    @TableField("EX17")
    private String ex17;

    /**
     * 
     */
    @TableField("EX18")
    private String ex18;

    /**
     * 
     */
    @TableField("EX19")
    private String ex19;

    /**
     * 
     */
    @TableField("EX20")
    private String ex20;

    /**
     * 
     */
    @TableField("EX21")
    private String ex21;

    /**
     * 
     */
    @TableField("EX22")
    private String ex22;

    /**
     * 
     */
    @TableField("EX23")
    private String ex23;

    /**
     * 
     */
    @TableField("EX24")
    private String ex24;

    /**
     * 
     */
    @TableField("EX25")
    private String ex25;

    /**
     * 
     */
    @TableField("EX26")
    private String ex26;

    /**
     * 
     */
    @TableField("EX27")
    private String ex27;

    /**
     * 
     */
    @TableField("EX28")
    private String ex28;

    /**
     * 
     */
    @TableField("EX29")
    private String ex29;

    /**
     * 
     */
    @TableField("EX30")
    private String ex30;

    /**
     * 
     */
    @TableField("EX31")
    private String ex31;

    /**
     * 
     */
    @TableField("EX32")
    private String ex32;

    /**
     * 
     */
    @TableField("EX33")
    private String ex33;

    /**
     * 
     */
    @TableField("EX34")
    private String ex34;

    /**
     * 
     */
    @TableField("EX35")
    private String ex35;

    /**
     * 
     */
    @TableField("EX36")
    private String ex36;

    /**
     * 
     */
    @TableField("EX37")
    private String ex37;

    /**
     * 
     */
    @TableField("EX38")
    private String ex38;

    /**
     * 
     */
    @TableField("EX39")
    private String ex39;

    /**
     * 
     */
    @TableField("EX40")
    private String ex40;

    /**
     * 
     */
    @TableField("EX41")
    private String ex41;

    /**
     * 
     */
    @TableField("EX42")
    private String ex42;

    /**
     * 
     */
    @TableField("EX43")
    private String ex43;

    /**
     * 
     */
    @TableField("EX44")
    private String ex44;

    /**
     * 
     */
    @TableField("EX45")
    private String ex45;

    /**
     * 
     */
    @TableField("EX46")
    private String ex46;

    /**
     * 
     */
    @TableField("EX47")
    private String ex47;

    /**
     * 
     */
    @TableField("EX48")
    private String ex48;

    /**
     * 
     */
    @TableField("EX49")
    private String ex49;

    /**
     * 
     */
    @TableField("EX50")
    private String ex50;

}
