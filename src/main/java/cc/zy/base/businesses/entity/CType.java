package cc.zy.base.businesses.entity;

import java.util.Date;

import lombok.Data;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 *  Entity
 *
 * @author Jiangjinlin
 * @date 2021-01-26 11:45:08
 */
@Data
@TableName("c_type")
public class CType {

    /**
     * 
     */
    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;

    /**
     * 
     */
    @TableField("PID")
    private Integer pid;

    /**
     * 
     */
    @TableField("NAME")
    private String name;

    /**
     * 
     */
    @TableField("SORT1")
    private Integer sort1;

    /**
     *
     */
    @TableField("SORT1")
    private String sort;
    /**
     * 
     */
    @TableField("SORT2")
    private Integer sort2;

    /**
     * 
     */
    @TableField("INFO")
    private String info;

    /**
     * 
     */
    @TableField("IMG_URL")
    private String imgUrl;

    /**
     * 
     */
    @TableField("CREATE_USER_ID")
    private Integer createUserId;

    /**
     * 
     */
    @TableField("CREATE_TIME")
    private Date createTime;

    /**
     * 
     */
    @TableField("UPDATE_USER_ID")
    private Integer updateUserId;

    /**
     * 
     */
    @TableField("UPDATE_TIME")
    private Date updateTime;

    /**
     * 
     */
    @TableField("STATUS")
    private Integer status;
    /**
     *
     */
    @TableField("NAME")
    private String userName;
    /**
     *
     */
    @TableField("NAME")
    private String updateName;


}
