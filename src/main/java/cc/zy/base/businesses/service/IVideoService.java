package cc.zy.base.businesses.service;


import cc.zy.base.businesses.entity.Video;
import cc.zy.base.common.entity.QueryRequest;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import io.swagger.models.auth.In;

import java.util.List;

/**
 *  Service接口
 *
 * @author Jiangjinlin
 * @date 2021-01-25 11:03:36
 */
public interface IVideoService extends IService<Video> {
    /**
     * 查询（分页）
     *
     * @param request QueryRequest
     * @param video video
     * @return IPage<Video>
     */
    IPage<Video> findVideos(QueryRequest request, Video video);

    /**
     * 查询（所有）
     *
     * @param video video
     * @return List<Video>
     */
    List<Video> findVideos(Video video);

    /**
     * 新增
     *
     * @param video video
     */
    void createVideo(Video video);

    /**
     * 修改
     *
     * @param video video
     */
    void updateVideo(Video video);

    /**
     * 删除
     *
     * @param video video
     */
    void deleteVideo(Video video);
    /**
     * 视频下架
     */
    void updateByIdSatus(int id);
    /**
     * 视频排序（一）
     */
    void updateBySort(int id, Integer sort, int sort1);
    /**
     * 查询
     */


    IPage<Video> findVideo(QueryRequest request, Video video);
    Video findById(Integer id);
    /**
     * 视频上架
     */
    void updateByIdSatusUp(int id);
    int findTypeIdByVid(Integer id);
    int findCourseIdByVid(Integer id);
    int insertVideoCourse(Integer courseId,Integer videoId);
    int insertVideoType(Integer videoId,Integer typeId);
    int insertCourseType(Integer courseId,Integer typeId);

}

