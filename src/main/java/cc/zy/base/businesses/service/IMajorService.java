package cc.zy.base.businesses.service;

import cc.zy.base.businesses.entity.Major;


import cc.zy.base.businesses.entity.SubjectCategory;
import cc.zy.base.common.entity.QueryRequest;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 专业表 Service接口
 *
 * @author Jiangjinlin
 * @date 2021-01-26 19:41:01
 */
public interface IMajorService extends IService<Major> {
    /**
     * 查询（分页）
     *
     * @param request QueryRequest
     * @param major major
     * @return IPage<Major>
     */
    IPage<Major> findMajors(QueryRequest request, Major major);


    Major findById(Integer id);
    /**
     * 查询（所有）
     *
     * @param major major
     * @return List<Major>
     */
    List<Major> findMajors(Major major);

    /**
     * 新增
     *
     * @param major major
     */
    void createMajor(Major major);

    /**
     * 修改
     *
     * @param major major
     */
    void updateMajor(Major major);

    /**
     * 删除
     *
     * @param major major
     */
    void deleteMajor(Major major);
}
