package cc.zy.base.businesses.service.impl;


import cc.zy.base.businesses.entity.ReqInfo;
import cc.zy.base.businesses.entity.ReqResult;
import cc.zy.base.businesses.entity.ReqResultExtension;
import cc.zy.base.businesses.mapper.ReqResultMapper;
import cc.zy.base.businesses.service.IReqResultExtensionService;
import cc.zy.base.businesses.service.IReqResultService;
import cc.zy.base.common.entity.QueryRequest;
import com.alibaba.fastjson.JSONObject;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Propagation;
import lombok.RequiredArgsConstructor;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import javax.annotation.Resource;
import java.util.List;

/**
 *  Service实现
 *
 * @author Jiangjinlin
 * @date 2021-01-25 10:16:25
 */
@Service
@RequiredArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class ReqResultServiceImpl extends ServiceImpl<ReqResultMapper, ReqResult> implements IReqResultService {
    @Resource
    private IReqResultExtensionService extensionService;

    private final ReqResultMapper reqResultMapper;

    @Override
    public IPage<ReqResult> findReqResults(QueryRequest request, ReqResult reqResult) {
        LambdaQueryWrapper<ReqResult> queryWrapper = new LambdaQueryWrapper<>();
        // TODO 设置查询条件
        Page<ReqResult> page = new Page<>(request.getPageNum(), request.getPageSize());
        return this.page(page, queryWrapper);
    }

    @Override
    public List<ReqResult> findReqResults(ReqResult reqResult) {
	    LambdaQueryWrapper<ReqResult> queryWrapper = new LambdaQueryWrapper<>();
		// TODO 设置查询条件
		return this.baseMapper.selectList(queryWrapper);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void createReqResult(ReqResult reqResult) {
        this.save(reqResult);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateReqResult(ReqResult reqResult) {
        this.saveOrUpdate(reqResult);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteReqResult(ReqResult reqResult) {
        LambdaQueryWrapper<ReqResult> wrapper = new LambdaQueryWrapper<>();
	    // TODO 设置删除条件
	    this.remove(wrapper);
	}

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void addReqResultList(List<ReqResult> reqResultList) {
        this.baseMapper.addReqResultList(reqResultList);
    }

    @Override
    public int addReqResultGetId(ReqResult reqResult) {
        return this.baseMapper.addReqResultGetId(reqResult);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ReqResult addReqResultAndReqResultExtension(ReqResult reqResult, ReqInfo reqInfo) {
        reqResult.setReqInfoId(reqInfo.getId());
        int i = addReqResultGetId(reqResult);
        reqResult.setId(i);
//        ReqResultExtension reqResultExtension = new ReqResultExtension();
//        reqResultExtension = JSONObject.parseObject(reqResult.getFieldInfos(), ReqResultExtension.class);
//        reqResultExtension.setReqInfoId(reqInfo.getId());
//        reqResultExtension.setReqResultId(reqResult.getId());
//        reqResultExtension.setReqResultId(reqResult.getId());
//        extensionService.createReqResultExtension(reqResultExtension);
        return reqResult;
    }
}
