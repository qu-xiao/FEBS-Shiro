package cc.zy.base.businesses.service;

import cc.zy.base.businesses.entity.StudentProgress;

import java.util.Map;


/**
 * 学籍进度service
 */

public interface IStudentProgressService {
    /**
     * 根据学生id查询学籍、异动状态、论文状态
     * @param id 学生id
     * @return
     */
    StudentProgress findStatusBySid(Integer id);

    /**
     *根据状态id查询字典表里的具体信息
     * @param id 字典表id
     * @return
     */
    String findStatusNameById(Integer id);

    /**
     * 查询学籍进度
     */
    Map<String,String> calculateProgress(Integer id);


}
