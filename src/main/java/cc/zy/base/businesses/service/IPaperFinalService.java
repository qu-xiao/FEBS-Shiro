package cc.zy.base.businesses.service;

import cc.zy.base.businesses.entity.PaperFinal;

import cc.zy.base.common.entity.QueryRequest;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 *  Service接口
 *
 * @author Jiangjinlin
 * @date 2021-01-26 14:57:22
 */
public interface IPaperFinalService extends IService<PaperFinal> {
    /**
     * 查询（分页）
     *
     * @param request QueryRequest
     * @param paperfinal paperfinal
     * @return IPage<Paperfinal>
     */
    IPage<PaperFinal> findPaperfinalPage(QueryRequest request, PaperFinal paperfinal);

    /**
     * 查询（所有）
     *
     * @param paperfinal paperfinal
     * @return List<Paperfinal>
     */
    List<PaperFinal> findPaperfinals(PaperFinal paperfinal);

    /**
     * 新增
     *
     * @param paperfinal paperfinal
     */
    void createPaperfinal(PaperFinal paperfinal);

    /**
     * 修改
     *
     * @param paperfinal paperfinal
     */
    void updatePaperfinal(PaperFinal paperfinal);

    /**
     * 删除
     *
     * @param paperfinal paperfinal
     */
    void deletePaperfinal(PaperFinal paperfinal);
}
