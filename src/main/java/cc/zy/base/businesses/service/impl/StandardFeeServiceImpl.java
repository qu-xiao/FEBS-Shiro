package cc.zy.base.businesses.service.impl;


import cc.zy.base.businesses.entity.StandardFee;
import cc.zy.base.businesses.mapper.StandardFeeMapper;
import cc.zy.base.businesses.service.IStandardFeeService;
import cc.zy.base.common.entity.FebsConstant;
import cc.zy.base.common.entity.QueryRequest;
import cc.zy.base.common.utils.SortUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 *  Service实现
 *
 * @author Jiangjinlin
 * @date 2021-01-26 09:54:23
 */
@Service
@RequiredArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class StandardFeeServiceImpl extends ServiceImpl<StandardFeeMapper, StandardFee> implements IStandardFeeService {
    @Resource
    private final StandardFeeMapper standardFeeMapper;


    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteStandardFees(String[] standardFeeIds) {
        List<String> list = Arrays.asList(standardFeeIds);
        // 删除用户
        this.removeByIds(list);
    }

    @Override
    public List<Map<String, String>> batchAll(Integer id) {

        return standardFeeMapper.batchAll(id);
    }

    @Override
    public List<Map<String, String>> collegeAll(Integer id) {
        return standardFeeMapper.collegeAll(id);
    }

    @Override
    public List<Map<String, String>> levelAll(Integer id) {
        return standardFeeMapper.levelAll(id);
    }

    @Override
    public List<Map<String, String>> subjectCategoryAll(Integer id) {
        return standardFeeMapper.subjectCategoryAll(id);
    }

    @Override
    public List<Map<String, String>> majorAll(Integer id) {
        return standardFeeMapper.majorAll(id);
    }

    @Override
    public IPage<StandardFee> findStandardFees(QueryRequest request, StandardFee standardFee) {
       LambdaQueryWrapper<StandardFee> queryWrapper = new LambdaQueryWrapper<>();
        Page<StandardFee> page = new Page<>(request.getPageNum(), request.getPageSize());
        page.setSearchCount(false);
        page.setTotal(baseMapper.countStandardFeeDetail(standardFee));
        SortUtil.handlePageSort(request, page, "T.ID", FebsConstant.ORDER_ASC, false);
        return this.baseMapper.findStandardFeeDetailPage(page,standardFee);
    }
//    @Override
//    public IPage<StandardFee> findStandardFees(QueryRequest request, StandardFee standardFee) {
//        LambdaQueryWrapper<StandardFee> queryWrapper = new LambdaQueryWrapper<>();
//        // TODO 设置查询条件
//        Page<StandardFee> page = new Page<>(request.getPageNum(), request.getPageSize());
//        return this.page(page, queryWrapper);
//
//
////          Page<StandardFee> page = new Page<>(request.getPageNum(), request.getPageSize());
////        page.setSearchCount(false);
////        page.setTotal(baseMapper.countStandardFeeDetail(standardFee));
////        SortUtil.handlePageSort(request, page, "id", FebsConstant.ORDER_ASC, false);
////        return this.baseMapper.findStandardFeeDetailPage(page,standardFee);
//    }

    @Override
    public List<StandardFee> findStandardFees(StandardFee standardFee) {
	    LambdaQueryWrapper<StandardFee> queryWrapper = new LambdaQueryWrapper<>();
		// TODO 设置查询条件
		return this.baseMapper.selectList(queryWrapper);
    }

//    @Override
//    public List<Map<String, Object>> fingStandardFeeAll() {
//
//        return this.standardFeeMapper.fingStandardFeeAll();
//    }

        @Override
    public StandardFee findById(Integer id) {
        return this.baseMapper.findById(id);
    }

    @Override
    public int addStandardFee(StandardFee standardFee) {
        return standardFeeMapper.addStandardFee(standardFee);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void createStandardFee(StandardFee standardFee) {
        this.save(standardFee);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateStandardFee(StandardFee standardFee) {
        this.saveOrUpdate(standardFee);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteStandardFee(StandardFee standardFee) {
        LambdaQueryWrapper<StandardFee> wrapper = new LambdaQueryWrapper<>();
	    // TODO 设置删除条件
	    this.remove(wrapper);
	}
}
