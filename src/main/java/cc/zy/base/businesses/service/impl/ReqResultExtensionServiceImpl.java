package cc.zy.base.businesses.service.impl;


import cc.zy.base.businesses.entity.ReqResultExtension;
import cc.zy.base.businesses.mapper.ReqResultExtensionMapper;
import cc.zy.base.businesses.service.IReqResultExtensionService;
import cc.zy.base.common.entity.QueryRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Propagation;
import lombok.RequiredArgsConstructor;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.List;

/**
 *  Service实现
 *
 * @author Jiangjinlin
 * @date 2021-01-25 16:06:07
 */
@Service
@RequiredArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class ReqResultExtensionServiceImpl extends ServiceImpl<ReqResultExtensionMapper, ReqResultExtension> implements IReqResultExtensionService {

    private final ReqResultExtensionMapper reqResultExtensionMapper;

    @Override
    public IPage<ReqResultExtension> findReqResultExtensions(QueryRequest request, ReqResultExtension reqResultExtension) {
        LambdaQueryWrapper<ReqResultExtension> queryWrapper = new LambdaQueryWrapper<>();
        // TODO 设置查询条件
        Page<ReqResultExtension> page = new Page<>(request.getPageNum(), request.getPageSize());
        return this.page(page, queryWrapper);
    }

    @Override
    public List<ReqResultExtension> findReqResultExtensions(ReqResultExtension reqResultExtension) {
	    LambdaQueryWrapper<ReqResultExtension> queryWrapper = new LambdaQueryWrapper<>();
		// TODO 设置查询条件
		return this.baseMapper.selectList(queryWrapper);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void createReqResultExtension(ReqResultExtension reqResultExtension) {
        this.save(reqResultExtension);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateReqResultExtension(ReqResultExtension reqResultExtension) {
        this.saveOrUpdate(reqResultExtension);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteReqResultExtension(ReqResultExtension reqResultExtension) {
        LambdaQueryWrapper<ReqResultExtension> wrapper = new LambdaQueryWrapper<>();
	    // TODO 设置删除条件
	    this.remove(wrapper);
	}
}
