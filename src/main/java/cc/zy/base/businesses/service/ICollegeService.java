package cc.zy.base.businesses.service;

import cc.zy.base.businesses.entity.Batch;
import cc.zy.base.businesses.entity.College;

import cc.zy.base.businesses.entity.Level;
import cc.zy.base.businesses.entity.Major;
import cc.zy.base.common.entity.QueryRequest;
import cc.zy.base.system.entity.User;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 *  Service接口
 *
 * @author Jiangjinlin
 * @date 2021-01-18 10:51:13
 */
public interface ICollegeService extends IService<College> {
    /**
     * 查询（分页）
     *
     * @param request QueryRequest
     * @param college college
     * @return IPage<College>
     */
    IPage<College> findColleges(QueryRequest request, College college);

    /**
     * 通过ID查找院校详细信息
     *
     * @param id id
     * @return 院校信息
     */
    College findCollegeDetailList(Integer id);

    /**
     * 通过ID查找院校详细信息
     *
     * @param id id
     * @return 院校信息
     */
    College findById(Integer id);

    /**
     * 查询（所有）
     *
     * @param college college
     * @return List<College>
     */
    List<College> findColleges(College college);

    /**
     * 新增
     *
     * @param college college
     */
    void createCollege(College college);

    /**
     * 修改
     *
     * @param college college
     */
    void updateCollege(College college);

    /**
     * 删除
     *
     * @param college college
     */
    void deleteCollege(College college);

    /**
     * 删除院校
     *
     * @param collegeIds 院校 id数组
     */
    void deleteColleges(String[] collegeIds);
    /**
     * 查询不带分页
     */
    List<College> findCollageListNotPage();

	/**
     * 级联：查询院校
     *
     * @return
     */
    List<College> findCollegeForSelect();

    /**
     * 级联：根据院校id查询层次
     *
     * @return
     */
    List<Level> findLevelForSelect(@Param("collegeId") Integer collegeId);

    /**
     * 级联：根据院校id和层次id查询专业
     *
     * @return
     */
    List<Major> findMajorForSelect(@Param("collegeId") Integer collegeId, @Param("levelId") Integer levelId);

    /**
     * 查询有效批次
     *
     * @return List<Batch>
     */
    List<Batch> findBatchForSelect();


}
