package cc.zy.base.businesses.service;

import cc.zy.base.businesses.entity.Papers;

import cc.zy.base.common.entity.QueryRequest;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 *  Service接口
 *
 * @author Jiangjinlin
 * @date 2021-01-25 10:40:25
 */
public interface IPapersService extends IService<Papers> {
    /**
     * 查询（分页）
     *
     * @param request QueryRequest
     * @param papers papers
     * @return IPage<Papers>
     */
    IPage<Papers> findPapersPage(QueryRequest request, Papers papers);

    /**
     * 查询（所有）
     *
     * @param papers
     * @return List<Papers>
     */
    List<Papers> findPapers(Papers papers);

    /**
     * 新增
     *
     * @param papers
     */
    void createPapers(Papers papers);

    /**
     * 修改
     *
     * @param papers
     */
    void updatePapers(Papers papers);

    /**
     * 删除
     *
     * @param papers
     */
    void deletePapers(Papers papers);



}
