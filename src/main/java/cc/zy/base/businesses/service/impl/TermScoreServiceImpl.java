package cc.zy.base.businesses.service.impl;


import cc.zy.base.businesses.entity.*;
import cc.zy.base.businesses.mapper.CollegeMapper;
import cc.zy.base.businesses.mapper.TermScoreMapper;
import cc.zy.base.businesses.service.ITermScoreService;
import cc.zy.base.common.entity.FebsConstant;
import cc.zy.base.common.entity.QueryRequest;
import cc.zy.base.common.utils.SortUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Propagation;
import lombok.RequiredArgsConstructor;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import javax.annotation.Resource;
import java.util.List;

/**
 *  Service实现
 *
 * @author zzz
 * @date 2021-01-28 16:13:11
 */
@Service
@RequiredArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class TermScoreServiceImpl extends ServiceImpl<TermScoreMapper, TermScore> implements ITermScoreService {

    private final TermScoreMapper termScoreMapper;
    @Resource
    private CollegeMapper collegeMapper;
    @Override
    public IPage<TermScore> findTermScores(QueryRequest request, TermScore termScore) {
        LambdaQueryWrapper<TermScore> queryWrapper = new LambdaQueryWrapper<>();
        // TODO 设置查询条件
        Page<TermScore> page = new Page<>(request.getPageNum(), request.getPageSize());
        return this.page(page, queryWrapper);
    }

    @Override
    public List<TermScore> findTermScores(TermScore termScore) {
	    LambdaQueryWrapper<TermScore> queryWrapper = new LambdaQueryWrapper<>();
		// TODO 设置查询条件
		return this.baseMapper.selectList(queryWrapper);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void createTermScore(TermScore termScore) {
        this.save(termScore);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateTermScore(TermScore termScore) {
        this.saveOrUpdate(termScore);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteTermScore(TermScore termScore) {
        LambdaQueryWrapper<TermScore> wrapper = new LambdaQueryWrapper<>();
	    // TODO 设置删除条件
	    this.remove(wrapper);
	}
    /**
     * 查询列表
     * @author wangpin
     * @param termScore
     * @return
     */
    @Override
    public IPage<TermScore> getTermScoreList(QueryRequest request,TermScore termScore) {
        Page<TermScore> page = new Page<>(request.getPageNum(), request.getPageSize());
        page.setSearchCount(false);
        page.setTotal(baseMapper.countTermScoreDetail(termScore));
        SortUtil.handlePageSort(request, page, "id", FebsConstant.ORDER_ASC, false);
        return this.baseMapper.getTermScoreList(page,termScore);
    }

    @Override
    public List<College> getCollegeList() {
        return collegeMapper.findCollegeDetail(new College());
    }

    /**
     * 通过学校id获取层次名称
     * @param collegeId
     * @return
     */
    @Override
    public List<Level> getLevelListByCollegeId(Integer collegeId) {
        return termScoreMapper.getLevelListByCollegeId(collegeId);
    }
    /**
     * 通过学校和层次获取对应专业
     * @param collegeId
     * @param levelId
     * @return
     */
    @Override
    public List<Major> getMajorListByIds(Integer collegeId, Integer levelId) {
        return termScoreMapper.getMajorListByIds(collegeId,levelId);
    }
    /**
     * 通过学校和层次及专业获取对应课程
     * @param collegeId
     * @param levelId
     * @param majorId
     * @return
     */
    @Override
    public List<TeachProgram> getCourseNameByIds(Integer collegeId, Integer levelId, Integer majorId,Integer termId) {
        return termScoreMapper.getCourseNameByIds(collegeId, levelId, majorId,termId);
    }
    /**
     * 根据层次id获取对应学期
     * @param levelId
     * @return
     */
    @Override
    public List<CollegeTerm> getTermsByLevelId(Integer levelId) {
        return termScoreMapper.getTermsByLevelId(levelId);
    }
}
