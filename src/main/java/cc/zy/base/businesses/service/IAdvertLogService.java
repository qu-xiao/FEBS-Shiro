package cc.zy.base.businesses.service;

import cc.zy.base.businesses.entity.AdvertLog;

import cc.zy.base.common.entity.QueryRequest;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 广告日志表 Service接口
 *
 * @author zhaojw
 * @date 2021-01-28 09:46:04
 */
public interface IAdvertLogService extends IService<AdvertLog> {
    /**
     * 查询（分页）
     *
     * @param request QueryRequest
     * @param advertLog advertLog
     * @return IPage<AdvertLog>
     */
    IPage<AdvertLog> findAdvertLogs(QueryRequest request, AdvertLog advertLog);

    /**
     * 查询（所有）
     *
     * @param advertLog advertLog
     * @return List<AdvertLog>
     */
    List<AdvertLog> findAdvertLogs(AdvertLog advertLog);

    /**
     * 新增
     *
     * @param  userId,用户id（学生表id）
     *        advertId，观看广告的id
     */
    void insertAdvertLogData(Integer userId,Integer advertId);

    /**
     * 用户退出广告时记录退出时间
     */
    void updateAdvertLog(Integer userId,Integer advertId);

    /**
     * 删除
     *
     * @param advertLog advertLog
     */
    void deleteAdvertLog(AdvertLog advertLog);

    /**
     * 根据openid查stuid
     */
    Integer findStuIdByOpenId(Integer openId);


}
