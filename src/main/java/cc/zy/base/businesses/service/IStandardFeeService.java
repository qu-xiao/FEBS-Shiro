package cc.zy.base.businesses.service;

import cc.zy.base.businesses.entity.StandardFee;
import cc.zy.base.common.entity.QueryRequest;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 *  Service接口
 *
 * @author Jiangjinlin
 * @date 2021-01-26 09:54:23
 */
public interface IStandardFeeService extends IService<StandardFee> {
    /**
     * 查询（分页）
     *
     * @param request QueryRequest
     * @param standardFee standardFee
     * @return IPage<StandardFee>
     */
    IPage<StandardFee> findStandardFees(QueryRequest request, StandardFee standardFee);

    /**
     * 查询（所有）
     *
     * @param standardFee standardFee
     * @return List<StandardFee>
     */
    List<StandardFee> findStandardFees(StandardFee standardFee);

//      /**
//     * 查询（所有）
//     *
//
//     * @return List<StandardFee>
//     */
//    List<Map<String,Object>> fingStandardFeeAll();


    /**
     * 新增
     *
     * @param standardFee standardFee
     */
    void createStandardFee(StandardFee standardFee);

    /**
     * 修改
     *
     * @param standardFee standardFee
     */
    void updateStandardFee(StandardFee standardFee);

    /**
     * 删除
     *
     * @param standardFee standardFee
     */
    void deleteStandardFee(StandardFee standardFee);

        /**
     * 通过ID查找套内缴费详细信息
     *
     * @param id id
     * @return 套内缴费信息
     */
    StandardFee findById(Integer id);

          /**
     * 通过ID查找套内缴费详细信息
     *
     * @param id id
     * @return 套内缴费信息
     */
    int addStandardFee(StandardFee standardFee);

        /**
     * 删除套内缴费信息
     *
     * @param standaedFeeIds 院校 id数组
     */
    void deleteStandardFees(String[] standaedFeeIds);


    List<Map<String,String>> batchAll(Integer id);

    List<Map<String,String>> collegeAll(Integer id);
    List<Map<String,String>> levelAll(Integer id);
    List<Map<String,String>> subjectCategoryAll(Integer id);
      List<Map<String,String>> majorAll(Integer id);
}
