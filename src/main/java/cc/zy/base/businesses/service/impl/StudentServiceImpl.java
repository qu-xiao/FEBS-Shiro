package cc.zy.base.businesses.service.impl;

import cc.zy.base.businesses.entity.StuLabel;
import cc.zy.base.common.entity.FebsConstant;
import cc.zy.base.common.entity.QueryRequest;
import cc.zy.base.businesses.entity.Student;
import cc.zy.base.businesses.mapper.StudentMapper;
import cc.zy.base.businesses.service.IStudentService;
import cc.zy.base.common.entity.QueryRequest;
import cc.zy.base.common.exception.FebsException;
import cc.zy.base.common.utils.SortUtil;
import cc.zy.base.system.entity.User;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Propagation;
import lombok.RequiredArgsConstructor;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Service实现
 *
 * @author Jiangjinlin
 * @date 2021-01-25 11:45:25
 */
@Service
@RequiredArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class StudentServiceImpl extends ServiceImpl<StudentMapper, Student> implements IStudentService {

    private final StudentMapper studentMapper;

    //通过学生表查询批次--使用批次表
    @Override
    public IPage<Student> selectStudentBatchPage(Student student, QueryRequest request) {
        Page<Student> page = new Page<>(request.getPageNum(), request.getPageSize());
        page.setSearchCount(false);
        page.setTotal(baseMapper.countStudentDetail(student));
        SortUtil.handlePageSort(request, page, "id", FebsConstant.ORDER_ASC, false);
        return this.baseMapper.selectStudentBatchPage(page,student);
    }

    @Override
    public IPage<Student> findStudents(QueryRequest request, Student student) {
        Page<Student> studentPage = new Page<>(request.getPageNum(), request.getPageSize());
        studentPage.setSearchCount(false);// 查询总记录数，默认是查询
        studentPage.setTotal(baseMapper.countStudentDetail(student));
        SortUtil.handlePageSort(request, studentPage, "ID", FebsConstant.ORDER_ASC, false);
        return this.baseMapper.findStudentDetailPage(studentPage, student);
    }

    @Override
    public List<Student> findStudents(Student student) {
        LambdaQueryWrapper<Student> queryWrapper = new LambdaQueryWrapper<>();
        // TODO 设置查询条件
        return this.baseMapper.selectList(queryWrapper);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void createStudent(Student student) {
        this.save(student);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateStudent(Student student) {
        this.saveOrUpdate(student);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteStudent(Student student) {
        LambdaQueryWrapper<Student> wrapper = new LambdaQueryWrapper<>();
        // TODO 设置删除条件
        this.remove(wrapper);
    }

    //查询分页--学生表
    @Override
    public IPage<Student> selectStudentDetailList(Student student, QueryRequest request) {
        Page<Student> page = new Page<>(request.getPageNum(), request.getPageSize());
        page.setSearchCount(false);
        System.out.println("********"+baseMapper.countStudentDetail(student));
        page.setTotal(baseMapper.countStudentDetail(student));

        SortUtil.handlePageSort(request,page,"id",FebsConstant.ORDER_ASC,false);
        return this.baseMapper.selectStudentDetailPage(page,student);
    }
	@Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteStudents(String[] collegeIds) {
        List<String> list = Arrays.asList(collegeIds);
        this.removeByIds(list);
    }

    @Override
    public void creatStuLable(StuLabel stuLabel) {
        this.baseMapper.creatStuLabel(stuLabel);
    }

    @Override
    public List<StuLabel> checkYourLabel(Long id) {
        return this.baseMapper.checkYourLabel(id);
    }

    @Override
    public void setRecycleStatus(int id) {
        this.baseMapper.setRecycleStatus(id);
    }

    @Override
    public void insertIntoRecycle(Student student) {
        this.baseMapper.insertIntoRecycle(student);
    }

    @Override
    public Student selectStuForRecycle(int id) {
        return this.baseMapper.selectStuForRecycle(id);
    }

    @Override
    public Student findStudentById(Integer id) {
        return this.studentMapper.selectById(id);
    }

    @Override
    public Boolean rejectTask(Integer id, Integer typeId, String remark) {
        int i = this.studentMapper.updateTaskStatusByStuId(id, typeId, remark);
        if (i > 0) {
            return true;
        } else {
            return false;
        }

    }

    @Override
    public Boolean passedTask(Integer id, Integer typeId, Student student) {
        int i = this.studentMapper.updateStatusByTask(id, typeId);
        if (i > 0) {
            int i1 = this.studentMapper.updateById(student);
            if (i1 > 0) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    @Override
    public Student findById(Integer id) {
        return this.baseMapper.findById(id);
    }
	@Override
    public Map<String, Object> findTaskDetail(Integer id, Integer typeId) {
        return this.studentMapper.findTaskDetailByStuId(id, typeId);
    }

    @Override
    public void delLable(Integer id) {
        this.baseMapper.delLable(id);
    }
    @Override
    public Integer[] findStuIdBuClassId(String classId) {
        Integer[] ids = this.studentMapper.selectStuidByClass(classId);
        return ids;
    }

    //学生现场确认地点审核通过
    @Override
    @Transactional
    public Boolean passExamLocation(Integer taskId, Integer stuId, Integer examLocationId) {
        try {
            studentMapper.updateStatusById(taskId);
            studentMapper.updateStudentExamLocation(stuId, examLocationId);
        }
        catch (Exception e){
            throw new FebsException("更新失败");
        }
        return true;
    }

    //学生现场确认地点审核不通过
    @Override
    public Boolean noPassExamLocation(Integer taskId, String remark) {
        try {
          studentMapper.updateStatusByIdNoPass(taskId,remark);
        }
        catch (Exception e){
            throw new FebsException("更新失败");
        }
        return true;
    }

    @Override
    public Map<String, Object> findTaskByStuidAndType(Integer stuId, Integer typeId) {
        return studentMapper.findTaskByStuidAndType(stuId,typeId);
    }

	@Override
    public int updateStudentByEntity(Student student) {
        return studentMapper.updateById(student);
    }
}
