package cc.zy.base.businesses.service.impl;


import cc.zy.base.businesses.entity.CType;
import cc.zy.base.businesses.entity.CTypeTree;
import cc.zy.base.businesses.mapper.CTypeMapper;
import cc.zy.base.businesses.service.ICTypeService;
import cc.zy.base.businesses.utils.TreeUtil;
import cc.zy.base.common.entity.FebsConstant;
import cc.zy.base.common.entity.QueryRequest;
import cc.zy.base.common.utils.FebsUtil;
import cc.zy.base.common.utils.SortUtil;
import cc.zy.base.system.entity.Dept;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Propagation;
import lombok.RequiredArgsConstructor;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *  Service实现
 *
 * @author Jiangjinlin
 * @date 2021-01-25 10:44:35
 */
@Service
@RequiredArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class CTypeServiceImpl extends ServiceImpl<CTypeMapper, CType> implements ICTypeService {

    private final CTypeMapper cTypeMapper;

    @Override
    public IPage<CType> findCTypes(QueryRequest request, CType cType) {

        Page<CType> page = new Page<>(request.getPageNum(), request.getPageSize());
        page.setSearchCount(false);
        page.setTotal(baseMapper.countCTypeDetail(cType));
        SortUtil.handlePageSort(request, page, "id", FebsConstant.ORDER_ASC, false);
        return this.baseMapper.findCTypeDetail(page, cType);
    }



    @Override
    public List<CType> findCTypes(CType cType) {
	    LambdaQueryWrapper<CType> queryWrapper = new LambdaQueryWrapper<>();
		// TODO 设置查询条件
		return this.baseMapper.selectList(queryWrapper);
    }




    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteCType(CType cType) {
        LambdaQueryWrapper<CType> wrapper = new LambdaQueryWrapper<>();
	    // TODO 设置删除条件
	    this.remove(wrapper);
	}

    @Override
    public List<CTypeTree<CType>> findCType() {
        List<CType> cTypeList = this.baseMapper.selectList(new QueryWrapper<>());
        System.out.println(cTypeList);
        List<CTypeTree<CType>> trees = this.convertCType(cTypeList);
        System.out.println(trees);
        return TreeUtil.buildCTypeTree(trees);
    }

    @Override
    public List<CTypeTree<CType>> findCType(CType cType) {
        System.out.println("CTypeServiceImpl.findCType" + cType);
        QueryWrapper<CType> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotBlank(cType.getName())) {
            queryWrapper.lambda().eq(CType::getName, cType.getName());
        }
        List<CType> cTypeList = this.baseMapper.selectList(queryWrapper);
        List<CTypeTree<CType>> trees = this.convertCType(cTypeList);
        System.out.println(cTypeList);
        System.out.println(trees);
        return TreeUtil.buildCTypeTree(trees);
    }

    private List<CTypeTree<CType>> convertCType(List<CType> CType) {
        List<CTypeTree<CType>> trees = new ArrayList<>();
        CType.forEach(cType -> {
            CTypeTree<CType> tree = new CTypeTree<>();
            tree.setId(String.valueOf(cType.getId()));
            tree.setParentId(String.valueOf(cType.getPid()));
            tree.setName(cType.getName());
            tree.setData(cType);
            trees.add(tree);
        });
        return trees;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateByIdStatus(int id) {
        int  pid= cTypeMapper.findPid(id);
        long l = System.currentTimeMillis();
        Date updateTime = new Date(l);
        Long updateUserId = FebsUtil.getCurrentUser().getUserId();
        if(pid==0){
        cTypeMapper.updateByIdStatus(id,updateTime,updateUserId.intValue());
        cTypeMapper.updateByIdStatusSon(id,updateTime,updateUserId.intValue());}
        else if(pid!=0) {
            cTypeMapper.updateByIdStatusSonDown(id,updateTime,updateUserId.intValue());
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateByIdStatusUp(int id) {
        int pid = cTypeMapper.findPid(id);
        long l = System.currentTimeMillis();
        Date updateTime = new Date(l);
        Long updateUserId = FebsUtil.getCurrentUser().getUserId();
         cTypeMapper.findStatus(pid);
        cTypeMapper.updateByIdStatusUp(id,updateTime,updateUserId.intValue());
    }

    @Override

    public void updateSort(int id,int sort1, int sort2,int sort3) {
        int id1 = cTypeMapper.selectByIdPid(sort1,sort3);
        cTypeMapper.updateByIdSort(id,sort3);
        cTypeMapper.updateByIdSort(id1,sort2);
    }

    @Override
    public CType findById(int id) {
    return  cTypeMapper.findById(id);
    }
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void createCType(CType cType) {

        Integer ctypeId=cType.getId();
        int cTypeSORT1MAX = baseMapper.findCTypeSORT1MAX();
        int ctypeSORT2MAX=0;
        if (cType.getId()==null){
            ctypeId = 0;
            cTypeSORT1MAX=cTypeSORT1MAX+1;
        } else {
            cTypeSORT1MAX=baseMapper.findCTypeSort1Now(ctypeId);
            ctypeSORT2MAX=baseMapper.findCTypeSORT2MAX(cTypeSORT1MAX);
            ctypeSORT2MAX=ctypeSORT2MAX+1;
        }
        cType.setPid(ctypeId);
        cType.setSort1(cTypeSORT1MAX);
        cType.setSort2(ctypeSORT2MAX);
        cType.setCreateTime(new Date(System.currentTimeMillis()));
        cType.setCreateUserId(Math.toIntExact(FebsUtil.getCurrentUser().getUserId()));
        this.save(cType);
    }
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateCType(CType cType) {
        if (cType.getPid()!=null){
            CType cType1 = findById(cType.getPid());
            cType.setSort1(cType1.getSort1());
            CType cType2 = findById(cType.getId());
            if(!cType.getPid().equals(cType2.getPid())){
                int ctypeSORT2MAX=baseMapper.findCTypeSORT2MAX(cType1.getSort1());
                cType.setSort2(ctypeSORT2MAX+1);
            }
        }
        cType.setUpdateUserId(Math.toIntExact(FebsUtil.getCurrentUser().getUserId()));
        cType.setUpdateTime(new Date(System.currentTimeMillis()));
        this.saveOrUpdate(cType);
    }
    @Override
    public List<CTypeTree<CType>> selectCTypeList1() {
        List<CType> cTypeList1 = cTypeMapper.selectCTypeList1();
        List<CTypeTree<CType>> trees = this.convertCType(cTypeList1);
        return trees;
    }

}
