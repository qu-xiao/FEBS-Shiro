package cc.zy.base.businesses.service;

import cc.zy.base.businesses.entity.StudentGroup;

import cc.zy.base.common.entity.QueryRequest;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 学生组表	 Service接口
 *
 * @author LiPeng
 * @date 2021-01-26 16:24:15
 */
public interface IStudentGroupService extends IService<StudentGroup> {
    /**
     * 查询（分页）
     *
     * @param request QueryRequest
     * @param studentGroup studentGroup
     * @return IPage<StudentGroup>
     */
    IPage<StudentGroup> findStudentGroups(QueryRequest request, StudentGroup studentGroup);

    /**
     * 查询（所有）
     *
     * @param studentGroup studentGroup
     * @return List<StudentGroup>
     */
    List<StudentGroup> findStudentGroups(StudentGroup studentGroup);

    /**
     * 新增
     *
     * @param studentGroup studentGroup
     */
    void createStudentGroup(StudentGroup studentGroup);

    /**
     * 修改
     *
     * @param studentGroup studentGroup
     */
    void updateStudentGroup(StudentGroup studentGroup);

    /**
     * 删除
     *
     * @param studentGroup studentGroup
     */
    void deleteStudentGroup(StudentGroup studentGroup);

    /**
     * 删除学生组
     *
     * @param groupIds 学生组 id数组
     */
    void deleteStudentGroup(String[] groupIds);

    /**
     * 新增
     *
     * @param studentGroup 学生组
     */
    void createGroup(StudentGroup studentGroup);
}
