package cc.zy.base.businesses.service;

import cc.zy.base.businesses.entity.ReqResultExtension;


import cc.zy.base.common.entity.QueryRequest;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 *  Service接口
 *
 * @author Jiangjinlin
 * @date 2021-01-25 16:06:07
 */
public interface IReqResultExtensionService extends IService<ReqResultExtension> {
    /**
     * 查询（分页）
     *
     * @param request QueryRequest
     * @param reqResultExtension reqResultExtension
     * @return IPage<ReqResultExtension>
     */
    IPage<ReqResultExtension> findReqResultExtensions(QueryRequest request, ReqResultExtension reqResultExtension);

    /**
     * 查询（所有）
     *
     * @param reqResultExtension reqResultExtension
     * @return List<ReqResultExtension>
     */
    List<ReqResultExtension> findReqResultExtensions(ReqResultExtension reqResultExtension);

    /**
     * 新增
     *
     * @param reqResultExtension reqResultExtension
     */
    void createReqResultExtension(ReqResultExtension reqResultExtension);

    /**
     * 修改
     *
     * @param reqResultExtension reqResultExtension
     */
    void updateReqResultExtension(ReqResultExtension reqResultExtension);

    /**
     * 删除
     *
     * @param reqResultExtension reqResultExtension
     */
    void deleteReqResultExtension(ReqResultExtension reqResultExtension);
}
