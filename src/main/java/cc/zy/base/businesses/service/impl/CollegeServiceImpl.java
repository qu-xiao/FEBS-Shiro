package cc.zy.base.businesses.service.impl;

import cc.zy.base.businesses.entity.Batch;
import cc.zy.base.businesses.entity.Level;
import cc.zy.base.businesses.entity.Major;
import cc.zy.base.businesses.mapper.BatchMapper;
import cc.zy.base.businesses.mapper.LevelMapper;
import cc.zy.base.businesses.mapper.MajorMapper;
import cc.zy.base.common.entity.FebsConstant;
import cc.zy.base.common.entity.QueryRequest;
import cc.zy.base.businesses.entity.College;
import cc.zy.base.businesses.mapper.CollegeMapper;
import cc.zy.base.businesses.service.ICollegeService;
import cc.zy.base.common.utils.Md5Util;
import cc.zy.base.common.utils.SortUtil;
import cc.zy.base.system.entity.User;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Propagation;
import lombok.RequiredArgsConstructor;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 *  Service实现
 *
 * @author Jiangjinlin
 * @date 2021-01-18 10:51:13
 */
@Service
@RequiredArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class CollegeServiceImpl extends ServiceImpl<CollegeMapper, College> implements ICollegeService {
    @Resource
    private final CollegeMapper collegeMapper;
	private final LevelMapper levelMapper;

    private final MajorMapper majorMapper;

    private final BatchMapper batchMapper;

    @Override
    public IPage<College> findColleges(QueryRequest request, College college) {
        Page<College> page = new Page<>(request.getPageNum(), request.getPageSize());
        page.setSearchCount(false);
        page.setTotal(baseMapper.countCollegeDetail(college));
        SortUtil.handlePageSort(request, page, "id", FebsConstant.ORDER_ASC, false);
        return this.baseMapper.findCollegeDetailPage(page, college);
    }

    @Override
    public College findById(Integer id) {
        return this.baseMapper.findById(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteColleges(String[] collegeIds) {
        List<String> list = Arrays.asList(collegeIds);
        // 删除用户
        this.removeByIds(list);
    }

    @Override
    public List<College> findCollageListNotPage() {
        List<College> collageListNotPage = baseMapper.findCollageListNotPage();
        return collageListNotPage;
    }



    @Override
    public College findCollegeDetailList(Integer id) {
        College param = new College();
        param.setId(id);
        List<College> college = this.baseMapper.findCollegeDetail(param);
        return CollectionUtils.isNotEmpty(college) ? college.get(0) : null;
    }

    @Override
    public List<College> findColleges(College college) {
	    LambdaQueryWrapper<College> queryWrapper = new LambdaQueryWrapper<>();
		// TODO 设置查询条件
		return this.baseMapper.selectList(queryWrapper);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void createCollege(College college) {
        college.setCreatedate(new Date());
        college.setUpdatedate(new Date());
        this.save(college);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateCollege(College college) {
        this.saveOrUpdate(college);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteCollege(College college) {
        LambdaQueryWrapper<College> wrapper = new LambdaQueryWrapper<>();
	    // TODO 设置删除条件
	    this.remove(wrapper);
	}

	@Override
    public List<College> findCollegeForSelect() {
        return this.collegeMapper.selectCollegeForSelect();
    }

    @Override
    public List<Level> findLevelForSelect(Integer collegeId) {
        return this.levelMapper.selectLevelForSelect(collegeId);
    }

    @Override
    public List<Major> findMajorForSelect(Integer collegeId, Integer levelId) {
        return this.majorMapper.selectMajorForSelect(collegeId, levelId);
    }

    @Override
    public List<Batch> findBatchForSelect() {
        return this.batchMapper.selectBatchForSelect();
    }
}
