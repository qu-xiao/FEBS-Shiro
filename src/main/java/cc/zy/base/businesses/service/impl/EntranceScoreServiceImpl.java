package cc.zy.base.businesses.service.impl;


import cc.zy.base.businesses.entity.EntranceScore;
import cc.zy.base.businesses.mapper.EntranceScoreMapper;
import cc.zy.base.businesses.mapper.TestSubjectMapper;
import cc.zy.base.businesses.service.IEntranceScoreService;
import cc.zy.base.common.entity.FebsConstant;
import cc.zy.base.common.entity.QueryRequest;
import cc.zy.base.common.utils.SortUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Propagation;
import lombok.RequiredArgsConstructor;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 *  Service实现
 *
 * @author Jiangjinlin
 * @date 2021-01-23 23:11:18
 */
@Service
@RequiredArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class EntranceScoreServiceImpl extends ServiceImpl<EntranceScoreMapper, EntranceScore> implements IEntranceScoreService {

    private final EntranceScoreMapper entranceScoreMapper;
    @Resource
    private TestSubjectMapper testSubjectMapper;
    @Override
    public IPage<EntranceScore> findTEntranceScores(QueryRequest request, EntranceScore entranceScore) {
        LambdaQueryWrapper<EntranceScore> queryWrapper = new LambdaQueryWrapper<>();
        // TODO 设置查询条件
        Page<EntranceScore> page = new Page<>(request.getPageNum(), request.getPageSize());
        return this.page(page, queryWrapper);
    }

    @Override
    public List<EntranceScore> findTEntranceScores(EntranceScore entranceScore) {
	    LambdaQueryWrapper<EntranceScore> queryWrapper = new LambdaQueryWrapper<>();
		// TODO 设置查询条件
		return this.baseMapper.selectList(queryWrapper);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void createTEntranceScore(EntranceScore entranceScore) {
        this.save(entranceScore);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateTEntranceScore(EntranceScore entranceScore) {
        this.saveOrUpdate(entranceScore);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteTEntranceScore(EntranceScore entranceScore) {
        LambdaQueryWrapper<EntranceScore> wrapper = new LambdaQueryWrapper<>();
	    // TODO 设置删除条件
	    this.remove(wrapper);
	}


    @Override
    public IPage<EntranceScore> findList(EntranceScore entranceScore, QueryRequest request) {
        String sequence = this.baseMapper.getSequence();
        Page<EntranceScore> page = new Page<>(request.getPageNum(), request.getPageSize());
        page.setSearchCount(false);
        Long l = this.baseMapper.countEntranceScoreDetail(entranceScore);
        if (l==null||l==0){
            page.setTotal(0);
            SortUtil.handlePageSort(request, page, "id", FebsConstant.ORDER_ASC, false);
            return this.baseMapper.findList(page,sequence, entranceScore);
        }else {
            page.setTotal(this.baseMapper.countEntranceScoreDetail(entranceScore));
            SortUtil.handlePageSort(request, page, "id", FebsConstant.ORDER_ASC, false);
            return this.baseMapper.findList(page,sequence, entranceScore);
        }
    }

    /**
     * 获取所有批次名称
     * @return
     */
    @Override
    public List<EntranceScore> getBatchs() {
        return this.entranceScoreMapper.getBatchs();
    }
    /**
     * 通过批次Id获取院校名称
     * @return
     */
    @Override
    public List<EntranceScore> getCollegesByBatchId(Integer batchId) {
        return this.baseMapper.getCollegesByBatchId(batchId);
    }

    @Override
    public Integer getStuTotalScore(EntranceScore entranceScore) {
        return this.baseMapper.getStuTotalScore(entranceScore);
    }

    @Override
    public List<EntranceScore> getSubjectsAliasAndNum(String levelName, String subtypeName) {
        return testSubjectMapper.getSubjectsAliasAndNum(levelName,subtypeName);
    }

    @Override
    public List<EntranceScore> getEntranceIds(String levelName, String subtypeName, String batchName) {
        return testSubjectMapper.getEntranceIds(levelName,subtypeName,batchName);
    }
    /**
     * 增加入学成绩数据
     * @param entranceScore
     * @param score
     * @return
     */
    @Override
    public int insertEntrance(EntranceScore entranceScore, String score) {
        return this.baseMapper.insertEntrance(entranceScore,score);
    }

    /**
     * 检查数据库是否有同批次，同一个人
     * @param identity
     * @param batchId
     * @return
     */
    @Override
    public List<EntranceScore> checkUnique(String identity, String batchId) {
        return this.baseMapper.checkUnique(identity,batchId);
    }
    /**
     * 查询所有
     * @param entranceScore
     * @param request
     * @return
     */
    @Override
    public IPage<EntranceScore> findAllList(EntranceScore entranceScore, QueryRequest request) {
        String sequence = this.baseMapper.getAllSequence();
        Page<EntranceScore> page = new Page<>(request.getPageNum(), request.getPageSize());
        page.setSearchCount(false);
        Long l = this.baseMapper.countEntranceScoreDetail(entranceScore);
        if (l==null||l==0){
            page.setTotal(0);
            SortUtil.handlePageSort(request, page, "id", FebsConstant.ORDER_ASC, false);
            return this.baseMapper.findList(page,sequence, entranceScore);
        }else {
            page.setTotal(this.baseMapper.countEntranceScoreDetail(entranceScore));
            SortUtil.handlePageSort(request, page, "id", FebsConstant.ORDER_ASC, false);
            return this.baseMapper.findList(page,sequence, entranceScore);
        }
    }
}
