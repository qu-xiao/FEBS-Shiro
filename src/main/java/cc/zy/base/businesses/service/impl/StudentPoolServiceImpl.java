package cc.zy.base.businesses.service.impl;

import cc.zy.base.businesses.entity.StudentPool;
import cc.zy.base.businesses.mapper.StudentPoolMapper;
import cc.zy.base.businesses.service.IStudentPoolService;
import cc.zy.base.common.entity.QueryRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Propagation;
import lombok.RequiredArgsConstructor;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.List;

/**
 *  Service实现
 *
 * @author Jiangjinlin
 * @date 2021-01-25 10:55:03
 */
@Service
@RequiredArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class StudentPoolServiceImpl extends ServiceImpl<StudentPoolMapper, StudentPool> implements IStudentPoolService {

    private final StudentPoolMapper studentPoolMapper;

    @Override
    public IPage<StudentPool> findStudentPools(QueryRequest request, StudentPool studentPool) {
        LambdaQueryWrapper<StudentPool> queryWrapper = new LambdaQueryWrapper<>();
        // TODO 设置查询条件
        Page<StudentPool> page = new Page<>(request.getPageNum(), request.getPageSize());
        return this.page(page, queryWrapper);
    }

    @Override
    public List<StudentPool> findStudentPools(StudentPool studentPool) {
	    LambdaQueryWrapper<StudentPool> queryWrapper = new LambdaQueryWrapper<>();
		// TODO 设置查询条件
		return this.baseMapper.selectList(queryWrapper);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void createStudentPool(StudentPool studentPool) {
        this.save(studentPool);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateStudentPool(StudentPool studentPool) {
        this.saveOrUpdate(studentPool);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteStudentPool(StudentPool studentPool) {
        LambdaQueryWrapper<StudentPool> wrapper = new LambdaQueryWrapper<>();
	    // TODO 设置删除条件
	    this.remove(wrapper);
	}

    @Override
    public int AddStudentPoolList(List<StudentPool> studentPools) {
        int StudentPoolRetrnValue= baseMapper.AddStudentPoolList(studentPools);
        return  StudentPoolRetrnValue;
    }

    @Override
    public StudentPool ifStudentPool(StudentPool studentPool) {
        StudentPool ifStudentPool= baseMapper.ifStudentPool(studentPool);
        return ifStudentPool;
    }
}
