package cc.zy.base.businesses.service.impl;

import cc.zy.base.businesses.entity.Dic;
import cc.zy.base.businesses.entity.Student;
import cc.zy.base.businesses.entity.TransferApplication;
import cc.zy.base.businesses.entity.TransferApplicationVo;
import cc.zy.base.businesses.mapper.DicMapper;
import cc.zy.base.businesses.mapper.StudentMapper;
import cc.zy.base.businesses.mapper.TransferApplicationMapper;
import cc.zy.base.businesses.service.TransferApplicationService;
import cc.zy.base.common.entity.FebsConstant;
import cc.zy.base.common.entity.QueryRequest;
import cc.zy.base.common.utils.SortUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 *  Service实现
 *
 * @author liuheng
 * @date 2021-01-18 10:51:13
 */
@Service
@RequiredArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class TransferApplicationServiceImpl extends ServiceImpl<TransferApplicationMapper, TransferApplication> implements TransferApplicationService {
    @Resource
    private final TransferApplicationMapper transferApplicationMapper;
    @Resource
    private final StudentMapper studentMapper;
    @Resource
    private final DicMapper dicMapper;


    @Override
    public IPage<TransferApplication> findTransferApplications(QueryRequest request, TransferApplication transferApplication) {
        Page<TransferApplication> page = new Page<>(request.getPageNum(), request.getPageSize());
        page.setSearchCount(true);
        page.setTotal(baseMapper.countTransferApplication(transferApplication));
        SortUtil.handlePageSort(request, page, "approvalStatusId", FebsConstant.ORDER_ASC, false);
        SortUtil.handlePageSort(request, page, "applicationTime", FebsConstant.ORDER_ASC, false);
        return this.baseMapper.findTransferApplicationPage(page, transferApplication);
    }

    /**
     * 根据id查找
     * @param id
     * @return
     */
    @Override
    public TransferApplicationVo findTransferDetailById(Integer id) {
        TransferApplicationVo transferApplicationVo = this.transferApplicationMapper.findTransferDetailById(id);
        List<String> attachmentList = this.transferApplicationMapper.findTransferAttachment(id);
        transferApplicationVo.setAttachmentList(attachmentList);
        if(transferApplicationVo!=null&&transferApplicationVo.getTransferTypeId()!=null){
            if(transferApplicationVo.getTransferTypeId()==24)
                transferApplicationVo.setYearLimit(this.transferApplicationMapper.findSuspensionCollegeByTransferApplicationId(transferApplicationVo.getId()));
            if(transferApplicationVo.getTransferTypeId()==25)
                transferApplicationVo.setNewBatchName(this.transferApplicationMapper.findBackCollegeByTransferApplicationId(transferApplicationVo.getId()));
            if(transferApplicationVo.getTransferTypeId()==27)
                transferApplicationVo.setNewMajorName(this.transferApplicationMapper.findMajorChangeByTransferApplicationId(transferApplicationVo.getId()));
            if(transferApplicationVo.getTransferTypeId()==28)
                transferApplicationVo.setNewStudyTypeName(this.transferApplicationMapper.findStudyTypeChangeByTransferApplicationId(transferApplicationVo.getId()));
        }
        return transferApplicationVo;
    }

    @Override
    public List<TransferApplication> findTransferApplications(TransferApplication transferApplication) {
	    LambdaQueryWrapper<TransferApplication> queryWrapper = new LambdaQueryWrapper<>();
		// TODO 设置查询条件
		return this.baseMapper.selectList(queryWrapper);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void createTransferApplication(TransferApplication transferApplication) {
        transferApplication.setApplicationTime(new Date());
        this.save(transferApplication);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateTransferApplication(TransferApplication transferApplication) {
        TransferApplication transferApplication1 = transferApplicationMapper.selectById(transferApplication.getId());
        transferApplication1.setOpinion(transferApplication.getOpinion());
        transferApplication1.setApprovalTime(new Date());
        transferApplication1.setApprovalStatusId(transferApplication.getApprovalStatusId());
        Student student = studentMapper.selectById(transferApplication1.getStuId());
        log.debug(transferApplication.getTransferTypeId()+"异动类型id");
        Dic dic = dicMapper.selectById(transferApplication1.getTransferTypeId());

        QueryWrapper<Dic> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("type","change_status");
        queryWrapper.eq("detail",dic.getDetail());
        List<Dic> dicList = dicMapper.selectList(queryWrapper);
        student.setTracsaction(dicList.get(0).getId());
        studentMapper.updateStudentTransaction(student);
        this.saveOrUpdate(transferApplication1);
    }
}
