package cc.zy.base.businesses.service.impl;


import cc.zy.base.businesses.entity.Classes;
import cc.zy.base.businesses.entity.College;
import cc.zy.base.businesses.entity.TeacherChangeLog;
import cc.zy.base.businesses.mapper.ClassesMapper;
import cc.zy.base.businesses.mapper.TeacherChangeLogMapper;
import cc.zy.base.businesses.service.IClassesService;
import cc.zy.base.common.entity.FebsConstant;
import cc.zy.base.common.entity.QueryRequest;
import cc.zy.base.common.utils.FebsUtil;
import cc.zy.base.common.utils.SortUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Propagation;
import lombok.RequiredArgsConstructor;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * 班级表 Service实现
 *
 * @author Jiangjinlin
 * @date 2021-01-25 19:29:08
 */
@Service
@RequiredArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class ClassesServiceImpl extends ServiceImpl<ClassesMapper, Classes> implements IClassesService {

    private final ClassesMapper classesMapper;
    private final TeacherChangeLogMapper teacherChangeLogMapper;

    @Override
    public IPage<Classes> findClassess(QueryRequest request, Classes classes) {
        Page<College> page = new Page<>(request.getPageNum(), request.getPageSize());
        page.setSearchCount(false);
        page.setTotal(baseMapper.countClassesDetail(classes));
        SortUtil.handlePageSort(request, page, "createTime", FebsConstant.ORDER_DESC, true);
        return this.baseMapper.findClassesDetailPage(page, classes);
    }

    @Override
    public List<Classes> findClassess(Classes classes) {
	    LambdaQueryWrapper<Classes> queryWrapper = new LambdaQueryWrapper<>();
		// TODO 设置查询条件
		return this.baseMapper.selectList(queryWrapper);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void createClasses(Classes classes) {
        this.save(classes);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateClasses(Classes classes) {
        this.saveOrUpdate(classes);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteClasses(Classes classes) {
        LambdaQueryWrapper<Classes> wrapper = new LambdaQueryWrapper<>();
	    // TODO 设置删除条件
	    this.remove(wrapper);
	}

	@Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteClass(String[] classIds) {
        List<String> list = Arrays.asList(classIds);
        this.removeByIds(list);
    }

	@Override
    public Classes findById(Integer id) {
        return this.baseMapper.findById(id);
    }

	@Override
    @Transactional(rollbackFor = Exception.class)
    public void updateTeacher(TeacherChangeLog teacherChangeLog) {
        teacherChangeLog.setChangeTime(new Date());
        this.teacherChangeLogMapper.insert(teacherChangeLog);
        Classes classes = new Classes();
        classes.setId(teacherChangeLog.getClassId());
        classes.setUserId(teacherChangeLog.getUserId());
        this.classesMapper.updateById(classes);
    }

	@Override
    @Transactional(rollbackFor = Exception.class)
    public void createClass(Classes classes) {
        classes.setCreateUserId(FebsUtil.getCurrentUser().getUserId());
        classes.setCreateTime(new Date());
        this.save(classes);
    }

	@Override
    public Classes findClassesById(Integer classesId) {
        return this.classesMapper.selectClassesById(classesId);
    }

	@Override
    public IPage<TeacherChangeLog> findTeacherChangeLogByClassesId(QueryRequest request, Integer classesId) {
        Page<TeacherChangeLog> page = new Page<>(request.getPageNum(), request.getPageSize());
        page.setSearchCount(false);
        page.setTotal(this.teacherChangeLogMapper.countTeacherChangeLog(classesId));
        SortUtil.handlePageSort(request, page, "changeTime", FebsConstant.ORDER_DESC, true);
        return this.teacherChangeLogMapper.selectTeacherChangeLogByClassId(page, classesId);
    }

}