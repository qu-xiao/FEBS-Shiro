package cc.zy.base.businesses.service.impl;


import cc.zy.base.businesses.entity.Batch;
import cc.zy.base.businesses.entity.TeachProgram;
import cc.zy.base.businesses.mapper.TeachProgramMapper;
import cc.zy.base.businesses.service.ITeachProgramService;
import cc.zy.base.common.entity.FebsConstant;
import cc.zy.base.common.entity.QueryRequest;
import cc.zy.base.common.utils.SortUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Propagation;
import lombok.RequiredArgsConstructor;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import javax.annotation.Resource;
import java.util.List;

/**
 *  Service实现
 *
 * @author pzj
 * @date 2021-01-25 09:38:13
 */
@Service
@RequiredArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class TeachProgramServiceImpl extends ServiceImpl<TeachProgramMapper, TeachProgram> implements ITeachProgramService {
    @Resource
    private final TeachProgramMapper teachProgramMapper;

    /**
     * 教学计划展示
     * @param request QueryRequest
     * @param teachProgram teachProgram
     * @return
     */
    @Override
    public IPage<TeachProgram> findTeachPrograms(QueryRequest request, TeachProgram teachProgram) {
        Page<TeachProgram> page = new Page<>(request.getPageNum(), request.getPageSize());
        page.setSearchCount(false);
        page.setTotal(baseMapper.countTeachProgramDetail(teachProgram));
        SortUtil.handlePageSort(request, page, "id", FebsConstant.ORDER_ASC, false);
        return this.baseMapper.findTeachProgramDetailPage(page,teachProgram);
    }

    @Override
    public List<TeachProgram> findTeachPrograms(TeachProgram teachProgram) {
	    LambdaQueryWrapper<TeachProgram> queryWrapper = new LambdaQueryWrapper<>();
		// TODO 设置查询条件
		return this.baseMapper.selectList(queryWrapper);
    }

    /**
     * 修改批次
     * @param teachProgram
     */
    @Override
    public void updateTeachPrograms(TeachProgram teachProgram) {
        int i = teachProgramMapper.updateTeachPrograms(teachProgram);
        String info;
        if (i > 0) {
            info = teachProgram.getBatchName()+ ">" +teachProgram.getLevel() + ">" +teachProgram.getTypeName() + ">"
                    + teachProgram.getStudyMode() + ">" + teachProgram.getCollegeName() + ">" + teachProgram.getMajorName() + ">"
                    +teachProgram.getSchool() + ">" +teachProgram.getYear() + ">" +teachProgram.getSemester() + ">" +
                    teachProgram.getCourseName();
            teachProgramMapper.updateInfo(teachProgram.getId(),info);
        }

    }




    @Override
    @Transactional(rollbackFor = Exception.class)
    public void createTeachProgram(TeachProgram teachProgram) {
        this.save(teachProgram);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateTeachProgram(TeachProgram teachProgram) {
        this.saveOrUpdate(teachProgram);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteTeachProgram(TeachProgram teachProgram) {
        LambdaQueryWrapper<TeachProgram> wrapper = new LambdaQueryWrapper<>();
	    // TODO 设置删除条件
	    this.remove(wrapper);
	}

    /**
     * 单批次停用
     * @param id
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateStatus(Integer id) {
         teachProgramMapper.updateStatus(id);
    }

    @Override
    public TeachProgram findTeachById(Integer id) {
        return this.baseMapper.findById(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void addNewTeachProgram(TeachProgram teachProgram) {
     teachProgramMapper.addNewTeachProgram(teachProgram);
    }

    @Override
    public Batch selectBatch(String batchName) {

        return teachProgramMapper.selectBatch(batchName);
    }

    @Override
    public List<TeachProgram> selectTeachByBatchId(Integer batchId) {
        return teachProgramMapper.selectBatchByBatchId(batchId);
    }

    @Override
    public List<Batch> selectAllBatch(Batch batch) {
        return teachProgramMapper.selectAllBatch(batch);
    }

    @Override
    public List<Batch> selectMoreBatch(String batchName) {
        return teachProgramMapper.selectMoreBatch(batchName);
    }

    @Override
    public List<TeachProgram> selectBatchByBatchName(String batchName) {
        return teachProgramMapper.selectBatchByBatchName(batchName);
    }


    @Override
    public List<TeachProgram> findLevelNameByCollegeId(Integer collegeId) {
        List<TeachProgram> levelName = teachProgramMapper.findLevelName(collegeId);
        return levelName ;
    }

    @Override
    public List<TeachProgram> findMajoNameByLevelId(Integer levelId) {
        return teachProgramMapper.getMajoName(levelId);
    }

	/**
     * 学院联动专业
     * @param collegeName
     * @return
     */
    @Override
    public List<TeachProgram> findCollegeByMajor(String collegeName) {
        List<TeachProgram> collegeByMajor = baseMapper.findCollegeByMajor(collegeName);
        return collegeByMajor;
    }

}
