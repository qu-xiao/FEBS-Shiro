package cc.zy.base.businesses.service;


import cc.zy.base.businesses.entity.Batch;
import cc.zy.base.businesses.entity.TeachProgram;
import cc.zy.base.common.entity.QueryRequest;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 *  Service接口
 *
 * @author Jiangjinlin
 * @date 2021-01-25 09:38:13
 */
public interface ITeachProgramService extends IService<TeachProgram> {

    public  List<TeachProgram> findLevelNameByCollegeId(Integer collegeId);

    public List<TeachProgram> findMajoNameByLevelId(Integer levelId);
    /**
     * 查询（分页）
     *
     * @param request QueryRequest
     * @param teachProgram teachProgram
     * @return IPage<TeachProgram>
     */
    IPage<TeachProgram> findTeachPrograms(QueryRequest request, TeachProgram teachProgram);

    /**
     * 查询（所有）
     *
     * @param teachProgram tTeachProgram
     * @return List<TTeachProgram>
     */
    List<TeachProgram> findTeachPrograms(TeachProgram teachProgram);

    /**
     * 修改批次
     * @param teachProgram
     */
    void updateTeachPrograms(TeachProgram teachProgram);


    /**
     * 新增
     *
     * @param teachProgram teachProgram
     */
    void createTeachProgram(TeachProgram teachProgram);

    /**
     * 修改
     *
     * @param teachProgram teachProgram
     */
    void updateTeachProgram(TeachProgram teachProgram);

    /**
     * 删除
     *
     * @param teachProgram teachProgram
     */
    void deleteTeachProgram(TeachProgram teachProgram);

    /**
     * 单批次停用
     * @param id
     */
    void updateStatus(Integer id);

    /**
     * 通过ID查找批次详细信息
     *
     * @param id id
     * @return 院校信息
     */
    TeachProgram findTeachById(Integer id);

    /**
     * 批量增加模板教学计划
     * @param teachProgram
     */
    void addNewTeachProgram(TeachProgram teachProgram);


    /**
     * 查询批次
     */
    Batch selectBatch(String batchName);

    /**
     * 根据batchId查询TeachProgram
     * @param batchId
     * @return
     */
    List<TeachProgram> selectTeachByBatchId(Integer batchId);

    List<Batch> selectAllBatch(Batch batch);

    List<Batch> selectMoreBatch( String batchName);
    /**
     * 根据batchName查教学计划
     * @param batchName
     * @return
     */
    List<TeachProgram> selectBatchByBatchName(String batchName);

	/**
     * 学院联动专业
     * @param collegeName
     * @return
     */
    List<TeachProgram> findCollegeByMajor(String collegeName);
}
