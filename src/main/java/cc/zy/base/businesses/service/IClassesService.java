package cc.zy.base.businesses.service;

import cc.zy.base.businesses.entity.Classes;


import cc.zy.base.businesses.entity.TeacherChangeLog;
import cc.zy.base.common.entity.QueryRequest;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 班级表 Service接口
 *
 * @author Jiangjinlin
 * @date 2021-01-25 19:29:08
 */
public interface IClassesService extends IService<Classes> {
    /**
     * 查询（分页）
     *
     * @param request QueryRequest
     * @param classes classes
     * @return IPage<Classes>
     */
    IPage<Classes> findClassess(QueryRequest request, Classes classes);

    /**
     * 查询（所有）
     *
     * @param classes classes
     * @return List<Classes>
     */
    List<Classes> findClassess(Classes classes);

    /**
     * 新增
     *
     * @param classes classes
     */
    void createClasses(Classes classes);

    void createClass(Classes classes);

    /**
     * 修改
     *
     * @param classes classes
     */
    void updateClasses(Classes classes);

    /**
     * 删除
     *
     * @param classes classes
     */
    void deleteClasses(Classes classes);

	/**
     * 删除
     *
     * @param classIds
     */
    void deleteClass(String[] classIds);

	/**
     * 通过ID查找班级信息
     *
     * @param id id
     * @return 班级信息
     */
    Classes findById(Integer id);

	/**
     *  更换班主任
     * @param teacherChangeLog 更换班主任日志表对象
     */
    void updateTeacher(TeacherChangeLog teacherChangeLog);

	/**
     * 根据班级id查询班级
     *
     * @param classesId
     * @return
     */
    Classes findClassesById(@Param("classesId") Integer classesId);

	/**
     * 根据班级id查询班主任更换记录
     *
     * @param classesId
     * @return
     */
    IPage<TeacherChangeLog> findTeacherChangeLogByClassesId(QueryRequest request, @Param("classesId")Integer classesId);

}
