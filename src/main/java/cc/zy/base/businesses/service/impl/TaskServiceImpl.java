package cc.zy.base.businesses.service.impl;


import cc.zy.base.businesses.entity.Task;
import cc.zy.base.businesses.mapper.TaskMapper;
import cc.zy.base.businesses.service.ITaskService;
import cc.zy.base.common.entity.QueryRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Propagation;
import lombok.RequiredArgsConstructor;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.List;

/**
 *  Service实现
 *
 * @author zzz
 * @date 2021-01-28 17:29:15
 */
@Service
@RequiredArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class TaskServiceImpl extends ServiceImpl<TaskMapper, Task> implements ITaskService {

    private final TaskMapper taskMapper;

    @Override
    public IPage<Task> findTasks(QueryRequest request, Task task) {
        LambdaQueryWrapper<Task> queryWrapper = new LambdaQueryWrapper<>();
        // TODO 设置查询条件
        Page<Task> page = new Page<>(request.getPageNum(), request.getPageSize());
        return this.page(page, queryWrapper);
    }

    @Override
    public List<Task> findTasks(Task task) {
	    LambdaQueryWrapper<Task> queryWrapper = new LambdaQueryWrapper<>();
		// TODO 设置查询条件
		return this.baseMapper.selectList(queryWrapper);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void createTask(Task task) {
        this.save(task);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateTask(Task task) {
        this.saveOrUpdate(task);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteTask(Task task) {
        LambdaQueryWrapper<Task> wrapper = new LambdaQueryWrapper<>();
	    // TODO 设置删除条件
	    this.remove(wrapper);
	}

    @Override
    public int findTotalTaskByStuId(Integer userId) {
        return taskMapper.findTotalTaskByStuId(userId);
    }
}
