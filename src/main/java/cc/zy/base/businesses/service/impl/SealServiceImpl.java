package cc.zy.base.businesses.service.impl;


import cc.zy.base.businesses.entity.College;
import cc.zy.base.businesses.entity.Seal;
import cc.zy.base.businesses.mapper.SealMapper;
import cc.zy.base.businesses.service.ISealService;
import cc.zy.base.common.entity.FebsConstant;
import cc.zy.base.common.entity.QueryRequest;
import cc.zy.base.common.utils.SortUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Propagation;
import lombok.RequiredArgsConstructor;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.List;
import java.util.Map;

/**
 *  Service实现
 *
 * @author Jiangjinlin
 * @date 2021-01-27 09:56:39
 */
@Service
@RequiredArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class SealServiceImpl extends ServiceImpl<SealMapper, Seal> implements ISealService {

    private final SealMapper sealMapper;

    @Override
    public IPage<College> findSeals(QueryRequest request, College college) {
        LambdaQueryWrapper<College> queryWrapper = new LambdaQueryWrapper<>();
        // TODO 设置查询条件
        Page<College> page = new Page<>(request.getPageNum(), request.getPageSize());
        page.setSearchCount(false);
        page.setTotal(baseMapper.countSealDetail(college));
        SortUtil.handlePageSort(request, page, "id", FebsConstant.ORDER_ASC, false);
        return this.baseMapper.findSealDetailPage(page, college);
    }

    @Override
    public List<Seal> findSeals(Seal seal) {
	    LambdaQueryWrapper<Seal> queryWrapper = new LambdaQueryWrapper<>();
		// TODO 设置查询条件
		return this.baseMapper.selectList(queryWrapper);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void createSeal(Seal seal) {
        this.save(seal);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateSeal(Seal seal) {
        this.saveOrUpdate(seal);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteSeal(Seal seal) {
        LambdaQueryWrapper<Seal> wrapper = new LambdaQueryWrapper<>();
	    // TODO 设置删除条件
	    this.remove(wrapper);
	}

    @Override
    public College findSealById(Integer id) {

        return this.baseMapper.findById(id);
    }

    /**
     * 更改印章表中IMG_URL列为空
     * @param collegeId
     */
    @Override

    public void deleteSeal(String collegeId) {
        int id = Integer.parseInt(collegeId);
        this.baseMapper.updateSealImgurlById(id);
    }
}
