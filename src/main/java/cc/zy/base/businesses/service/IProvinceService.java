package cc.zy.base.businesses.service;

import cc.zy.base.businesses.entity.Province;


import cc.zy.base.common.entity.QueryRequest;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 *  Service接口
 *
 * @author Jiangjinlin
 * @date 2021-01-26 11:49:58
 */
public interface IProvinceService extends IService<Province> {


    /**
     * 查询（所有）
     *
     *
     * @return List<Province>
     */
    List<Province> findProvinces(Integer pid);

    Integer findProvinceId(String name);

}
