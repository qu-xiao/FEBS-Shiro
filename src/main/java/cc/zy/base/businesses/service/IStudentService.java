package cc.zy.base.businesses.service;

import cc.zy.base.businesses.entity.StuLabel;
import cc.zy.base.businesses.entity.Student;


import cc.zy.base.common.entity.QueryRequest;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 *  Service接口
 *
 * @author Jiangjinlin
 * @date 2021-01-25 11:45:25
 */
public interface IStudentService extends IService<Student> {
    //查找学生批次
    IPage<Student> selectStudentBatchPage(Student student, QueryRequest request);

    /**
     * 通用组件选择用户信息
     *
     * @param request request
     * @param student    用户对象，用于传递查询条件
     * @return IPage
     */
    IPage<Student> selectStudentDetailList(Student student, QueryRequest request);
    /**
     * 查询（分页）
     *
     * @param request QueryRequest
     * @param student student
     * @return IPage<Student>
     */
    IPage<Student> findStudents(QueryRequest request, Student student);

    /**
     * 查询（所有）
     *
     * @param student student
     * @return List<Student>
     */
    List<Student> findStudents(Student student);

    /**
     * 新增
     *
     * @param student student
     */
    void createStudent(Student student);

    /**
     * 修改
     *
     * @param student student
     */
    void updateStudent(Student student);

    /**
     * 删除
     *
     * @param student student
     */
    void deleteStudent(Student student);

	/**
     * 查询（单个学生）
     *
     * @return IPage<Student>
     */
    Student findStudentById(Integer id);

    /**
     * 通过学生Id和类型驳回审核信息
     *
     * @return IPage<Student>
     */
    Boolean rejectTask(Integer id,Integer typeId,String remark);

    /**
     * 通过学生Id和类型通过审核信息
     *
     * @param student Student
     * @return IPage<Student>
     */
    Boolean passedTask(Integer id,Integer typeId,Student student);

    /**
     * 通过ID查找学生详细信息
     *
     * @param id id
     * @return 学生信息
     */
    Student findById(Integer id);

	/**
     * 通过ID查找学生提交的待审核信息
     *
     * @param id id
     * @return 学生信息
     */
    Map<String,Object> findTaskDetail(Integer id,Integer typeId);

	void deleteStudents(String[] collegeIds);

    void creatStuLable(StuLabel stuLabel);

    List<StuLabel> checkYourLabel(Long id);

    void setRecycleStatus(int id);

    void insertIntoRecycle(Student student);

    Student selectStuForRecycle(int id);
    void delLable(Integer id);

    //考试地点审核通过
    Boolean passExamLocation(Integer taskId, Integer stuId, Integer examLocationId);

    //考试地点审核不通过
    Boolean noPassExamLocation(Integer taskId, String remark);

    //查找学生的待办任务
    /**
     * 通过班级id查找该班学生id数组
     *
     * @return 学生信息
     */
    Integer[] findStuIdBuClassId(String classId);

    Map<String,Object> findTaskByStuidAndType(Integer stuId, Integer typeId);

	/**
     * 更新学生异动状态
     *
     * @param student
     * @return
     */
    int updateStudentByEntity(Student student);



}
