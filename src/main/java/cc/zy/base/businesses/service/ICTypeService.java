package cc.zy.base.businesses.service;

import cc.zy.base.businesses.entity.CType;


import cc.zy.base.businesses.entity.CTypeTree;
import cc.zy.base.common.entity.QueryRequest;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 *  Service接口
 *
 * @author Jiangjinlin
 * @date 2021-01-25 10:44:35
 */
public interface ICTypeService extends IService<CType> {
    /**
     * 查询（分页）
     *
     * @param request QueryRequest
     * @param cType cType
     * @return IPage<CType>
     */
    IPage<CType> findCTypes(QueryRequest request, CType cType);

    /**
     * 查询（所有）
     *
     * @param cType cType
     * @return List<CType>
     */
    List<CType> findCTypes(CType cType);

    /**
     * 新增
     *
     * @param cType cType
     */
    void createCType(CType cType);

    /**
     * 修改
     *
     * @param cType cType
     */
    void updateCType(CType cType);

    /**
     * 删除
     *
     * @param cType cType
     */
    void deleteCType(CType cType);


    /**
     * 获取类别树（下拉选使用）
     *
     * @return 部门树集合
     */
    List<CTypeTree<CType>> findCType();

    /**
     * 获取类别列表（树形列表）
     *
     * @param CType 部门对象（传递查询参数）
     * @return 类别树
     */
    List<CTypeTree<CType>> findCType(CType cType);
    void updateByIdStatus(int id);

    void updateByIdStatusUp(int id);
    /**
     * 排序
     */
    void updateSort(int id,int sort1,int sort2,int sort3);

    /**
     * 通过ID查找
     *
     * @param id id
     * @return 院校信息
     */
    CType findById(int id);
    List<CTypeTree<CType>> selectCTypeList1();

}
