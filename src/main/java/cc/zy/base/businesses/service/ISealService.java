package cc.zy.base.businesses.service;

import cc.zy.base.businesses.entity.College;
import cc.zy.base.businesses.entity.Seal;


import cc.zy.base.common.entity.QueryRequest;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 *  Service接口
 *
 * @author Jiangjinlin
 * @date 2021-01-27 09:56:39
 */
public interface ISealService extends IService<Seal> {
    /**
     * 查询（分页）
     *
     * @param request QueryRequest
     * @param seal seal
     * @return IPage<Seal>
     */
    IPage<College> findSeals(QueryRequest request, College college);

    /**
     * 查询（所有）
     *
     * @param seal seal
     * @return List<Seal>
     */
    List<Seal> findSeals(Seal seal);

    /**
     * 新增
     *
     * @param seal seal
     */
    void createSeal(Seal seal);

    /**
     * 修改
     *
     * @param seal seal
     */
    void updateSeal(Seal seal);

    /**
     * 删除
     *
     * @param seal seal
     */
    void deleteSeal(Seal seal);

    /**
     * 根据id查询该印章的相关信息
     * @param id
     * @return
     */
    College findSealById(Integer id);

    /**
     * 更改印章表中IMG_URL列为空
     * @param collegeId
     */
    void deleteSeal(String collegeId);
}
