package cc.zy.base.businesses.controller;


import cc.zy.base.businesses.entity.NoticeUser;
import cc.zy.base.businesses.service.INoticeUserService;
import cc.zy.base.common.annotation.ControllerEndpoint;
import cc.zy.base.common.controller.BaseController;
import cc.zy.base.common.entity.FebsConstant;
import cc.zy.base.common.entity.FebsResponse;
import cc.zy.base.common.entity.QueryRequest;
import cc.zy.base.common.utils.FebsUtil;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.wuwenze.poi.ExcelKit;
import lombok.extern.slf4j.Slf4j;
import lombok.RequiredArgsConstructor;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 通知人表 Controller
 *
 * @author Jiangjinlin
 * @date 2021-01-27 14:14:52
 */
@Slf4j
@Validated
@Controller
@RequiredArgsConstructor
@RequestMapping("noticeUser")
public class NoticeUserController extends BaseController {
    @Resource
    private final INoticeUserService noticeUserService;

    /**
     * 阅读详情
     * @param request
     * @param noticeUser
     * @param noticeId
     * @return
     */
    @GetMapping("statistics/{noticeId}")
    @ResponseBody
//    @RequiresPermissions("noticeUser:list")
    public FebsResponse noticeUserList(QueryRequest request, NoticeUser noticeUser,@PathVariable Integer noticeId) {
        Map<String, Object> dataTable = getDataTable(this.noticeUserService.findNoticeUsers(request, noticeUser,noticeId));
        return new FebsResponse().success().data(dataTable);
    }

    /**
     * 获取应读人数
     * @param noticeId
     * @return
     */
    @GetMapping("noticeUserCount")
    @ResponseBody
    public Map<String, Object> noticeUserCount(Integer noticeId){

        Map<String, Object> resultMap = new HashMap<>();
        long count = noticeUserService.countNoticeUser(noticeId);
        int readCount = noticeUserService.countNoticeUserRead(noticeId);
        if(count >0 && readCount>0){
            int countNoticeUser = (int)count;
            resultMap.put("count",countNoticeUser);
            resultMap.put("readCount",readCount);
        }else {
            resultMap.put("flag",false);
        }

        return resultMap;
    }





//    @ControllerEndpoint(operation = "修改NoticeUser", exceptionMessage = "导出Excel失败")
//    @PostMapping("noticeUser/excel")
//    @ResponseBody
//    @RequiresPermissions("noticeUser:export")
//    public void export(QueryRequest queryRequest, NoticeUser noticeUser, HttpServletResponse response) {
//        List<NoticeUser> noticeUsers = this.noticeUserService.findNoticeUsers(queryRequest, noticeUser).getRecords();
//        ExcelKit.$Export(NoticeUser.class, response).downXlsx(noticeUsers, false);
//    }
}
