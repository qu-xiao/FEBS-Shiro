package cc.zy.base.businesses.controller;


import cc.zy.base.businesses.entity.College;
import cc.zy.base.businesses.entity.DistinctReqResult;
import cc.zy.base.businesses.entity.StudentPool;
import cc.zy.base.businesses.service.IStudentPoolService;
import cc.zy.base.common.annotation.ControllerEndpoint;
import cc.zy.base.common.controller.BaseController;
import cc.zy.base.common.entity.FebsConstant;
import cc.zy.base.common.entity.FebsResponse;
import cc.zy.base.common.entity.QueryRequest;
import cc.zy.base.common.utils.FebsUtil;
import com.wuwenze.poi.ExcelKit;
import lombok.extern.slf4j.Slf4j;
import lombok.RequiredArgsConstructor;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *  Controller
 *
 * @author Jiangjinlin
 * @date 2021-01-25 10:55:03
 */
@Slf4j
@Validated
@Controller
@RequiredArgsConstructor
@RequestMapping("studentpool")
public class StudentPoolController extends BaseController {

    private  final IStudentPoolService studentPoolService;

    @GetMapping(FebsConstant.VIEW_PREFIX + "studentPool")
    public String studentPoolIndex(){
        return FebsUtil.view("studentPool/studentPool");
    }

    @GetMapping("studentPool")
    @ResponseBody
    @RequiresPermissions("studentPool:list")
    public FebsResponse getAllStudentPools(StudentPool studentPool) {
        return new FebsResponse().success().data(studentPoolService.findStudentPools(studentPool));
    }

    @GetMapping("list")
    @ResponseBody
  //  @RequiresPermissions("studentPool:list")
    public FebsResponse studentPoolList(QueryRequest request, StudentPool studentPool) {
        Map<String, Object> dataTable = getDataTable(this.studentPoolService.findStudentPools(request, studentPool));
        return new FebsResponse().success().data(dataTable);
    }

    @ControllerEndpoint(operation = "新增StudentPool", exceptionMessage = "新增StudentPool失败")
    @PostMapping("studentPool")
    @ResponseBody
    @RequiresPermissions("studentPool:add")
    public FebsResponse addStudentPool(@Valid StudentPool studentPool) {
        this.studentPoolService.createStudentPool(studentPool);
        return new FebsResponse().success();
    }

    @ControllerEndpoint(operation = "删除StudentPool", exceptionMessage = "删除StudentPool失败")
    @GetMapping("studentPool/delete")
    @ResponseBody
    @RequiresPermissions("studentPool:delete")
    public FebsResponse deleteStudentPool(StudentPool studentPool) {
        this.studentPoolService.deleteStudentPool(studentPool);
        return new FebsResponse().success();
    }

    @ControllerEndpoint(operation = "修改StudentPool", exceptionMessage = "修改StudentPool失败")
    @PostMapping("studentPool/update")
    @ResponseBody
    @RequiresPermissions("studentPool:update")
    public FebsResponse updateStudentPool(StudentPool studentPool) {
        this.studentPoolService.updateStudentPool(studentPool);
        return new FebsResponse().success();
    }

    @ControllerEndpoint(operation = "修改StudentPool", exceptionMessage = "导出Excel失败")
    @PostMapping("studentPool/excel")
    @ResponseBody
    @RequiresPermissions("studentPool:export")
    public void export(QueryRequest queryRequest, StudentPool studentPool, HttpServletResponse response) {
        List<StudentPool> studentPools = this.studentPoolService.findStudentPools(queryRequest, studentPool).getRecords();
        ExcelKit.$Export(StudentPool.class, response).downXlsx(studentPools, false);
    }




    @GetMapping("add")
    @ResponseBody
    @ControllerEndpoint(operation = "新增学生", exceptionMessage = "新增学生失败")
    public FebsResponse addStudentPool(List<DistinctReqResult>  distinctReqResults) {
         List<StudentPool>  studentPool=new ArrayList<StudentPool>();
        if (distinctReqResults!=null){
            for (int i = 0; i <distinctReqResults.size(); i++) {
//                studentPool.get(i).setIdCard();
//                studentPool.get(i).setBatch();
//                studentPool.get(i).setStudyMode();
                 //这三个字段扩展表id查询 distinctReqResults.get(i).getId()
                studentPool.get(i).setReqExtensionId(distinctReqResults.get(i).getId());
                studentPool.get(i).setDistinctReqId(distinctReqResults.get(i).getId());
                studentPool.get(i).setReqInfoId(distinctReqResults.get(i).getReqInfoId());
                studentPool.get(i).setFollowUserId(distinctReqResults.get(i).getFollowUserId());
                studentPool.get(i).setGroupId(distinctReqResults.get(i).getGroupId());
                studentPool.get(i).setName(distinctReqResults.get(i).getName());
                //性别要改为外键
                // studentPool.get(i).setGender(Integer.parseInt(distinctReqResults.get(i).getGender()));
                studentPool.get(i).setBirthday(distinctReqResults.get(i).getBirthday());
                studentPool.get(i).setIsLunarBirthday(distinctReqResults.get(i).getIsLunarBirthday());
                studentPool.get(i).setTitle(distinctReqResults.get(i).getTitle());
                studentPool.get(i).setQq(distinctReqResults.get(i).getQq());
                studentPool.get(i).setMobile(distinctReqResults.get(i).getMobile());
                studentPool.get(i).setPhone(distinctReqResults.get(i).getPhone());
                studentPool.get(i).setFax(distinctReqResults.get(i).getFax());
                studentPool.get(i).setEmail(distinctReqResults.get(i).getEmail());
                studentPool.get(i).setCompany(distinctReqResults.get(i).getCompany());
                studentPool.get(i).setCompanyUrl(distinctReqResults.get(i).getCompanyUrl());
                studentPool.get(i).setCompanyAddress(distinctReqResults.get(i).getCompanyAddress());
                studentPool.get(i).setMemo(distinctReqResults.get(i).getMemo());
                studentPool.get(i).setVocation(distinctReqResults.get(i).getVocation());
                if( distinctReqResults.get(i).getChannel().equals("未知")){
                    //转为未0
                    studentPool.get(i).setChannel(0);
                }else {
                    //直接转化未数字
                    studentPool.get(i).setChannel(Integer.parseInt(distinctReqResults.get(i).getChannel()));
                }
                studentPool.get(i).setPrefecture(distinctReqResults.get(i).getPrefecture());
                //自定义字段
                studentPool.get(i).setFieldInfos(distinctReqResults.get(i).getFieldInfos());
                studentPool.get(i).setCrmId(distinctReqResults.get(i).getCrmId());
                studentPool.get(i).setModifyTime(distinctReqResults.get(i).getModifyTime());
                studentPool.get(i).setContacTtime(distinctReqResults.get(i).getBirthday());
                studentPool.get(i).setCreateTime(distinctReqResults.get(i).getCreateTime());
                studentPool.get(i).setLastFollowUserId(distinctReqResults.get(i).getLastFollowUserId());
                studentPool.get(i).setStep(distinctReqResults.get(i).getStep());
            }
        }
        //新的学生信息集合
        List<StudentPool>  NewStudentPool=new ArrayList<StudentPool>();
        if(studentPool.size()>0){
            for (int i = 0; i <studentPool.size() ; i++) {
                System.out.println(studentPool.get(i));
                 StudentPool ifStudentPool = studentPoolService.ifStudentPool(studentPool.get(i));
                 if (ifStudentPool!=null){
                     //有重复修改扩展表
                     System.out.println("数据库种又");

                 }else {
                     //没有重复加到新集合里面
                     NewStudentPool.add(studentPool.get(i));
                 }
            }
            //一起加到学生公海表中
            if(NewStudentPool.size()>0){
                studentPoolService.AddStudentPoolList(NewStudentPool);
            }
        }
        return new FebsResponse().success();
    }
}
