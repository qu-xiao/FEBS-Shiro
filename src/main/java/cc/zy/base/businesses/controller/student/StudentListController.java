package cc.zy.base.businesses.controller.student;

import cc.zy.base.businesses.controller.ViewController;
import cc.zy.base.businesses.entity.College;
import cc.zy.base.businesses.entity.StuLabel;
import cc.zy.base.businesses.entity.StuLabelContent;
import cc.zy.base.businesses.entity.Student;
import cc.zy.base.businesses.mapper.StudentMapper;
import cc.zy.base.businesses.service.ICollegeService;
import cc.zy.base.businesses.service.IStudentService;
import cc.zy.base.common.annotation.ControllerEndpoint;
import cc.zy.base.common.controller.BaseController;
import cc.zy.base.common.entity.FebsResponse;
import cc.zy.base.common.entity.QueryRequest;
import cc.zy.base.system.entity.User;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import cc.zy.base.common.exception.FebsException;
import cc.zy.base.common.utils.FebsUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONObject;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.lang.reflect.Field;
import java.util.List;
import javax.validation.Valid;
import java.beans.Transient;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Set;

/**
 * Controller
 *
 * @author JiangWeiguang
 * @date 2021\1\22 0022 16:50
 */
@Slf4j
@Validated
@Controller
@RequiredArgsConstructor
@RequestMapping("student")
public class StudentListController extends BaseController {
    private final IStudentService studentsService;
    private final StudentMapper studentMapper;

    @GetMapping("list")
    @ResponseBody
    @RequiresPermissions("student:view")
    public FebsResponse studentList(QueryRequest request, Student student) {
        log.info("返回studentsList方法");
        Map<String, Object> dataTable = getDataTable(this.studentsService.findStudents(request, student));
        return new FebsResponse().success().data(dataTable);
    }

    @GetMapping("moveToRecycle/{studentIds}")
    @ControllerEndpoint(operation = "移入回收站", exceptionMessage = "移入回收站失败")
    @ResponseBody
    public FebsResponse moveToRecycle(@PathVariable String studentIds) {
        log.info("移入回收站的id" + studentIds);
        String[] ids = studentIds.split(StringPool.COMMA);
        int[] arr = new int[ids.length];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = Integer.parseInt(ids[i]);
            log.info(arr[i] + "");
        }
        for (int i : arr) {
            this.studentsService.setRecycleStatus(i);
        }
        for (int i : arr) {
            this.studentsService.insertIntoRecycle(studentsService.selectStuForRecycle(i));
        }

        return new FebsResponse().success();
    }

    @PostMapping("affirm/rejectIden")
    @ControllerEndpoint(operation = "驳回身份证审核", exceptionMessage = "驳回身份证失败")
    @ResponseBody
    public FebsResponse rejectIden(@Valid Integer id, Integer typeId, String remark) {
        if (id == null) {
            throw new FebsException("学生ID为空");
        } else {
            Boolean b = this.studentsService.rejectTask(id, typeId, remark);
            if (b) {
                return new FebsResponse().success();
            } else {
                return new FebsResponse().fail();
            }
        }
    }

    @PostMapping("affirm/agreeIdentity")
    @ControllerEndpoint(operation = "通过身份证审核", exceptionMessage = "通过身份证失败")
    @ResponseBody
    @Transactional
    public FebsResponse agreeIdentity(@Valid Integer id, Integer typeId, String idenFront,
                                      String idenBack, String identityFrontN, String identityContraryN) {
        if (id == null) {
            throw new FebsException("学生ID为空");
        } else {
            Student student = new Student();
            String[] iden = identityContraryN.split("至");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            try {
                Date dateBegin = sdf.parse(iden[0]);
                Date dateEnd = sdf.parse(iden[1]);
                student.setIdenBeginDate(dateBegin);
                student.setIdecEndDate(dateEnd);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            student.setId(id);
            student.setIdFrontImgUrl(idenFront);
            student.setIdBackImgUrl(idenBack);
            student.setIdentity(identityFrontN);
            Boolean task = this.studentsService.passedTask(id, typeId, student);
            if (task) {
                return new FebsResponse().success();
            } else {
                throw new FebsException("审核失败");
            }
        }
    }

    @PostMapping("affirm/rejectPhoto")
    @ControllerEndpoint(operation = "驳回照片审核", exceptionMessage = "驳回照片失败")
    @ResponseBody
    public FebsResponse rejectPhoto(@Valid Integer id, Integer typeId, String remark) {
        if (id == null) {
            throw new FebsException("学生ID为空");
        } else {
            Boolean b = this.studentsService.rejectTask(id, typeId, remark);
            if (b) {
                return new FebsResponse().success();
            } else {
                return new FebsResponse().fail();
            }
        }
    }


    @PostMapping("affirm/agreePhoto")
    @ControllerEndpoint(operation = "通过照片审核", exceptionMessage = "照片审核失败")
    @ResponseBody
    public FebsResponse agreePhoto(@Valid Integer id, Integer typeId, String photo) {
        if (id == null) {
            throw new FebsException("学生ID为空");
        } else {
            Student student = new Student();
            student.setId(id);
            student.setHeadImgUrl(photo);
            Boolean task = this.studentsService.passedTask(id, typeId, student);
            if (task) {
                return new FebsResponse().success();
            } else {
                return new FebsResponse().fail();
            }
        }
    }

    @PostMapping("createLabel")
    @ResponseBody
    @ControllerEndpoint(operation = "创建标签", exceptionMessage = "创建标签失败")
    public FebsResponse createLabel(StuLabelContent stuLabelContent) throws IllegalAccessException, InstantiationException {
        log.info("创建标签" + stuLabelContent.toString());
        User user = getCurrentUser();
        JSONObject obj = JSONObject.fromObject(stuLabelContent);
        String jsonContent = obj.toString();
        log.info(jsonContent);
        StuLabel stuLabel = new StuLabel();
        stuLabel.setCreateUserId(user.getUserId().intValue());
        stuLabel.setLabelName(stuLabelContent.getMediaLabelName());
        stuLabel.setConditionContent(jsonContent);
        this.studentsService.creatStuLable(stuLabel);
        return new FebsResponse().success();
    }

    @GetMapping("checkYourLabel")
    @ResponseBody
    @ControllerEndpoint(operation = "查询标签", exceptionMessage = "查询标签失败")
    public List<StuLabel> checkYourLabel() {
        User user = getCurrentUser();
        return studentsService.checkYourLabel(user.getUserId());
    }

    @DeleteMapping("delLabel")
    @ResponseBody
    @ControllerEndpoint(operation = "删除标签", exceptionMessage = "删除标签失败")
    public FebsResponse delYourLabel(Integer id) {
        log.info("删除标签id" + id);
        studentsService.delLable(id);
        return new FebsResponse().success();
    }
}
