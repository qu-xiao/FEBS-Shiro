package cc.zy.base.businesses.controller;

import cc.zy.base.businesses.entity.Batch;
import cc.zy.base.businesses.service.IBatchService;
import cc.zy.base.common.annotation.ControllerEndpoint;
import cc.zy.base.common.controller.BaseController;
import cc.zy.base.common.entity.FebsConstant;
import cc.zy.base.common.entity.FebsResponse;
import cc.zy.base.common.entity.QueryRequest;
import cc.zy.base.common.utils.FebsUtil;
import com.wuwenze.poi.ExcelKit;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@Slf4j
@Validated
@Controller
@RequiredArgsConstructor
@RequestMapping("batch")
public class BatchController extends BaseController {
    @Resource
    private IBatchService iBatchService;

    @GetMapping("getList")
    @ResponseBody
    public FebsResponse getBatchlist(){
        List<Batch> batchs = iBatchService.findBatchs();
        return new FebsResponse().success().data(batchs);
    }

    @GetMapping("list2")
    public FebsResponse getBatchlistById(Integer batchName) {
        Batch batch = iBatchService.findBatchsById(batchName);
        return new FebsResponse().success().data(batch);
    }
	 @GetMapping(FebsConstant.VIEW_PREFIX + "batch")
    public String batchIndex(){
        return FebsUtil.view("batch/batch");
    }

    @GetMapping("batch")
    @ResponseBody
//    @RequiresPermissions("batch:list")
    public FebsResponse getAllBatchs(Batch batch) {
        return new FebsResponse().success().data(iBatchService.findBatchs(batch));
    }

    @GetMapping("list")
    @ResponseBody
//    @RequiresPermissions("batch:list")
    public FebsResponse batchList(QueryRequest request, Batch batch) {
        Map<String, Object> dataTable = getDataTable(this.iBatchService.findBatchs(request, batch));
        return new FebsResponse().success().data(dataTable);
    }

    @ControllerEndpoint(operation = "Batch", exceptionMessage = "Batch")
    @PostMapping("batch")
    @ResponseBody
//    @RequiresPermissions("batch:add")
    public FebsResponse addBatch(@Valid Batch batch) {
        this.iBatchService.createBatch(batch);
        return new FebsResponse().success();
    }

    @ControllerEndpoint(operation = "Batch", exceptionMessage = "Batch")
    @GetMapping("batch/delete")
    @ResponseBody
//    @RequiresPermissions("batch:delete")
    public FebsResponse deleteBatch(Batch batch) {
        this.iBatchService.deleteBatch(batch);
        return new FebsResponse().success();
    }

    @ControllerEndpoint(operation = "", exceptionMessage = "Batch")
    @PostMapping("batch/update")
    @ResponseBody
//    @RequiresPermissions("batch:update")
    public FebsResponse updateBatch(Batch batch) {
        this.iBatchService.updateBatch(batch);
        return new FebsResponse().success();
    }

    @ControllerEndpoint(operation = "Batch", exceptionMessage = "Excel")
    @PostMapping("batch/excel")
    @ResponseBody
//    @RequiresPermissions("batch:export")
    public void export(QueryRequest queryRequest, Batch batch, HttpServletResponse response) {
        List<Batch> batchs = this.iBatchService.findBatchs(queryRequest, batch).getRecords();
        ExcelKit.$Export(Batch.class, response).downXlsx(batchs, false);
    }

}
