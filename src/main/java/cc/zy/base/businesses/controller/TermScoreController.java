package cc.zy.base.businesses.controller;


import cc.zy.base.businesses.entity.*;
import cc.zy.base.businesses.mapper.TestSubjectMapper;
import cc.zy.base.businesses.service.IEntranceScoreService;
import cc.zy.base.businesses.service.ILevelService;
import cc.zy.base.businesses.service.ISubjectCategoryService;
import cc.zy.base.businesses.service.ITermScoreService;
import cc.zy.base.common.annotation.ControllerEndpoint;
import cc.zy.base.common.controller.BaseController;
import cc.zy.base.common.entity.FebsResponse;
import cc.zy.base.common.entity.QueryRequest;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wuwenze.poi.ExcelKit;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Slf4j
@Validated
@Controller
@RequiredArgsConstructor
@RequestMapping("termScore")
public class TermScoreController extends BaseController {
    @Resource
    private ITermScoreService iTermScoreService;


    @GetMapping("list")
    @ResponseBody
    public FebsResponse getAll(QueryRequest request, TermScore termScore) {
//        System.out.println(termScore.getCourseName());
        Map<String, Object> dataTable = getDataTable(this.iTermScoreService.getTermScoreList(request,termScore));
        if (dataTable!=null){
            return new FebsResponse().success().data(dataTable);
        }else {
            return new FebsResponse().success().data("");
        }
    }

    @GetMapping("collegeList")
    @ResponseBody
    public FebsResponse getCollegeList() {
        List<College> collegeList = iTermScoreService.getCollegeList();
//        System.out.println(collegeList);
        return new FebsResponse().success().data(collegeList);
    }

    @GetMapping("levelList")
    @ResponseBody
    public FebsResponse getLevelList(Integer collegeId) {
        System.out.println(collegeId);
        List<Level> levelList = iTermScoreService.getLevelListByCollegeId(collegeId);
        System.out.println(levelList);
        return new FebsResponse().success().data(levelList);
    }

    @GetMapping("majorList")
    @ResponseBody
    public FebsResponse getMajorList(Integer collegeId,Integer levelId) {
        System.out.println(collegeId);
        List<Major> majorListByIds = iTermScoreService.getMajorListByIds(collegeId, levelId);
        System.out.println(majorListByIds);
        return new FebsResponse().success().data(majorListByIds);
    }

    @GetMapping("courseList")
    @ResponseBody
    public FebsResponse getCourseList(Integer collegeId,Integer levelId,Integer majorId,Integer termId) {
        System.out.println(collegeId);
        List<TeachProgram> courseNameByIds = iTermScoreService.getCourseNameByIds(collegeId, levelId, majorId,termId);
        System.out.println(courseNameByIds);
        return new FebsResponse().success().data(courseNameByIds);
    }

    @GetMapping("termList")
    @ResponseBody
    public FebsResponse getTermList(Integer levelId) {
        List<CollegeTerm> termsByLevelId = iTermScoreService.getTermsByLevelId(levelId);
        return new FebsResponse().success().data(termsByLevelId);
    }

}
