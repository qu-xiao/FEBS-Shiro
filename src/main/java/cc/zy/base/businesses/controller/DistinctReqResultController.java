package cc.zy.base.businesses.controller;


import cc.zy.base.businesses.entity.DistinctReqResult;

import cc.zy.base.businesses.service.IDistinctReqResultService;
import cc.zy.base.common.annotation.ControllerEndpoint;
import cc.zy.base.common.controller.BaseController;
import cc.zy.base.common.entity.FebsConstant;
import cc.zy.base.common.entity.FebsResponse;
import cc.zy.base.common.entity.QueryRequest;
import cc.zy.base.common.utils.FebsUtil;
import com.wuwenze.poi.ExcelKit;
import lombok.extern.slf4j.Slf4j;
import lombok.RequiredArgsConstructor;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/**
 *  Controller
 *
 * @author peihaodong
 * @date 2021-01-25 20:16:04
 */
@Slf4j
@Validated
@Controller
@RequiredArgsConstructor
public class DistinctReqResultController extends BaseController {

    private final IDistinctReqResultService distinctReqResultService;

    @GetMapping(FebsConstant.VIEW_PREFIX + "distinctReqResult")
    public String distinctReqResultIndex(){
        return FebsUtil.view("distinctReqResult/distinctReqResult");
    }

    @GetMapping("distinctReqResult")
    @ResponseBody
    @RequiresPermissions("distinctReqResult:list")
    public FebsResponse getAllDistinctReqResults(DistinctReqResult distinctReqResult) {
        return new FebsResponse().success().data(distinctReqResultService.findDistinctReqResults(distinctReqResult));
    }

    @GetMapping("distinctReqResult/list")
    @ResponseBody
    @RequiresPermissions("distinctReqResult:list")
    public FebsResponse distinctReqResultList(QueryRequest request, DistinctReqResult distinctReqResult) {
        Map<String, Object> dataTable = getDataTable(this.distinctReqResultService.findDistinctReqResults(request, distinctReqResult));
        return new FebsResponse().success().data(dataTable);
    }

    @ControllerEndpoint(operation = "新增DistinctReqResult", exceptionMessage = "新增DistinctReqResult失败")
    @PostMapping("distinctReqResult")
    @ResponseBody
    @RequiresPermissions("distinctReqResult:add")
    public FebsResponse addDistinctReqResult(@Valid DistinctReqResult distinctReqResult) {
        this.distinctReqResultService.createDistinctReqResult(distinctReqResult);
        return new FebsResponse().success();
    }

    @ControllerEndpoint(operation = "删除DistinctReqResult", exceptionMessage = "删除DistinctReqResult失败")
    @GetMapping("distinctReqResult/delete")
    @ResponseBody
    @RequiresPermissions("distinctReqResult:delete")
    public FebsResponse deleteDistinctReqResult(DistinctReqResult distinctReqResult) {
        this.distinctReqResultService.deleteDistinctReqResult(distinctReqResult);
        return new FebsResponse().success();
    }

    @ControllerEndpoint(operation = "修改DistinctReqResult", exceptionMessage = "修改DistinctReqResult失败")
    @PostMapping("distinctReqResult/update")
    @ResponseBody
    @RequiresPermissions("distinctReqResult:update")
    public FebsResponse updateDistinctReqResult(DistinctReqResult distinctReqResult) {
        this.distinctReqResultService.updateDistinctReqResult(distinctReqResult);
        return new FebsResponse().success();
    }

    @ControllerEndpoint(operation = "修改DistinctReqResult", exceptionMessage = "导出Excel失败")
    @PostMapping("distinctReqResult/excel")
    @ResponseBody
    @RequiresPermissions("distinctReqResult:export")
    public void export(QueryRequest queryRequest, DistinctReqResult distinctReqResult, HttpServletResponse response) {
        List<DistinctReqResult> distinctReqResults = this.distinctReqResultService.findDistinctReqResults(queryRequest, distinctReqResult).getRecords();
        ExcelKit.$Export(DistinctReqResult.class, response).downXlsx(distinctReqResults, false);
    }
}
