package cc.zy.base.businesses.controller;


import cc.zy.base.businesses.entity.Notice;
import cc.zy.base.businesses.service.INoticeService;
import cc.zy.base.common.annotation.ControllerEndpoint;
import cc.zy.base.common.controller.BaseController;
import cc.zy.base.common.entity.FebsConstant;
import cc.zy.base.common.entity.FebsResponse;
import cc.zy.base.common.entity.QueryRequest;
import cc.zy.base.common.utils.FebsUtil;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.wuwenze.poi.ExcelKit;
import lombok.extern.slf4j.Slf4j;
import lombok.RequiredArgsConstructor;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.Map;

/**
 * 通知表 Controller
 *
 * @author Jiangjinlin
 * @date 2021-01-26 11:52:14
 */
@Slf4j
@Validated
@Controller
@RequiredArgsConstructor
@RequestMapping("notice")
public class NoticeController extends BaseController {

    private final INoticeService noticeService;

    @GetMapping(FebsConstant.VIEW_PREFIX + "notice")
    public String noticeIndex(){
        return FebsUtil.view("notice/notice");
    }

    @GetMapping("notice")
    @ResponseBody
    @RequiresPermissions("notice:list")
    public FebsResponse getAllNotices(Notice notice) {
        return new FebsResponse().success().data(noticeService.findNotices(notice));
    }

    @GetMapping("list")
    @ResponseBody
    @RequiresPermissions("notice:list")
    public FebsResponse noticeList(QueryRequest request, Notice notice) {
        Map<String, Object> dataTable = getDataTable(this.noticeService.findNotices(request, notice));
        return new FebsResponse().success().data(dataTable);
    }

    /**
     * 通知列表
     * @param notice
     * @param request
     * @return
     */
    @GetMapping("foundNoticeSelect")
    @ResponseBody
    public FebsResponse foundNoticeSelect(Notice notice, QueryRequest request) {
        Map<String, Object> dataTable = getDataTable(this.noticeService.findNoticesDetailList(notice, request));
        return new FebsResponse().success().data(dataTable);
    }

    @ControllerEndpoint(operation = "新增Notice", exceptionMessage = "新增Notice失败")
    @PostMapping("notice")
    @ResponseBody
    @RequiresPermissions("notice:add")
    public FebsResponse addNotice(@Valid Notice notice) {
        this.noticeService.createNotice(notice);
        return new FebsResponse().success();
    }

    @ControllerEndpoint(operation = "删除Notice", exceptionMessage = "删除Notice失败")
    @GetMapping("delete")
    @ResponseBody
    @RequiresPermissions("notice:delete")
    public FebsResponse deleteNotice(Notice notice) {
        this.noticeService.deleteNotice(notice);
        return new FebsResponse().success();
    }

    @ControllerEndpoint(operation = "修改Notice", exceptionMessage = "修改Notice失败")
    @PostMapping("update")
    @ResponseBody
    @RequiresPermissions("notice:update")
    public FebsResponse updateNotice(Notice notice) {
        this.noticeService.updateNotice(notice);
        return new FebsResponse().success();
    }

    @ControllerEndpoint(operation = "修改Notice", exceptionMessage = "导出Excel失败")
    @PostMapping("excel")
    @ResponseBody
    @RequiresPermissions("notice:export")
    public void export(QueryRequest queryRequest, Notice notice, HttpServletResponse response) {
        List<Notice> notices = this.noticeService.findNotices(queryRequest, notice).getRecords();
        ExcelKit.$Export(Notice.class, response).downXlsx(notices, false);
    }

    @GetMapping("deleteById/{noticeId}")
    @ResponseBody
    public FebsResponse deleteNoticeById(@NotBlank(message = "{required}") @PathVariable String noticeId) {
        this.noticeService.deleteNoticeById(Integer.parseInt(noticeId));
        return new FebsResponse().success();
    }
}
