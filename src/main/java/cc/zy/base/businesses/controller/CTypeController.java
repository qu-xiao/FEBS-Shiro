package cc.zy.base.businesses.controller;


import cc.zy.base.businesses.entity.CType;
import cc.zy.base.businesses.entity.CTypeTree;
import cc.zy.base.businesses.service.ICTypeService;
import cc.zy.base.common.annotation.ControllerEndpoint;
import cc.zy.base.common.controller.BaseController;
import cc.zy.base.common.entity.DeptTree;
import cc.zy.base.common.entity.FebsConstant;
import cc.zy.base.common.entity.FebsResponse;
import cc.zy.base.common.entity.QueryRequest;
import cc.zy.base.common.exception.FebsException;
import cc.zy.base.common.utils.FebsUtil;
import cc.zy.base.system.entity.Dept;
import com.wuwenze.poi.ExcelKit;
import lombok.extern.slf4j.Slf4j;
import lombok.RequiredArgsConstructor;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/**
 *  Controller
 *
 * @author Jiangjinlin
 * @date 2021-01-25 10:44:35
 */
@Slf4j
@Validated
@Controller
@RequiredArgsConstructor
@RequestMapping("cType")
public class CTypeController extends BaseController {

    private final ICTypeService cTypeService;

    @GetMapping(FebsConstant.VIEW_PREFIX + "cType")
    public String cTypeIndex(){
        return FebsUtil.view("cType/cType");
    }

    @GetMapping("list")
    @ResponseBody
    public FebsResponse cTypeList(QueryRequest request, CType cType) {
        Map<String, Object> dataTable = getDataTable(this.cTypeService.findCTypes(request, cType));
        return new FebsResponse().success().data(dataTable);
    }
    @GetMapping("select/tree")
    @ResponseBody
    @ControllerEndpoint(exceptionMessage = "获取类别树失败")
    public FebsResponse getCTypeTree() throws FebsException {
        List<CTypeTree<CType>> cTypeTrees = this.cTypeService.findCType();
        return new FebsResponse().success().data(cTypeTrees);
    }

    @GetMapping("tree")
    @ResponseBody
    @ControllerEndpoint(exceptionMessage = "获取类别树失败")
    public FebsResponse getCTypeTree( CType cType) throws FebsException {

        List<CTypeTree<CType>> cTypeTrees = this.cTypeService.findCType(cType);
        return new FebsResponse().success().data(cTypeTrees);
    }



    @ControllerEndpoint(operation = "删除CType", exceptionMessage = "删除CType失败")
    @GetMapping("delete")
    @ResponseBody
    @RequiresPermissions("cType:delete")
    public FebsResponse deleteCType(CType cType) {
        this.cTypeService.deleteCType(cType);
        return new FebsResponse().success();
    }

    @ControllerEndpoint(operation = "修改CType", exceptionMessage = "修改CType失败")
    @PostMapping("update")
    @ResponseBody
//    @RequiresPermissions("cType:update")
    public FebsResponse updateCType(CType cType) {
        this.cTypeService.updateCType(cType);
        return new FebsResponse().success();
    }

    @ControllerEndpoint(operation = "修改CType", exceptionMessage = "导出Excel失败")
    @PostMapping("cType/excel")
    @ResponseBody
    @RequiresPermissions("cType:export")
    public void export(QueryRequest queryRequest, CType cType, HttpServletResponse response) {
        List<CType> cTypes = this.cTypeService.findCTypes(queryRequest, cType).getRecords();
        ExcelKit.$Export(CType.class, response).downXlsx(cTypes, false);
    }
    @GetMapping("updateByIdSatus")
    @ResponseBody
    public FebsResponse updateByIdSatus(int id) {
        System.out.println(id+"________");
        this.cTypeService.updateByIdStatus(id);
        return new FebsResponse().success();
    }
    @GetMapping("updateByIdSatusUp")
    @ResponseBody
    public FebsResponse updateByIdSatusUp(int id) {

        this.cTypeService.updateByIdStatusUp(id);
        return new FebsResponse().success();
    }
    @GetMapping("updateSort")
    @ResponseBody
    public FebsResponse updateSort(int id,int sort1,int sort2,int sort3) {
        System.out.println(id+"___"+sort2+"_____"+"_____"+sort3);
        this.cTypeService.updateSort(id,sort1,sort2,sort3);
        return new FebsResponse().success();
    }

    @ControllerEndpoint(operation = "新增CType", exceptionMessage = "新增CType失败")
    @PostMapping("add")
    @ResponseBody
    public FebsResponse addCType(@Valid CType cType) {
        System.out.println(cType.getInfo());
        this.cTypeService.createCType(cType);
        return new FebsResponse().success();
    }
    @GetMapping("select/tree1")
    @ResponseBody
    @ControllerEndpoint(exceptionMessage = "获取类别树失败")
    public FebsResponse getCTypeTree1() throws FebsException {
        List<CTypeTree<CType>> cTypeTrees = cTypeService.selectCTypeList1();
        return new FebsResponse().success().data(cTypeTrees);
    }
}
