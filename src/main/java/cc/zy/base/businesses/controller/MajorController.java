package cc.zy.base.businesses.controller;


import cc.zy.base.businesses.entity.College;
import cc.zy.base.businesses.entity.Major;
import cc.zy.base.businesses.service.IMajorService;
import cc.zy.base.common.annotation.ControllerEndpoint;
import cc.zy.base.common.controller.BaseController;
import cc.zy.base.common.entity.FebsConstant;
import cc.zy.base.common.entity.FebsResponse;
import cc.zy.base.common.entity.QueryRequest;
import cc.zy.base.common.utils.FebsUtil;
import com.wuwenze.poi.ExcelKit;
import lombok.extern.slf4j.Slf4j;
import lombok.RequiredArgsConstructor;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/**
 * 专业表 Controller
 *
 * @author Jiangjinlin
 * @date 2021-01-26 19:41:01
 */
@Slf4j
@Validated
@Controller
@RequiredArgsConstructor
@RequestMapping("major")
public class MajorController extends BaseController {
    @Resource
    private final IMajorService majorService;

    @GetMapping(FebsConstant.VIEW_PREFIX + "major")
    public String majorIndex(){
        return FebsUtil.view("major/major");
    }



    @GetMapping("list")
    @ResponseBody
//    @RequiresPermissions("major:view")
    public FebsResponse majorList(QueryRequest request, Major major) {
        Map<String, Object> dataTable = getDataTable(this.majorService.findMajors(request, major));

        return new FebsResponse().success().data(dataTable);
    }

    @ControllerEndpoint(operation = "新增Major", exceptionMessage = "新增Major失败")
    @PostMapping("add")
    @ResponseBody
//    @RequiresPermissions("major:add")
    public FebsResponse addMajor(@Valid Major major) {
        this.majorService.createMajor(major);
        return new FebsResponse().success();
    }


    @ControllerEndpoint(operation = "修改Major", exceptionMessage = "修改Major失败")
    @PostMapping("update")
    @ResponseBody
//    @RequiresPermissions("major:update")
    public FebsResponse updateMajor(Major major) {

        this.majorService.updateMajor(major);
        return new FebsResponse().success();
    }



    @ControllerEndpoint(operation = "修改Major", exceptionMessage = "导出Excel失败")
    @PostMapping("major/excel")
    @ResponseBody
    @RequiresPermissions("major:export")
    public void export(QueryRequest queryRequest, Major major, HttpServletResponse response) {
        List<Major> majors = this.majorService.findMajors(queryRequest, major).getRecords();
        ExcelKit.$Export(Major.class, response).downXlsx(majors, false);
    }

	@GetMapping("findById/{majorId}")
    @ResponseBody
    public FebsResponse findById(@PathVariable Integer majorId) {
        Major major = this.majorService.findById(majorId);
        return new FebsResponse().success().data(major);
    }
}
