package cc.zy.base.businesses.controller;



import cc.zy.base.businesses.entity.College;
import cc.zy.base.businesses.entity.SubjectCategory;
import cc.zy.base.businesses.service.ISubjectCategoryService;
import cc.zy.base.common.annotation.ControllerEndpoint;
import cc.zy.base.common.controller.BaseController;
import cc.zy.base.common.entity.FebsConstant;
import cc.zy.base.common.entity.FebsResponse;
import cc.zy.base.common.entity.QueryRequest;
import cc.zy.base.common.exception.FebsException;
import cc.zy.base.common.utils.FebsUtil;
import com.wuwenze.poi.ExcelKit;
import lombok.extern.slf4j.Slf4j;
import lombok.RequiredArgsConstructor;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.sql.Date;
import java.util.List;
import java.util.Map;

/**
 * 学科类别表 Controller
 *
 * @author Jiangjinlin
 * @date 2021-01-25 19:09:36
 */
@Slf4j
@Validated
@Controller
@RequiredArgsConstructor
@RequestMapping("subjectCategory")
public class SubjectCategoryController extends BaseController {
    @Resource
    private final ISubjectCategoryService subjectCategoryService;

    @GetMapping(FebsConstant.VIEW_PREFIX + "subjectCategory")
    public String subjectCategoryIndex(){
        return FebsUtil.view("subjectCategory/subjectCategory");
    }



    @GetMapping("list")
    @ResponseBody
//    @RequiresPermissions("subjectCategory:view")
    public FebsResponse subjectCategoryList(QueryRequest request, SubjectCategory subjectCategory) {
        Map<String, Object> dataTable = getDataTable(this.subjectCategoryService.findSubjectCategorys(request, subjectCategory));
        return new FebsResponse().success().data(dataTable);
    }



    @PostMapping("add")
    @ResponseBody
//    @RequiresPermissions("subjectCategory:add")
    @ControllerEndpoint(operation = "新增院校", exceptionMessage = "新增院校失败")
    public FebsResponse addSubjectCategory(@Valid SubjectCategory subjectCategory) {

        this.subjectCategoryService.createSubjectCategory(subjectCategory);
        return new FebsResponse().success();
    }

    @ControllerEndpoint(operation = "修改SubjectCategory", exceptionMessage = "修改SubjectCategory失败")
    @PostMapping("update")
    @ResponseBody
    public FebsResponse updateSubjectCategory(@Valid SubjectCategory subjectCategory) {
        System.out.println("===");
        System.out.println(subjectCategory.getId());
        if(subjectCategory.getId() ==null){
            throw new FebsException("院校类别ID为空");

        }
        this.subjectCategoryService.updateSubjectCategory(subjectCategory);
        return new FebsResponse().success();
    }

    @ControllerEndpoint(operation = "修改SubjectCategory", exceptionMessage = "导出Excel失败")
    @PostMapping("subjectCategory/excel")
    @ResponseBody
    @RequiresPermissions("subjectCategory:export")
    public void export(QueryRequest queryRequest, SubjectCategory subjectCategory, HttpServletResponse response) {
        List<SubjectCategory> subjectCategorys = this.subjectCategoryService.findSubjectCategorys(queryRequest, subjectCategory).getRecords();
        ExcelKit.$Export(SubjectCategory.class, response).downXlsx(subjectCategorys, false);
    }
}
