package cc.zy.base.businesses.controller;


import cc.zy.base.businesses.entity.Video;

import cc.zy.base.businesses.service.IVideoService;
import cc.zy.base.common.annotation.ControllerEndpoint;
import cc.zy.base.common.controller.BaseController;
import cc.zy.base.common.entity.FebsConstant;
import cc.zy.base.common.entity.FebsResponse;
import cc.zy.base.common.entity.QueryRequest;
import cc.zy.base.common.utils.FebsUtil;
import com.wuwenze.poi.ExcelKit;
import lombok.extern.slf4j.Slf4j;
import lombok.RequiredArgsConstructor;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/**
 *  Controller
 *
 * @author Jiangjinlin
 * @date 2021-01-25 11:03:36
 */
@Slf4j
@Validated
@Controller
@RequiredArgsConstructor
@RequestMapping("video")
public class VideoController extends BaseController {
    private final IVideoService videoService;
    @GetMapping(FebsConstant.VIEW_PREFIX + "video")
    public String videoIndex() {
        return FebsUtil.view("video/video");
    }
    @GetMapping("video")
    @ResponseBody
    @RequiresPermissions("video:list")
    public FebsResponse getAllVideos(Video video) {
        return new FebsResponse().success().data(videoService.findVideos(video));
    }
    //分页展示-夏凯
    @GetMapping("list")
    @ResponseBody
    public FebsResponse videoList(QueryRequest request, Video video) {
        Map<String, Object> dataTable = getDataTable(this.videoService.findVideos(request, video));
        return new FebsResponse().success().data(dataTable);
    }
    @ControllerEndpoint(operation = "新增Video", exceptionMessage = "新增Video失败")
    @PostMapping("add")
    @ResponseBody
    public FebsResponse addVideo(@Valid Video video) {
        this.videoService.createVideo(video);
        return new FebsResponse().success();
    }

    @ControllerEndpoint(operation = "删除Video", exceptionMessage = "删除Video失败")
    @GetMapping("video/delete")
    @ResponseBody
    @RequiresPermissions("video:delete")
    public FebsResponse deleteVideo(Video video) {
        this.videoService.deleteVideo(video);
        return new FebsResponse().success();
    }
    @ControllerEndpoint(operation = "修改Video", exceptionMessage = "修改Video失败")
    @PostMapping("update")
    @ResponseBody
//    @RequiresPermissions("video:update")
    public FebsResponse updateVideo(Video video) {
        this.videoService.updateVideo(video);
        return new FebsResponse().success();
    }

    @ControllerEndpoint(operation = "修改Video", exceptionMessage = "导出Excel失败")
    @PostMapping("video/excel")
    @ResponseBody
    @RequiresPermissions("video:export")
    public void export(QueryRequest queryRequest, Video video, HttpServletResponse response) {
        List<Video> videos = this.videoService.findVideos(queryRequest, video).getRecords();
        ExcelKit.$Export(Video.class, response).downXlsx(videos, false);
    }
    @ControllerEndpoint(operation = "修改成功", exceptionMessage = "修改失败")
    @GetMapping("updateByIdSatus")
    @ResponseBody
    public FebsResponse updateByIdSatus(int id) {
        this.videoService.updateByIdSatus(id);
        return new FebsResponse().success();
    }
    @ControllerEndpoint(operation = "修改成功", exceptionMessage = "修改失败")
    @GetMapping("updateByIdSatusUp")
    @ResponseBody
    public FebsResponse updateByIdSatusUp(int id) {
        System.out.println(id+"________");
        this.videoService.updateByIdSatusUp(id);
        return new FebsResponse().success();
    }
    @ControllerEndpoint(operation = "修改成功", exceptionMessage = "修改失败")
    @GetMapping("updateBySort")
    @ResponseBody
    public FebsResponse updateBySort(int id, int sort,int sort1) {
        this.videoService.updateBySort(id,sort,sort1);
        return new FebsResponse().success();
    }
    @GetMapping("findVideo")
    @ResponseBody
    public FebsResponse findVideo(QueryRequest request,Video video) {
        System.out.println("--------------");
        System.out.println(video.getUpdateName());
        System.out.println(request);
        Map<String, Object> dataTable = getDataTable(this.videoService.findVideo(request,video));
        return new FebsResponse().success().data(dataTable);
    }

}