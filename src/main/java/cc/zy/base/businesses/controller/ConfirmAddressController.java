package cc.zy.base.businesses.controller;


import cc.zy.base.businesses.entity.*;
import cc.zy.base.businesses.service.*;
import cc.zy.base.common.annotation.ControllerEndpoint;
import cc.zy.base.common.controller.BaseController;
import cc.zy.base.common.entity.FebsConstant;
import cc.zy.base.common.entity.FebsResponse;
import cc.zy.base.common.entity.QueryRequest;
import cc.zy.base.common.exception.FebsException;
import cc.zy.base.common.utils.FebsUtil;
import com.wuwenze.poi.ExcelKit;
import lombok.extern.slf4j.Slf4j;
import lombok.RequiredArgsConstructor;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/**
 * 现场确认地点 Controller
 *
 * @author Jiangjinlin
 * @date 2021-01-25 18:18:46
 */
@Slf4j
@Validated
@Controller
@RequiredArgsConstructor
@RequestMapping("confirmAddress")
public class ConfirmAddressController extends BaseController {

    @Resource
    private final IConfirmAddressService confirmAddressService;

    private final IProvinceService provinceService;
    private final ICityService cityService;
    private final IAreaService areaService;
    private final IDicService dicService;


    @GetMapping("list")
    @ResponseBody
//    @RequiresPermissions("confirmAddress:view")
    public FebsResponse collegeList(QueryRequest request,
                                    ConfirmAddress confirmAddress,
                                    String province,
                                    String city) {

        if (province!=null && !province.equals("") ){
            List<Province> provinces = provinceService.findProvinces(Integer.parseInt(province));
            province = provinces.get(0).getProvinceName();
            System.out.println(province);
            confirmAddress.setProvince(province);
        }
        if (city!=null && !city.equals("")){
            City cityById = cityService.findCityById(Integer.parseInt(city));
            city = cityById.getCityName();
            confirmAddress.setCity(city);
        }
        System.out.println(confirmAddress);

        Map<String, Object> dataTable = getDataTable(this.confirmAddressService.findConfirmAddresss(request, confirmAddress));
        return new FebsResponse().success().data(dataTable);
    }

    @PostMapping("add")
    @ResponseBody
    @ControllerEndpoint(operation = "新增确认地点", exceptionMessage = "新增确认地失败")
    public FebsResponse addCollege(@Valid ConfirmAddress confirmAddress,
                                   String province,
                                   String city,
                                   String area,
                                   String status) {

        //获取省市区的名字
        List<Province> provinces = provinceService.findProvinces(Integer.parseInt(province));
        province = provinces.get(0).getProvinceName();
        City cityById = cityService.findCityById(Integer.parseInt(city));
        city = cityById.getCityName();
        confirmAddress.setCity(city);
        confirmAddress.setProvince(province);

        Area areaById = areaService.findAreaById(Integer.parseInt(area));
        area = areaById.getAreaName();
        confirmAddress.setArea(area);

        Dic dicBid = dicService.findDicBid(Integer.parseInt(status));
        status = dicBid.getDetail();
        confirmAddress.setStatus(status);

        this.confirmAddressService.createConfirmAddress(confirmAddress);
        return new FebsResponse().success();
    }

    @PostMapping("update")
    @ControllerEndpoint(operation = "修改确认地", exceptionMessage = "修改确认点失败")
    @ResponseBody
    public FebsResponse updateAddress(@Valid ConfirmAddress confirmAddress) {
        System.out.println(confirmAddress);
        log.info(confirmAddress+"!!!!!!!!!!!");
        if (confirmAddress.getId() == null) {
            throw new FebsException("确认地ID为空");
        }
        this.confirmAddressService.updateConfirmAddress(confirmAddress);
        return new FebsResponse().success();
    }

    @PostMapping("del")
    @ControllerEndpoint(operation = "修改确认地", exceptionMessage = "修改确认点失败")
    @ResponseBody
    public FebsResponse delAddress(@Valid ConfirmAddress confirmAddress) {

        if (confirmAddress.getId() == null) {
            throw new FebsException("确认地ID为空");
        }
        this.confirmAddressService.deleteConfirmAddress(confirmAddress);
        return new FebsResponse().success();
    }

//-------------------------------------------------------------------------------------------刘润雨
	@GetMapping(FebsConstant.VIEW_PREFIX + "confirmAddress")
    public String confirmAddressIndex(){
        return FebsUtil.view("confirmAddress/confirmAddress");
    }

    @GetMapping("confirmAddress")
    @ResponseBody
    public FebsResponse getAllConfirmAddresss(ConfirmAddress confirmAddress) {
        return new FebsResponse().success().data(confirmAddressService.findConfirmAddresss(confirmAddress));
    }

    @GetMapping("confirmAddress/list")
    @ResponseBody
    public FebsResponse confirmAddressList() {
        List<ConfirmAddress> confirmAddresss = confirmAddressService.findConfirmAddresss(null);
        System.out.println(confirmAddresss);
        return new FebsResponse().success().data(confirmAddresss);
    }



}
