package cc.zy.base.businesses.controller;


import cc.zy.base.businesses.entity.EntranceScore;
import cc.zy.base.businesses.entity.Level;
import cc.zy.base.businesses.entity.SubjectCategory;
import cc.zy.base.businesses.entity.TestSubject;
import cc.zy.base.businesses.mapper.TestSubjectMapper;
import cc.zy.base.businesses.service.IEntranceScoreService;
import cc.zy.base.businesses.service.ILevelService;
import cc.zy.base.businesses.service.ISubjectCategoryService;
import cc.zy.base.businesses.utils.ExcelUtil;
import cc.zy.base.common.annotation.ControllerEndpoint;
import cc.zy.base.common.controller.BaseController;
import cc.zy.base.common.entity.FebsResponse;
import cc.zy.base.common.entity.QueryRequest;
import cc.zy.base.system.entity.User;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wuwenze.poi.ExcelKit;
import com.wuwenze.poi.util.DateUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.lang.reflect.Field;
import java.net.URLEncoder;
import java.util.*;

@Slf4j
@Validated
@Controller
@RequiredArgsConstructor
@RequestMapping("entranceScore")
public class EntranceScoreController extends BaseController {
    @Resource
    private IEntranceScoreService iEntranceScoreService;
    @Resource
    private TestSubjectMapper testSubjectMapper;
    @Resource
    private ISubjectCategoryService iSubjectCategoryService;
    @Resource
    private ILevelService iLevelService;

    @GetMapping("list")
    @ResponseBody
    public FebsResponse getAll(QueryRequest request, EntranceScore entranceScore) {
        System.out.println(entranceScore.getLevelId());
        System.out.println(entranceScore.getSubtypeId());
        System.out.println("collegeId"+entranceScore.getCollegeId());
        //判断总数据是否空数据
        Boolean flag = true;
        IPage<EntranceScore> list = this.iEntranceScoreService.findList(entranceScore, request);

        List<EntranceScore> records = list.getRecords();
        System.out.println(records);
        //合格的学生集合
        List<EntranceScore> passStu = new ArrayList<>();
        //不合格的学生集合
        List<EntranceScore> noPassStu = new ArrayList<>();
        if (records!=null){
            for (EntranceScore score : records) {
                if (score.getStuName() == null || score.getStuName().equals("")) {
                    flag = false;
                    break;
                }
            }
        }else {
            flag=false;
        }

        if (flag) {
            Integer grade = 0;
            //先获取当前批次>当前层次>当前科目类型的分数线
            /*获取分数线的方法*/
            Integer scoreLine = 130;
            //获取当前查询的所有科目名
            for (EntranceScore score : records) {
                //获取学生总成绩
                Integer stuTotalScore = iEntranceScoreService.getStuTotalScore(score);
                System.out.println(stuTotalScore);
                //判断成绩是否合格
                if (stuTotalScore<scoreLine){
                    //先设置合格状态到entranceScore对象中
                    score.setIsPass("通过");
                    //如果合格就把对象装进合格的集合中
                    passStu.add(score);
                }else {
                    score.setIsPass("未过");
                    //如果不合格就把对象装进不合格的集合中
                    noPassStu.add(score);
                }
            }
            System.out.println("是否通过：" + entranceScore.getIsPass());
            //判断需要查合格还是不合格的学生
            if (entranceScore.getIsPass().equals("1")) {
                //合格学生
                IPage<EntranceScore> list1 = new Page<>();
                list1.setRecords(passStu);
                list1.setTotal(list1.getSize());
                list1.setCurrent(request.getPageNum());
                list1.setSize(request.getPageSize());
                Map<String, Object> dataTable = getDataTable(list1);
                System.out.println(dataTable);
                return new FebsResponse().success().data(dataTable);
            } else if (entranceScore.getIsPass().equals("0")) {
                //不合格
                IPage<EntranceScore> list2 = new Page<>();
                list2.setRecords(noPassStu);
                list2.setTotal(list2.getSize());
                list2.setCurrent(request.getPageNum());
                list2.setSize(request.getPageSize());
                Map<String, Object> dataTable = getDataTable(list2);
                System.out.println(dataTable);
                return new FebsResponse().success().data(dataTable);
            } else {
                //查所有
                Map<String, Object> dataTable = getDataTable(list);
                System.out.println(dataTable);
                return new FebsResponse().success().data(dataTable);
            }
        } else {
            return new FebsResponse().success().data("");
        }
    }


    @GetMapping("subjects")
    @ResponseBody
    public FebsResponse getSubjects(Integer levelId, Integer subtypeId) {
        System.out.println("subtypeId" + subtypeId);
        System.out.println("levelId" + levelId);
        List<TestSubject> subjests = testSubjectMapper.getSubjestsById(levelId, subtypeId);
        System.out.println(subjests);
        return new FebsResponse().success().data(subjests);
    }

    @GetMapping("levelList")
    @ResponseBody
    public FebsResponse getlevelList() {
        List<Level> levels = iLevelService.findLevels();
        System.out.println(levels);
        return new FebsResponse().success().data(levels);
    }

    /**
     * 获取所有层次
     * @param levelId
     * @return
     */
    @GetMapping("subTypeList")
    @ResponseBody
    public FebsResponse getSubTypeList(Integer levelId) {
//        System.out.println(levelId);
        List<SubjectCategory> subjectCategorys = iSubjectCategoryService.getSubjectCategorys(levelId);

        return new FebsResponse().success().data(subjectCategorys);
    }
    /**
     * 获取所有批次
     * @return
     */
    @GetMapping("batchs")
    @ResponseBody
    public FebsResponse getBatchs() {
        List<EntranceScore> batchs = iEntranceScoreService.getBatchs();
        return new FebsResponse().success().data(batchs);
    }

    /**
     * 通过批次Id获取院校名称
     *
     * @return
     */
    @GetMapping("colleges")
    @ResponseBody
    public FebsResponse getCollegeByBatchId(Integer batchId) {
        System.out.println(batchId);
        List<EntranceScore> colleges = iEntranceScoreService.getCollegesByBatchId(batchId);
        return new FebsResponse().success().data(colleges);
    }

    /**
     * 导出信息
     *
     * @return
     */
    @GetMapping("excel")
    @ControllerEndpoint(exceptionMessage = "导出Excel失败")
    public void export(QueryRequest request, HttpServletResponse response, EntranceScore entranceScore) {
        EntranceScore score1 = new EntranceScore();
        IPage<EntranceScore> list = this.iEntranceScoreService.findAllList(score1, request);
        List<EntranceScore> records = list.getRecords();
        //合格的学生集合
        List<EntranceScore> passStu = new ArrayList<>();
        //不合格的学生集合
        List<EntranceScore> noPassStu = new ArrayList<>();
        Integer grade = 0;
        //先获取当前批次>当前层次>当前科目类型的分数线
        /*获取分数线的方法*/
        Integer scoreLine = 130;
        //获取当前查询的所有科目名
        for (EntranceScore score : records) {
            //获取学生总成绩
            Integer stuTotalScore = iEntranceScoreService.getStuTotalScore(score);
            System.out.println(stuTotalScore);
            //判断成绩是否合格
            if (stuTotalScore>=scoreLine){
                //先设置合格状态到entranceScore对象中
                score.setIsPass("通过");
                //如果合格就把对象装进合格的集合中
                passStu.add(score);
            }else {
                score.setIsPass("未过");
                //如果不合格就把对象装进不合格的集合中
                noPassStu.add(score);
            }
        }
        //判断需要查合格还是不合格的学生
        if (entranceScore.getIsPass().equals("通过")) {
            //合格学生
            ExcelKit.$Export(EntranceScore.class, response).downXlsx(passStu, false);
        } else if (entranceScore.getIsPass().equals("未过")) {
            //不合格
            ExcelKit.$Export(EntranceScore.class, response).downXlsx(noPassStu, false);
        } else {
            //查所有
            ExcelKit.$Export(EntranceScore.class, response).downXlsx(records, false);
        }
    }

    /**
     * 导入信息
     *
     * @return
     */
    @GetMapping("importExcel")
    public void importExcel(String url) {
        System.out.println(url);
    }

    @GetMapping("readFile")
    public FebsResponse uploadPicture(String url) {
        ArrayList<EntranceScore> list = new ArrayList<>();
        try {
            //1、获取文件输入流
             InputStream inputStream = new FileInputStream("D:/入学成绩表.xlsx");
            //2、获取Excel工作簿对象
            HSSFWorkbook workbook = new HSSFWorkbook(inputStream);
            //3、得到Excel工作表对象
            HSSFSheet sheetAt = workbook.getSheetAt(0);
            //4、循环读取表格数据
           for (Row row : sheetAt) {
               //首行（即表头）不读取
                if (row.getRowNum() == 0) {
                    continue;
                }
                //读取当前行中单元格数据，索引从0开始
                String areaNum = row.getCell(0).getStringCellValue();
                String province = row.getCell(1).getStringCellValue();
                String city = row.getCell(2).getStringCellValue();
                String district = row.getCell(3).getStringCellValue();
                String postcode = row.getCell(4).getStringCellValue();

                EntranceScore score = new EntranceScore();
//                score.setCity(city);
//                score.setDistrict(district);
//                score.setProvince(province);
//                 score.setPostCode(postcode);
                list.add(score);
            }
            //5、关闭流
            workbook.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
       return new FebsResponse().success().data(list);
    }


}
