package cc.zy.base.businesses.controller;


import cc.zy.base.businesses.entity.ResolveException;
import cc.zy.base.businesses.service.IResolveExceptionService;
import cc.zy.base.common.annotation.ControllerEndpoint;
import cc.zy.base.common.controller.BaseController;
import cc.zy.base.common.entity.FebsConstant;
import cc.zy.base.common.entity.FebsResponse;
import cc.zy.base.common.entity.QueryRequest;
import cc.zy.base.common.utils.FebsUtil;
import com.wuwenze.poi.ExcelKit;
import lombok.extern.slf4j.Slf4j;
import lombok.RequiredArgsConstructor;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *  Controller
 *
 * @author Jiangjinlin
 * @date 2021-01-25 16:03:48
 */
@Slf4j
@Validated
@Controller
@RequiredArgsConstructor
public class ResolveExceptionController extends BaseController {

    private final IResolveExceptionService resolveExceptionService;

    @GetMapping(FebsConstant.VIEW_PREFIX + "resolveException")
    public String resolveExceptionIndex(){
        return FebsUtil.view("resolveException/resolveException");
    }

    @GetMapping("resolveException")
    @ResponseBody
    @RequiresPermissions("resolveException:list")
    public FebsResponse getAllResolveExceptions(ResolveException resolveException) {
        return new FebsResponse().success().data(resolveExceptionService.findResolveExceptions(resolveException));
    }

    @GetMapping("resolveException/list")
    @ResponseBody
//    @RequiresPermissions("resolveException:list")
    public FebsResponse resolveExceptionList(QueryRequest request, String startDate, String  endDate, Integer triggerType, Integer reqUserId, ResolveException resolveException) {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Timestamp dtpStartDate = null;
        Timestamp dtpEndDate =null;
        try {
            if (startDate != null && !startDate.equals("") ) {
               Date startDate0 = simpleDateFormat.parse(startDate);
               dtpStartDate = new Timestamp(startDate0.getTime());
            }
            if (endDate != null && !endDate.equals("") ) {
                Date  endDate0 = simpleDateFormat.parse(endDate);
                dtpEndDate = new Timestamp(endDate0.getTime());
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
//        System.out.println("start"+dtpStartDate+"end"+ dtpEndDate+"类型"+ triggerType+"使用者ID"+ reqUserId);
        Map<String, Object> dataTable = getDataTable(this.resolveExceptionService.findByResolveException(request, dtpStartDate, dtpEndDate, triggerType, reqUserId));
        return new FebsResponse().success().data(dataTable);
    }

    @ControllerEndpoint(operation = "新增ResolveException", exceptionMessage = "新增ResolveException失败")
    @PostMapping("resolveException")
    @ResponseBody
    @RequiresPermissions("resolveException:add")
    public FebsResponse addResolveException(@Valid ResolveException resolveException) {
        this.resolveExceptionService.createResolveException(resolveException);
        return new FebsResponse().success();
    }

    @ControllerEndpoint(operation = "删除ResolveException", exceptionMessage = "删除ResolveException失败")
    @GetMapping("resolveException/delete")
    @ResponseBody
    @RequiresPermissions("resolveException:delete")
    public FebsResponse deleteResolveException(ResolveException resolveException) {
        this.resolveExceptionService.deleteResolveException(resolveException);
        return new FebsResponse().success();
    }

    @ControllerEndpoint(operation = "修改ResolveException", exceptionMessage = "修改ResolveException失败")
    @PostMapping("resolveException/update")
    @ResponseBody
    @RequiresPermissions("resolveException:update")
    public FebsResponse updateResolveException(ResolveException resolveException) {
        this.resolveExceptionService.updateResolveException(resolveException);
        return new FebsResponse().success();
    }

    @ControllerEndpoint(operation = "修改ResolveException", exceptionMessage = "导出Excel失败")
    @PostMapping("resolveException/excel")
    @ResponseBody
    @RequiresPermissions("resolveException:export")
    public void export(QueryRequest queryRequest, ResolveException resolveException, HttpServletResponse response) {
        List<ResolveException> resolveExceptions = this.resolveExceptionService.findResolveExceptions(queryRequest, resolveException).getRecords();
        ExcelKit.$Export(ResolveException.class, response).downXlsx(resolveExceptions, false);
    }
}
