package cc.zy.base.businesses.controller;

import cc.zy.base.common.annotation.ControllerEndpoint;
import cc.zy.base.businesses.entity.ReqResult;
import cc.zy.base.businesses.service.IReqResultService;
import cc.zy.base.common.controller.BaseController;
import cc.zy.base.common.entity.FebsConstant;
import cc.zy.base.common.entity.FebsResponse;
import cc.zy.base.common.entity.QueryRequest;
import cc.zy.base.common.utils.FebsUtil;
import com.wuwenze.poi.ExcelKit;
import lombok.extern.slf4j.Slf4j;
import lombok.RequiredArgsConstructor;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *  Controller
 *
 * @author Jiangjinlin
 * @date 2021-01-25 10:16:25
 */
@Slf4j
@Validated
@Controller
@RequiredArgsConstructor
//@RequestMapping("reqResult")
public class ReqResultController extends BaseController {

    private final IReqResultService reqResultService;

    @GetMapping(FebsConstant.VIEW_PREFIX + "reqResult")
    public String reqResultIndex(){
        return FebsUtil.view("reqResult/reqResult");
    }

    @GetMapping("reqResult")
    @ResponseBody
    @RequiresPermissions("reqResult:list")
    public FebsResponse getAllReqResults(ReqResult reqResult) {
        return new FebsResponse().success().data(reqResultService.findReqResults(reqResult));
    }

    @GetMapping("reqResult/list")
    @ResponseBody
    @RequiresPermissions("reqResult:list")
    public FebsResponse reqResultList(QueryRequest request, ReqResult reqResult) {
        Map<String, Object> dataTable = getDataTable(this.reqResultService.findReqResults(request, reqResult));
        return new FebsResponse().success().data(dataTable);
    }

    @ControllerEndpoint(operation = "新增ReqResult", exceptionMessage = "新增ReqResult失败")
    @PostMapping("reqResult")
    @ResponseBody
    @RequiresPermissions("reqResult:add")
    public FebsResponse addReqResult(@Valid ReqResult reqResult) {
        this.reqResultService.createReqResult(reqResult);
        return new FebsResponse().success();
    }

    @ControllerEndpoint(operation = "删除ReqResult", exceptionMessage = "删除ReqResult失败")
    @GetMapping("reqResult/delete")
    @ResponseBody
    @RequiresPermissions("reqResult:delete")
    public FebsResponse deleteReqResult(ReqResult reqResult) {
        this.reqResultService.deleteReqResult(reqResult);
        return new FebsResponse().success();
    }

    @ControllerEndpoint(operation = "修改ReqResult", exceptionMessage = "修改ReqResult失败")
    @PostMapping("reqResult/update")
    @ResponseBody
    @RequiresPermissions("reqResult:update")
    public FebsResponse updateReqResult(ReqResult reqResult) {
        this.reqResultService.updateReqResult(reqResult);
        return new FebsResponse().success();
    }

    @ControllerEndpoint(operation = "修改ReqResult", exceptionMessage = "导出Excel失败")
    @PostMapping("reqResult/excel")
    @ResponseBody
    @RequiresPermissions("reqResult:export")
    public void export(QueryRequest queryRequest, ReqResult reqResult, HttpServletResponse response) {
        List<ReqResult> reqResults = this.reqResultService.findReqResults(queryRequest, reqResult).getRecords();
        ExcelKit.$Export(ReqResult.class, response).downXlsx(reqResults, false);
    }

    @ControllerEndpoint(operation = "新增ReqResultList", exceptionMessage = "新增ReqResultList失败")
    @PostMapping("reqResult/list")
    @ResponseBody
    @RequiresPermissions("reqResult:addList")
    public FebsResponse addReqResultLiat() {
        List<ReqResult> list = new ArrayList<>();
        ReqResult reqResult = new ReqResult();
        ReqResult reqResult1 = new ReqResult();
        reqResult.setId(4);
        reqResult.setReqInfoId(4);
        reqResult.setFollowUserId(4);
        reqResult.setGroupId(4);
        reqResult1.setId(5);
        reqResult.setReqInfoId(5);
        reqResult.setFollowUserId(5);
        reqResult.setGroupId(5);
        list.add(reqResult);
        list.add(reqResult1);
        this.reqResultService.addReqResultList(list);
        return new FebsResponse().success();
    }
}
