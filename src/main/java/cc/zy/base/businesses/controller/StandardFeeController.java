package cc.zy.base.businesses.controller;


import cc.zy.base.businesses.entity.StandardFee;
import cc.zy.base.businesses.service.IStandardFeeService;
import cc.zy.base.common.annotation.ControllerEndpoint;
import cc.zy.base.common.controller.BaseController;
import cc.zy.base.common.entity.FebsConstant;
import cc.zy.base.common.entity.FebsResponse;
import cc.zy.base.common.entity.QueryRequest;
import cc.zy.base.common.exception.FebsException;
import cc.zy.base.common.utils.FebsUtil;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.Map;

/**
 *  Controller
 *
 * @author Jiangjinlin
 * @date 2021-01-26 09:54:23
 */
@Slf4j
@Validated
@Controller
@RequiredArgsConstructor
@RequestMapping("standardFee")
public class StandardFeeController extends BaseController {

     @Resource
    private final IStandardFeeService standardFeeService;

    @GetMapping(FebsConstant.VIEW_BUS_PREFIX + "stanardFee")
    public String stanardFeeIndex(){
        return FebsUtil.BusinessView("stanardFee/stanardFee");
    }

    @GetMapping("list")
    @ResponseBody
    @RequiresPermissions("standardFee:view")
    public FebsResponse standardFeeList(QueryRequest request, StandardFee standardFee) {
        System.out.println("================stanardFee=============");
        Map<String, Object> dataTable = getDataTable(this.standardFeeService.findStandardFees(request, standardFee));
        return new FebsResponse().success().data(dataTable);
    }

    @PostMapping("add")
    @ResponseBody
    @ControllerEndpoint(operation = "新增套内缴费", exceptionMessage = "新增套内缴费失败")
    public FebsResponse addStandardFee(@Valid StandardFee standardFee) {
        System.out.println("====================进入新增套内缴费==================="
                 +"  "+standardFee.getBatchId()
                 +"  "+standardFee.getCultivate()
                 +"  "+standardFee.getCollegeId()
                 +"  "+standardFee.getMajorId()
                 +"  "+standardFee.getSubjectCategoryId()
                 +"  "+standardFee.getStudyLevelId()
                 +"  "+standardFee.getPaymenExplain()
                +"  "+standardFee.getOriginalPrice()
                +"报名费"+standardFee.getEntryFee()


        );


        StandardFee s1=new StandardFee();
        s1.setPaymenExplain("报名费");
        s1.setBatchId(standardFee.getBatchId());
        s1.setOriginalPrice(standardFee.getEntryFee());
        s1.setCollegeId(standardFee.getCollegeId());
        s1.setCultivate(1);
        s1.setStudyLevelId(standardFee.getStudyLevelId());
        s1.setMajorId(standardFee.getMajorId());
        s1.setSubjectCategoryId(standardFee.getSubjectCategoryId());

        StandardFee s2=new StandardFee();
        s2.setPaymenExplain("第一学年");
        s2.setBatchId(standardFee.getBatchId());
        s2.setOriginalPrice(standardFee.getFirstFee());
        s2.setCollegeId(standardFee.getCollegeId());
        s2.setCultivate(1);
        s2.setStudyLevelId(standardFee.getStudyLevelId());
        s2.setMajorId(standardFee.getMajorId());
        s2.setSubjectCategoryId(standardFee.getSubjectCategoryId());

        StandardFee s3=new StandardFee();
        s3.setPaymenExplain("第二学年");
        s3.setBatchId(standardFee.getBatchId());
        s3.setOriginalPrice(standardFee.getSecondFee());
        s3.setCollegeId(standardFee.getCollegeId());
        s3.setCultivate(1);
        s3.setStudyLevelId(standardFee.getStudyLevelId());
        s3.setMajorId(standardFee.getMajorId());
        s3.setSubjectCategoryId(standardFee.getSubjectCategoryId());

        StandardFee s4=new StandardFee();
        s4.setPaymenExplain("第三学年");
        s4.setBatchId(standardFee.getBatchId());
        s4.setOriginalPrice(standardFee.getThirdlyFee());
        s4.setCollegeId(standardFee.getCollegeId());
        s4.setCultivate(1);
        s4.setStudyLevelId(standardFee.getStudyLevelId());
        s4.setMajorId(standardFee.getMajorId());
        s4.setSubjectCategoryId(standardFee.getSubjectCategoryId());

        StandardFee s5=new StandardFee();
        s5.setPaymenExplain("第四学年");
        s5.setBatchId(standardFee.getBatchId());
        s5.setOriginalPrice(standardFee.getFourthlyFee());
        s5.setCollegeId(standardFee.getCollegeId());
        s5.setCultivate(1);
        s5.setStudyLevelId(standardFee.getStudyLevelId());
        s5.setMajorId(standardFee.getMajorId());
        s5.setSubjectCategoryId(standardFee.getSubjectCategoryId());

          StandardFee s6=new StandardFee();
        s6.setPaymenExplain("第五学年");
        s6.setBatchId(standardFee.getBatchId());
        s6.setOriginalPrice(standardFee.getFifthFee());
        s6.setCollegeId(standardFee.getCollegeId());
        s6.setCultivate(1);
        s6.setStudyLevelId(standardFee.getStudyLevelId());
        s6.setMajorId(standardFee.getMajorId());
        s6.setSubjectCategoryId(standardFee.getSubjectCategoryId());


        int count=0;


            count += this.standardFeeService.addStandardFee(s1);
            count += this.standardFeeService.addStandardFee(s2);
            count += this.standardFeeService.addStandardFee(s3);
            count += this.standardFeeService.addStandardFee(s4);
            count += this.standardFeeService.addStandardFee(s5);
            count += this.standardFeeService.addStandardFee(s6);




        if(count==6){
            return new FebsResponse().success();

        }else {

            return null;
        }






//        this.standardFeeService.createStandardFee(standardFee);
//        return new FebsResponse().success();

    }

    @GetMapping("delete/{standardFeeIds}")
    @ControllerEndpoint(operation = "删除院校", exceptionMessage = "删除院校失败")
    @ResponseBody
    public FebsResponse deleteColleges(@NotBlank(message = "{required}") @PathVariable String standardFeeIds) {
        String[] ids = standardFeeIds.split(StringPool.COMMA);
        this.standardFeeService.deleteStandardFees(ids);
        return new FebsResponse().success();
    }
//
    @PostMapping("update")
    @ControllerEndpoint(operation = "修改套内缴费信息", exceptionMessage = "修改记录失败")
    @ResponseBody
    public FebsResponse updateStandardFee(@Valid StandardFee standardFee) {
        System.out.println("===========修改套内缴费==========="+standardFee.getId());
        if (standardFee.getId() == null) {
            throw new FebsException("院校ID为空");
        }
        this.standardFeeService.updateStandardFee(standardFee);
        return new FebsResponse().success();
    }
//
//    @ControllerEndpoint(operation = "修改College", exceptionMessage = "导出Excel失败")
//    @PostMapping("college/excel")
//    @ResponseBody
//    @RequiresPermissions("college:export")
//    public void export(QueryRequest queryRequest, College college, HttpServletResponse response) {
//        List<College> colleges = this.collegeService.findColleges(queryRequest, college).getRecords();
//        ExcelKit.$Export(College.class, response).downXlsx(colleges, false);
//    }

/*************************下拉列表******************************/
     @GetMapping("batchlist")
    @ResponseBody
    @RequiresPermissions("standardFee:view")
    public FebsResponse batchList(Integer id) {
        System.out.println("================batchAll=============");
         List<Map<String, String>> maps = standardFeeService.batchAll(id);

        return new FebsResponse().success().data(maps);

    }
     @GetMapping("collegelist")
    @ResponseBody
    @RequiresPermissions("standardFee:view")
    public FebsResponse collegeList(Integer id) {
        System.out.println("================collegeAll=============id:"+id);
         List<Map<String, String>> maps = standardFeeService.collegeAll(id);

        return new FebsResponse().success().data(maps);

    }

    @GetMapping("levallist")
    @ResponseBody
    @RequiresPermissions("standardFee:view")
    public FebsResponse levelList(Integer id) {
        System.out.println("================levalAll=============id:"+id);
         List<Map<String, String>> maps = standardFeeService.levelAll(id);

        return new FebsResponse().success().data(maps);

    }

    @GetMapping("subjectcategorylist")
    @ResponseBody
    @RequiresPermissions("standardFee:view")
    public FebsResponse subjectcategoryList(Integer id) {
        System.out.println("================subjectcategoryAll=============");
         List<Map<String, String>> maps = standardFeeService.subjectCategoryAll(id);

        return new FebsResponse().success().data(maps);

    }
     @GetMapping("majorlist")
    @ResponseBody
    @RequiresPermissions("standardFee:view")
    public FebsResponse majorList(Integer id) {
        System.out.println("================majorAll=============");
         List<Map<String, String>> maps = standardFeeService.majorAll(id);

        return new FebsResponse().success().data(maps);

    }
}
