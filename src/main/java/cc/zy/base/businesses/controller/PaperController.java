package cc.zy.base.businesses.controller;

import cc.zy.base.businesses.entity.Papers;
import cc.zy.base.businesses.entity.TeachProgram;
import cc.zy.base.businesses.service.IBatchService;
import cc.zy.base.businesses.service.IPapersService;
import cc.zy.base.businesses.service.ITeachProgramService;
import cc.zy.base.common.annotation.ControllerEndpoint;
import cc.zy.base.common.controller.BaseController;
import cc.zy.base.common.entity.FebsResponse;
import cc.zy.base.common.entity.QueryRequest;
import cc.zy.base.system.entity.User;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.wuwenze.poi.ExcelKit;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

@Slf4j
@Validated
@Controller
@RequiredArgsConstructor
@RequestMapping("papers")
public class PaperController extends BaseController {
    @Resource
    private IPapersService iPapersService;
    @Resource
    private IBatchService iBatchService;
    @Resource
    private ITeachProgramService iTeachProgramService;

    @GetMapping("list")
    @ResponseBody
    @RequiresPermissions("papers:view")
    public FebsResponse papersList(QueryRequest request, Papers papers) {
        IPage<Papers> papersPage = iPapersService.findPapersPage(request, papers);
        Map<String, Object> dataTable = getDataTable(papersPage);
        return new FebsResponse().success().data(dataTable);
    }

    /**
     * 做联合查询
     * 通过院校id查询到对应的层次名称
     */
    @GetMapping("levelName")
    @ResponseBody
    @RequiresPermissions("papers:view")
    public FebsResponse levelName(Integer collegeId) {
        List<TeachProgram> levelNames = iTeachProgramService.findLevelNameByCollegeId(collegeId);
        return new FebsResponse().success().data(levelNames);
    }

    /**
     * 做联合查询
     * 通过层次id查询到对应的专业名称
     */
    @GetMapping("majorName")
    @ResponseBody
    @RequiresPermissions("papers:view")
    public FebsResponse majorName(Integer levelId) {
        List<TeachProgram> majorNames = iTeachProgramService.findMajoNameByLevelId(levelId);
        return new FebsResponse().success().data(majorNames);
    }

    /**
     * 导出论文信息表
     * @param queryRequest
     * @param papers
     * @param response
     */
    @GetMapping("excel")
    @RequiresPermissions("papers:view")
    @ControllerEndpoint(exceptionMessage = "导出Excel失败")
    public void export(QueryRequest queryRequest, Papers papers, HttpServletResponse response) {
        List<Papers> users = this.iPapersService.findPapersPage(queryRequest, papers).getRecords();
        ExcelKit.$Export(Papers.class, response).downXlsx(users, false);
    }
}
