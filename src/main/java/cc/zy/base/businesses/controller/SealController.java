package cc.zy.base.businesses.controller;


import cc.zy.base.businesses.entity.College;
import cc.zy.base.businesses.entity.Seal;
import cc.zy.base.businesses.service.ISealService;
import cc.zy.base.common.annotation.ControllerEndpoint;
import cc.zy.base.common.controller.BaseController;
import cc.zy.base.common.entity.FebsConstant;
import cc.zy.base.common.entity.FebsResponse;
import cc.zy.base.common.entity.QueryRequest;
import cc.zy.base.common.utils.FebsUtil;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.wuwenze.poi.ExcelKit;
import lombok.extern.slf4j.Slf4j;
import lombok.RequiredArgsConstructor;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.Map;

/**
 *  Controller
 *
 * @author Jiangjinlin
 * @date 2021-01-27 09:56:39
 */
@Slf4j
@Validated
@Controller
@RequiredArgsConstructor
@RequestMapping("seal")
public class SealController extends BaseController {

    private final ISealService sealService;

    @GetMapping(FebsConstant.VIEW_PREFIX + "seal")
    public String sealIndex(){
        return FebsUtil.view("seal/seal");
    }

    @GetMapping("seal")
    @ResponseBody
    @RequiresPermissions("seal:list")
    public FebsResponse getAllSeals(Seal seal) {
        return new FebsResponse().success().data(sealService.findSeals(seal));
    }

    @GetMapping("list")
    @ResponseBody
    @RequiresPermissions("seal:view")
    public FebsResponse sealList(QueryRequest request, College college) {
        Map<String, Object> dataTable = getDataTable(this.sealService.findSeals(request, college));
        return new FebsResponse().success().data(dataTable);
    }

    @ControllerEndpoint(operation = "新增Seal", exceptionMessage = "新增Seal失败")
    @PostMapping("add")
    @ResponseBody
    @RequiresPermissions("seal:add")
    public FebsResponse addSeal(@Valid Seal seal) {
        this.sealService.createSeal(seal);
        return new FebsResponse().success();
    }


    @ControllerEndpoint(operation = "删除Seal", exceptionMessage = "删除Seal失败")
    @GetMapping("seal/delete")
    @ResponseBody
    @RequiresPermissions("seal:delete")
    public FebsResponse deleteSeal(Seal seal) {
        this.sealService.deleteSeal(seal);
        return new FebsResponse().success();
    }

    /**
     * 删除学校的印章
     * @param collegeId
     * @return
     */

    @GetMapping("delete/{collegeId}")
    @ControllerEndpoint(operation = "删除印章", exceptionMessage = "删除印章失败")
    @ResponseBody
    public FebsResponse deleteColleges(@NotBlank(message = "{required}") @PathVariable String collegeId) {



        this.sealService.deleteSeal(collegeId);
        return new FebsResponse().success();
    }

    @ControllerEndpoint(operation = "修改Seal", exceptionMessage = "修改Seal失败")
    @PostMapping("seal/update")
    @ResponseBody
    @RequiresPermissions("seal:update")
    public FebsResponse updateSeal(Seal seal) {
        this.sealService.updateSeal(seal);
        return new FebsResponse().success();
    }

    @ControllerEndpoint(operation = "修改Seal", exceptionMessage = "导出Excel失败")
    @PostMapping("seal/excel")
    @ResponseBody
    @RequiresPermissions("seal:export")
    public void export(QueryRequest queryRequest, College college, HttpServletResponse response) {
        List<College> seals = this.sealService.findSeals(queryRequest, college).getRecords();
        ExcelKit.$Export(Seal.class, response).downXlsx(seals, false);
    }
}
