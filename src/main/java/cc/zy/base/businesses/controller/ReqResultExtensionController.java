package cc.zy.base.businesses.controller;


import cc.zy.base.businesses.entity.ReqResultExtension;
import cc.zy.base.businesses.service.IReqResultExtensionService;
import cc.zy.base.common.annotation.ControllerEndpoint;
import cc.zy.base.common.controller.BaseController;
import cc.zy.base.common.entity.FebsConstant;
import cc.zy.base.common.entity.FebsResponse;
import cc.zy.base.common.entity.QueryRequest;
import cc.zy.base.common.utils.FebsUtil;
import com.wuwenze.poi.ExcelKit;
import lombok.extern.slf4j.Slf4j;
import lombok.RequiredArgsConstructor;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/**
 *  Controller
 *
 * @author Jiangjinlin
 * @date 2021-01-25 16:06:07
 */
@Slf4j
@Validated
@Controller
@RequiredArgsConstructor
public class ReqResultExtensionController extends BaseController {

    private final IReqResultExtensionService reqResultExtensionService;

    @GetMapping(FebsConstant.VIEW_PREFIX + "reqResultExtension")
    public String reqResultExtensionIndex(){
        return FebsUtil.view("reqResultExtension/reqResultExtension");
    }

    @GetMapping("reqResultExtension")
    @ResponseBody
    @RequiresPermissions("reqResultExtension:list")
    public FebsResponse getAllReqResultExtensions(ReqResultExtension reqResultExtension) {
        return new FebsResponse().success().data(reqResultExtensionService.findReqResultExtensions(reqResultExtension));
    }

    @GetMapping("reqResultExtension/list")
    @ResponseBody
    @RequiresPermissions("reqResultExtension:list")
    public FebsResponse reqResultExtensionList(QueryRequest request, ReqResultExtension reqResultExtension) {
        Map<String, Object> dataTable = getDataTable(this.reqResultExtensionService.findReqResultExtensions(request, reqResultExtension));
        return new FebsResponse().success().data(dataTable);
    }

    @ControllerEndpoint(operation = "新增ReqResultExtension", exceptionMessage = "新增ReqResultExtension失败")
    @PostMapping("reqResultExtension")
    @ResponseBody
    @RequiresPermissions("reqResultExtension:add")
    public FebsResponse addReqResultExtension(@Valid ReqResultExtension reqResultExtension) {
        this.reqResultExtensionService.createReqResultExtension(reqResultExtension);
        return new FebsResponse().success();
    }

    @ControllerEndpoint(operation = "删除ReqResultExtension", exceptionMessage = "删除ReqResultExtension失败")
    @GetMapping("reqResultExtension/delete")
    @ResponseBody
    @RequiresPermissions("reqResultExtension:delete")
    public FebsResponse deleteReqResultExtension(ReqResultExtension reqResultExtension) {
        this.reqResultExtensionService.deleteReqResultExtension(reqResultExtension);
        return new FebsResponse().success();
    }

    @ControllerEndpoint(operation = "修改ReqResultExtension", exceptionMessage = "修改ReqResultExtension失败")
    @PostMapping("reqResultExtension/update")
    @ResponseBody
    @RequiresPermissions("reqResultExtension:update")
    public FebsResponse updateReqResultExtension(ReqResultExtension reqResultExtension) {
        this.reqResultExtensionService.updateReqResultExtension(reqResultExtension);
        return new FebsResponse().success();
    }

    @ControllerEndpoint(operation = "修改ReqResultExtension", exceptionMessage = "导出Excel失败")
    @PostMapping("reqResultExtension/excel")
    @ResponseBody
    @RequiresPermissions("reqResultExtension:export")
    public void export(QueryRequest queryRequest, ReqResultExtension reqResultExtension, HttpServletResponse response) {
        List<ReqResultExtension> reqResultExtensions = this.reqResultExtensionService.findReqResultExtensions(queryRequest, reqResultExtension).getRecords();
        ExcelKit.$Export(ReqResultExtension.class, response).downXlsx(reqResultExtensions, false);
    }
}
