package cc.zy.base.businesses.controller;

import cc.zy.base.common.annotation.ControllerEndpoint;
import cc.zy.base.common.exception.FebsException;
import cc.zy.base.common.utils.FebsUtil;
import cc.zy.base.common.entity.FebsConstant;
import cc.zy.base.common.controller.BaseController;
import cc.zy.base.common.entity.FebsResponse;
import cc.zy.base.common.entity.QueryRequest;
import cc.zy.base.businesses.entity.College;
import cc.zy.base.businesses.service.ICollegeService;
import cc.zy.base.common.utils.HttpContextUtil;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.wuwenze.poi.ExcelKit;
import lombok.extern.slf4j.Slf4j;
import lombok.RequiredArgsConstructor;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *  Controller
 *
 * @author Jiangjinlin
 * @date 2021-01-18 10:51:13
 */
@Slf4j
@Validated
@Controller
@RequiredArgsConstructor
@RequestMapping("college")
public class CollegeController extends BaseController {
    @Resource
    private final ICollegeService collegeService;

    @GetMapping(FebsConstant.VIEW_BUS_PREFIX + "college")
    public String collegeIndex(){
        return FebsUtil.BusinessView("college/college");
    }

    @GetMapping("list")
    @ResponseBody
    @RequiresPermissions("college:view")
    public FebsResponse collegeList(QueryRequest request, College college) {
        Map<String, Object> dataTable = getDataTable(this.collegeService.findColleges(request, college));
        return new FebsResponse().success().data(dataTable);
    }

    @PostMapping("add")
    @ResponseBody
    @ControllerEndpoint(operation = "新增院校", exceptionMessage = "新增院校失败")
    public FebsResponse addCollege( @Valid College college) {
        college.setCreateuserid(FebsUtil.getCurrentUser().getUserId());
        this.collegeService.createCollege(college);
        return new FebsResponse().success();
    }

    @PostMapping("collegeImgUpload")
    @ResponseBody
    @ControllerEndpoint(operation = "上传院校签章", exceptionMessage = "上传院校签章失败")
    public FebsResponse collegeImgUpload(@RequestParam("file") MultipartFile multipartFile) {

        if (multipartFile != null && multipartFile.getSize()>0){
            String filename = multipartFile.getOriginalFilename();
            String suffix = filename.substring(filename.lastIndexOf(".") + 1);
            String realPath = "D:\\\\info\\\\collegeImg\\\\"+new Date().getTime()+"."+suffix;
            File newfile = new File(realPath);
            try {
                multipartFile.transferTo(newfile);
                return new FebsResponse().message(realPath);
            } catch (IOException e) {
                e.printStackTrace();
                return new FebsResponse().message("上传院校签章失败");
            }

        }else {
            return new FebsResponse().message("上传院校签章,文件为空");
        }
    }

    @GetMapping("delete/{collegeIds}")
    @ControllerEndpoint(operation = "删除院校", exceptionMessage = "删除院校失败")
    @ResponseBody
    public FebsResponse deleteColleges(@NotBlank(message = "{required}") @PathVariable String collegeIds) {
        String[] ids = collegeIds.split(StringPool.COMMA);
        this.collegeService.deleteColleges(ids);
        return new FebsResponse().success();
    }

    @PostMapping("update")
    @ControllerEndpoint(operation = "修改院校", exceptionMessage = "修改院校失败")
    @ResponseBody
    public FebsResponse updateCollege(@Valid College college) {
        if (college.getId() == null) {
            throw new FebsException("院校ID为空");
        }
        college.setUpdateuserid(FebsUtil.getCurrentUser().getUserId());
        this.collegeService.updateCollege(college);
        return new FebsResponse().success();
    }


    @GetMapping("excel")
    @ControllerEndpoint(exceptionMessage = "导出Excel失败")
    public void export(QueryRequest queryRequest, College college, HttpServletResponse response) {
        List<College> colleges = this.collegeService.findColleges(queryRequest, college).getRecords();
        ExcelKit.$Export(College.class, response).downXlsx(colleges, false);
    }
    @GetMapping("findCollageListNotPage")
    @ResponseBody
    public FebsResponse findCollageListNotPage(){
        List<College> collageListNotPage = collegeService.findCollageListNotPage();
        return new FebsResponse().success().data(collageListNotPage);
    }

	@GetMapping("findById/{collegeId}")
    @ResponseBody
    public FebsResponse findById(@PathVariable Integer collegeId) {
        College college = this.collegeService.findById(collegeId);
        return new FebsResponse().success().data(college);
    }

    @GetMapping("batchSelect")
    @ResponseBody
    public FebsResponse findBatchForSelect() {
        return new FebsResponse().success().data(this.collegeService.findBatchForSelect());
    }

    @GetMapping("collegeSelect")
    @ResponseBody
    public FebsResponse findCollegeForSelect() {
        return new FebsResponse().success().data(this.collegeService.findCollegeForSelect());
    }

    @GetMapping("levelSelect")
    @ResponseBody
    public FebsResponse findLevelForSelect(Integer collegeId) {
        log.info("院校id：" + collegeId);
        return new FebsResponse().success().data(this.collegeService.findLevelForSelect(collegeId));
    }

    @GetMapping("majorSelect")
    @ResponseBody
    public FebsResponse findMajorForSelect(Integer collegeId, Integer levelId) {
        log.info("院校id：" + collegeId + "*****层次id：" + levelId);
        return new FebsResponse().success().data(this.collegeService.findMajorForSelect(collegeId, levelId));
    }



}
