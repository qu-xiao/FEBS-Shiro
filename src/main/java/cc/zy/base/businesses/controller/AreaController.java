package cc.zy.base.businesses.controller;


import cc.zy.base.businesses.entity.Area;
import cc.zy.base.businesses.entity.City;
import cc.zy.base.businesses.service.IAreaService;
import cc.zy.base.businesses.service.ICityService;
import cc.zy.base.common.controller.BaseController;
import cc.zy.base.common.entity.FebsConstant;
import cc.zy.base.common.entity.FebsResponse;
import cc.zy.base.common.utils.FebsUtil;
import com.wuwenze.poi.ExcelKit;
import lombok.extern.slf4j.Slf4j;
import lombok.RequiredArgsConstructor;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/**
 *  Controller
 *
 * @author Jiangjinlin
 * @date 2021-01-26 16:29:51
 */
@Slf4j
@Validated
@Controller
@RequiredArgsConstructor
public class AreaController extends BaseController {

    private final IAreaService areaService;
    private final ICityService cityService;

    @GetMapping(FebsConstant.VIEW_PREFIX + "area")
    public String areaIndex(){
        return FebsUtil.view("area/area");
    }

    @GetMapping("area")
    @ResponseBody
   // @RequiresPermissions("area:list")
    public FebsResponse getAllAreas(Integer cid) {
        if(cid != null && cid != 0){
            City cityById = cityService.findCityById(cid);
            String cId = cityById.getCid();
            log.info("cid的值事"+cId);
            log.info("查出来的市信息为："+areaService.findAreas(cId));
            return new FebsResponse().success().data(areaService.findAreas(cId));
        }else{
            return null;
        }

    }

	  @GetMapping("allarea")
    @ResponseBody
    //@RequiresPermissions("province:list")
    public FebsResponse getAllArea() {
//        System.out.println("区域信息未"+areaService.findAllArea());
        return new FebsResponse().success().data(areaService.findAllArea());
    }

}
