package cc.zy.base.businesses.controller;

import cc.zy.base.businesses.entity.Dic;
import cc.zy.base.businesses.entity.Student;
import cc.zy.base.businesses.entity.TransferApplication;
import cc.zy.base.businesses.service.IDicService;
import cc.zy.base.businesses.service.ICollegeService;
import cc.zy.base.businesses.service.IStudentService;
import cc.zy.base.businesses.service.TransferApplicationService;
import cc.zy.base.common.annotation.ControllerEndpoint;
import cc.zy.base.common.controller.BaseController;
import cc.zy.base.common.entity.FebsConstant;
import cc.zy.base.common.entity.FebsResponse;
import cc.zy.base.common.entity.QueryRequest;
import cc.zy.base.common.exception.FebsException;
import cc.zy.base.common.utils.FebsUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/**
 * Controller
 *
 * @author Liuheng
 * @date 2021-01-18 10:51:13
 */
@Slf4j
@Validated
@Controller
@RequiredArgsConstructor
@RequestMapping("transferApplication")
public class TransferApplicationController extends BaseController {
    private final TransferApplicationService transferApplicationService;
    private final ICollegeService collegeService;
    private final IDicService dicService;
    private final IStudentService studentService;


    @GetMapping(FebsConstant.VIEW_BUS_PREFIX + "transferApplication")
    public String collegeIndex() {
        return FebsUtil.BusinessView("transferApplication/transferApplication");
    }

    @GetMapping("list")
    @ResponseBody
    @RequiresPermissions("transferApplication:view")
    public FebsResponse transferApplicationList(QueryRequest request, TransferApplication transferApplication, Student student) {
        log.info(transferApplication + "" + student);
        transferApplication.setStudent(student);
        Map<String, Object> dataTable = getDataTable(this.transferApplicationService.findTransferApplications(request, transferApplication));
        return new FebsResponse().success().data(dataTable);
    }

    @PostMapping("add")
    @ResponseBody
    @ControllerEndpoint(operation = "新增异动申请", exceptionMessage = "新增异动申请失败")
    public FebsResponse addCollege(@Valid TransferApplication transferApplication) {
        this.transferApplicationService.createTransferApplication(transferApplication);
        return new FebsResponse().success();
    }

    @GetMapping("transferTypeList")
    @ResponseBody
    public FebsResponse transferTypeList() {
        List<Dic> transferTypeList = this.dicService.findAllTransferType();
        return new FebsResponse().success().data(transferTypeList);
    }

    @PostMapping("update")
    @ControllerEndpoint(operation = "修改异动申请", exceptionMessage = "修改异动申请失败")
    @ResponseBody
    public FebsResponse updateTransferApplication(@Valid TransferApplication transferApplication) {
        log.info(transferApplication + "");
        if (transferApplication.getId() == null) {
            throw new FebsException("异动申请ID为空");
        }
        this.transferApplicationService.updateTransferApplication(transferApplication);
        return new FebsResponse().success();
    }
}
