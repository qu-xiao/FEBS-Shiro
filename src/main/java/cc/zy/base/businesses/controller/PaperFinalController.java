package cc.zy.base.businesses.controller;

import cc.zy.base.businesses.entity.PaperFinal;
import cc.zy.base.businesses.entity.Papers;
import cc.zy.base.businesses.service.IPaperFinalService;
import cc.zy.base.common.annotation.ControllerEndpoint;
import cc.zy.base.common.controller.BaseController;
import cc.zy.base.common.entity.FebsResponse;
import cc.zy.base.common.entity.QueryRequest;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.wuwenze.poi.ExcelKit;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

@Slf4j
@Validated
@Controller
@RequiredArgsConstructor
@RequestMapping("paperFinal")
public class PaperFinalController extends BaseController {
    @Resource
    private IPaperFinalService iPaperfinalService;

    @GetMapping("list")
    @ResponseBody
    @RequiresPermissions("papers:view")
    public FebsResponse papersFinalList(QueryRequest request, PaperFinal paperFinal) {
        IPage<PaperFinal> papersPage = iPaperfinalService.findPaperfinalPage(request,paperFinal);
        Map<String, Object> dataTable = getDataTable(papersPage);
        return new FebsResponse().success().data(dataTable);
    }

    /**
     * 导出论文终稿信息表
     * @param queryRequest
     * @param paperFinal
     * @param response
     */
    @GetMapping("excel")
    @RequiresPermissions("papers:view")
    @ControllerEndpoint(exceptionMessage = "导出Excel失败")
    public void export(QueryRequest queryRequest, PaperFinal paperFinal, HttpServletResponse response) {
        List<PaperFinal> users = this.iPaperfinalService.findPaperfinalPage(queryRequest, paperFinal).getRecords();
        ExcelKit.$Export(Papers.class, response).downXlsx(users, false);
    }
}
