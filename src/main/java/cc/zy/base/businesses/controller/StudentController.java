package cc.zy.base.businesses.controller;

import cc.zy.base.common.annotation.ControllerEndpoint;
import cc.zy.base.common.exception.FebsException;
import cc.zy.base.common.utils.FebsUtil;
import cc.zy.base.common.entity.FebsConstant;
import cc.zy.base.common.controller.BaseController;
import cc.zy.base.common.entity.FebsResponse;
import cc.zy.base.common.entity.QueryRequest;
import cc.zy.base.businesses.entity.Student;
import cc.zy.base.businesses.service.IStudentService;
import com.wuwenze.poi.ExcelKit;
import lombok.extern.slf4j.Slf4j;
import lombok.RequiredArgsConstructor;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/**
 * Controller
 *
 * @author Jiangjinlin
 * @date 2021-01-25 11:45:25
 */
@Slf4j
@Validated
@Controller
@RequiredArgsConstructor
@RequestMapping("student")
public class StudentController extends BaseController {
    @Resource
    private final IStudentService studentService;

    //通过学生表查询批次
//    public FebsResponse selectStudentBatch(Student student,QueryRequest request){
//
//    }


//
    /**
     * 通用组件：选择学生
     * @param student
     * @param request
     * @return
     */

    @GetMapping("selectStudentlist")
    @ResponseBody
//    @RequiresPermissions("student:view")
    public FebsResponse selectStudentlist(Student student, QueryRequest request) {
        log.info("前台传来的id为："+student.getBatchId());
        Map<String,Object> dataTable = getDataTable(this.studentService.selectStudentDetailList(student,request));
        return new FebsResponse().success().data(dataTable);
    }


    @GetMapping(FebsConstant.VIEW_PREFIX + "student")
    public String studentIndex() {
        return FebsUtil.view("student/student");
    }

    @GetMapping("student")
    @ResponseBody
//    @RequiresPermissions("student:list")
    public FebsResponse getAllStudents(Student student) {
        return new FebsResponse().success().data(studentService.findStudents(student));
    }

//    @GetMapping("list")
//    @ResponseBody
////    @RequiresPermissions("student:list")
//    public FebsResponse studentList(QueryRequest request, Student student) {
//        Map<String, Object> dataTable = getDataTable(this.studentService.findStudents(request, student));
//        return new FebsResponse().success().data(dataTable);
//    }

    @ControllerEndpoint(operation = "新增Student", exceptionMessage = "新增Student失败")
    @PostMapping("student")
    @ResponseBody
//    @RequiresPermissions("student:add")
    public FebsResponse addStudent(@Valid Student student) {
        this.studentService.createStudent(student);
        return new FebsResponse().success();
    }

    @ControllerEndpoint(operation = "删除Student", exceptionMessage = "删除Student失败")
    @GetMapping("student/delete")
    @ResponseBody
//    @RequiresPermissions("student:delete")
    public FebsResponse deleteStudent(Student student) {
        this.studentService.deleteStudent(student);
        return new FebsResponse().success();
    }

    @ControllerEndpoint(operation = "修改Student", exceptionMessage = "修改Student失败")
    @PostMapping("student/update")
    @ResponseBody
//    @RequiresPermissions("student:update")
    public FebsResponse updateStudent(Student student) {
        this.studentService.updateStudent(student);
        return new FebsResponse().success();
    }

    @ControllerEndpoint(operation = "修改Student", exceptionMessage = "导出Excel失败")
    @PostMapping("student/excel")
    @ResponseBody
//    @RequiresPermissions("student:export")
    public void export(QueryRequest queryRequest, Student student, HttpServletResponse response) {
        List<Student> students = this.studentService.findStudents(queryRequest, student).getRecords();
        ExcelKit.$Export(Student.class, response).downXlsx(students, false);
    }

}
