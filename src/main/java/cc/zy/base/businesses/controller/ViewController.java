package cc.zy.base.businesses.controller;

import cc.zy.base.businesses.entity.*;
import cc.zy.base.businesses.service.*;
import cc.zy.base.common.controller.BaseController;
import cc.zy.base.common.entity.FebsConstant;
import cc.zy.base.common.utils.DateUtil;
import cc.zy.base.common.utils.FebsUtil;
import cc.zy.base.system.entity.User;
import cc.zy.base.system.service.IUserService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Jiangjinlin
 */
@Controller("businessesView")
@RequiredArgsConstructor
public class ViewController extends BaseController {

    private final ICollegeService collegeService;
    private IEntranceScoreService IEntranceScoreService;
    private final ITeachProgramService teachProgramService;
	private final IStudentService studentsService;
    private final IDicService dicService;
    private final IConfirmAddressService confirmAddressService;
	
    private final IStandardFeeService standardFeeService;
	private final ISealService sealService;
	
    private final IClassesService classesService;
    private final TransferApplicationService transferApplicationService;
    private final IVideoService videoService;
    private final ICTypeService cTypeService;

    /**
     * 论文的跳转控制转发
     */
    @GetMapping(FebsConstant.VIEW_PREFIX + "papers/list")
    @RequiresPermissions("papers:view")
    public String papers(){
        return FebsUtil.businessesView("paper/papers");
    }


    @GetMapping(FebsConstant.VIEW_PREFIX + "paperFinal/list")
    @RequiresPermissions("papers:view")
    public String paperFinal(){
        return FebsUtil.businessesView("paper/paperFinal");
    }

    @GetMapping(FebsConstant.VIEW_PREFIX + "college/list")
    @RequiresPermissions("college:view")
    public String college() {
        return FebsUtil.businessesView("college/college");
    }

    @GetMapping(FebsConstant.VIEW_PREFIX + "score/entranceScore")
    @RequiresPermissions("score:view")
    public String entranceScore() {
        return FebsUtil.businessesView("score/entranceScore");
    }
    @GetMapping(FebsConstant.VIEW_PREFIX + "score/termScore")
    @RequiresPermissions("score:view")
    public String termScore() {
        return FebsUtil.businessesView("score/termScore");
    }
    @GetMapping(FebsConstant.VIEW_PREFIX + "entranceScore/import")
    public String entranceScoreImport() {
        return FebsUtil.businessesView("score/entranceScoreImport");
    }
    @GetMapping(FebsConstant.VIEW_PREFIX + "college/detail/{id}")
    @RequiresPermissions("user:view")
    public String collegeDetail(@PathVariable Integer id, Model model) {
        resolveCollegeModel(id, model, true);
        return FebsUtil.businessesView("college/collegeDetail");
    }

    @GetMapping(FebsConstant.VIEW_PREFIX + "entranceScore/detail/{id}")
    @RequiresPermissions("user:view")
    public String entranceScoreDetail(@PathVariable Integer id, Model model) {
        resolveCollegeModel(id, model, true);
        return FebsUtil.businessesView("score/entranceScoreDetail");
    }


    @GetMapping(FebsConstant.VIEW_PREFIX + "college/update/{id}")
    public String collegeUpdate(@PathVariable Integer id, Model model) {
        resolveCollegeModel(id, model, false);
        return FebsUtil.businessesView("college/collegeUpdate");
    }
    @GetMapping(FebsConstant.VIEW_PREFIX + "college/add")
    public String collegeAdd() {
        return FebsUtil.businessesView("college/collegeAdd");
    }

    /**
     * 跳转教学计划页面
     */
    @GetMapping(FebsConstant.VIEW_PREFIX + "teachProgram/list")
//    @RequiresPermissions("teachProgram:view")
    public String teachProgram() {
        return FebsUtil.businessesView("teachProgram/teachProgram");
    }

    @GetMapping(FebsConstant.VIEW_PREFIX + "teachProgram/update/{id}")
    public String teachProgramUpdate(@PathVariable Integer id, Model model) {

        resolveTeachProgramModel(id, model, false);
        return FebsUtil.businessesView("teachProgram/teachProgramUpdate");
    }

    private void resolveTeachProgramModel(Integer id, Model model, Boolean transform) {

        TeachProgram teachProgram = teachProgramService.findTeachById(id);
        System.out.println(teachProgram);
        model.addAttribute("teachProgram", teachProgram);
    }

    /**
     * 跳转增加教学计划页面
     */
    @GetMapping(FebsConstant.VIEW_PREFIX + "teachProgram/add")
//    @RequiresPermissions("teachProgram:view")
    public String teachProgramAdd() {

        return FebsUtil.businessesView("teachProgram/teachProgramAdd");
    }

    private void resolveCollegeModel(Integer id, Model model, Boolean transform) {
        College college = collegeService.findById(id);
        if (college.getCreatedate() != null) {
            model.addAttribute("createDateTime", DateUtil.getDateFormat(college.getCreatedate(), DateUtil.FULL_TIME_SPLIT_PATTERN));
        }
        if (college.getUpdatedate() != null) {
            model.addAttribute("updateDateTime", DateUtil.getDateFormat(college.getUpdatedate(), DateUtil.FULL_TIME_SPLIT_PATTERN));
        }
        model.addAttribute("college", college);
    }

    private void resolveEntranceScoreModel(Integer id, Model model, Boolean transform) {
        College college = collegeService.findById(id);

        model.addAttribute("college", college);
    }

/**linan开始**/
//    private final IConfirmAddressService confirmAddressService;

    //confirmAddress
    @GetMapping(FebsConstant.VIEW_PREFIX + "confirmAddress/list")
//    @RequiresPermissions("confirmAddress:view")
    public String confirmAddress() {
        return FebsUtil.businessesView("confirmAddress/confirmAddress");
    }

    //增加确认地页面 confirmAddress
    @GetMapping(FebsConstant.VIEW_PREFIX + "confirmAddress/add")
    public String confirmAddressAdd() {
        return FebsUtil.businessesView("confirmAddress/confirmAddressAdd");
    }

    //修改确认地页面confirmAddress
    @GetMapping(FebsConstant.VIEW_PREFIX + "confirmAddress/update/{id}")
    public String confirmAddressUpdate(@PathVariable Integer id, Model model) {
        resolveAddressModel(id, model, false);
        return FebsUtil.businessesView("confirmAddress/confirmAddressUpdate");
    }


    //现场确认地confirmAddress
    private void resolveAddressModel(Integer id, Model model, Boolean transform) {
        ConfirmAddress confirmAddress = confirmAddressService.findById(id);
        model.addAttribute("confirmAddress", confirmAddress);
    }

    //现场确认地详情页面confirmAddress
    @GetMapping(FebsConstant.VIEW_PREFIX + "confirmAddress/detail/{id}")
    @RequiresPermissions("user:view")
    public String confirmAddresseDetail(@PathVariable Integer id, Model model) {
        resolveAddressModel(id, model, true);
        return FebsUtil.businessesView("confirmAddress/confirmAddressDetail");
    }
    /**linan结束**/

    /*广告开始-lijian*/
    private final IAdvertService advertService;
//    private final IUserService userService;
    @GetMapping(FebsConstant.VIEW_PREFIX + "advert/list")
    @RequiresPermissions("advert:view")
    public String advert() {
        return FebsUtil.businessesView("advert/advert");
    }
    @GetMapping(FebsConstant.VIEW_PREFIX + "advert/detail/{id}")
    @RequiresPermissions("user:view")
    public String advertDetail(@PathVariable Integer id, Model model) {
        resolveAdvertModel(id, model, true);
        return FebsUtil.businessesView("advert/advertDetail");
    }

    private void resolveAdvertModel(Integer id, Model model, Boolean transform) {
        Advert advertDetail = advertService.findAdvertDetailById(id);
        if (advertDetail.getCreateTime() != null) {
            model.addAttribute("createTime", DateUtil.getDateFormat(advertDetail.getCreateTime(), DateUtil.FULL_TIME_SPLIT_PATTERN));
        }
        model.addAttribute("advert", advertDetail);
    }
    @GetMapping(FebsConstant.VIEW_PREFIX + "advert/update/{id}")
    public String advertUpdate(@PathVariable Integer id, Model model) {
        resolveAdvertModel(id, model, false);
        return FebsUtil.businessesView("advert/advertUpdate");
    }
    @GetMapping(FebsConstant.VIEW_PREFIX + "advert/add")
    public String advertAdd(Model model) {
        User principal = userService.findByName(getCurrentUser().getUsername());
        model.addAttribute("username", principal.getUsername());
        model.addAttribute("userId", principal.getUserId());
        return FebsUtil.businessesView("advert/advertAdd");
    }
    @GetMapping(FebsConstant.VIEW_PREFIX + "advert/loopSort/{id}")
    public String advertLoopSort(@PathVariable Integer id, Model model) {
        long useLoopCount = advertService.findUseLoopCount();
        model.addAttribute("id",id);
        model.addAttribute("count",useLoopCount);
        return FebsUtil.businessesView("advert/advertLoopSort");
    }
    /**李健结束**/

    /**张志钊开始**/
    private final IUserService userService;
    //跳转selectStudent页面
    @GetMapping(FebsConstant.VIEW_PREFIX + "student/list")
    public String student() {
        return FebsUtil.businessesView("students/selectStudent");
    }

    //跳转到noticeAdd页面
    @GetMapping(FebsConstant.VIEW_PREFIX + "notice/add")
    public String noticeAdd(Model model) {
        User principal = userService.findByName(getCurrentUser().getUsername());
        model.addAttribute("username", principal.getUsername());//传递到noticeAdd页面
        model.addAttribute("userId", principal.getUserId());//传递到noticeAdd页面
        return FebsUtil.businessesView("notice/noticeAdd");
    }
    /**张志钊结束**/

/**赵佳伟开始**/
    private final ICorrespondenceCollegeService correspondenceCollegeService;
    private final IProvinceService provinceService;
    /**
     * 跳转到函授站新增页面
     */
    @GetMapping(FebsConstant.VIEW_PREFIX + "correspondenceCollege/add")
    @RequiresPermissions("college:view")
    public String countCorrespondenceCollegeAdd() {
        return FebsUtil.businessesView("correspondenceCollege/correspondenceCollegeAdd");
    }
    /**
     * 跳转到函授站信息页面
     * @return
     */
    @GetMapping(FebsConstant.VIEW_PREFIX + "correspondenceCollege/list")
    @RequiresPermissions("college:view")
    public String countCorrespondenceCollege() {
        return FebsUtil.businessesView("correspondenceCollege/correspondenceCollege");
    }

    /**
     * 跳转到函授站信息详情页面
     * @return
     */
    @GetMapping(FebsConstant.VIEW_PREFIX + "correspondence/detail/{id}")
    @RequiresPermissions("user:view")
    public String correspondenceCollegeDetail(@PathVariable Integer id, Model model) {
        resolvecorrespondenceModel(id, model, true);
        return FebsUtil.businessesView("correspondenceCollege/correspondenceCollegeDetail");
    }

    /**
     *跳转到函授站修改页面
     * @return
     */
    @GetMapping(FebsConstant.VIEW_PREFIX + "correspondenceCollege/update/{id}")
    public String correspondenceCollegeUpdate(@PathVariable Integer id, Model model) {
        resolvecorrespondenceUpdateModel(id, model, false);
        return FebsUtil.businessesView("correspondenceCollege/correspondenceCollegeUpdate");
    }

    /**
     * 跳转到广告日志页面
     * @return
     */
    @GetMapping(FebsConstant.VIEW_PREFIX + "advertLog/list")
    @RequiresPermissions("college:view")
    public String countAdvertLog() {
        return FebsUtil.businessesView("advert/advertLog");
    }

    /**
     * 显示详情页面
     * @param id 函授站id
     * @param model
     * @param transform
     */
    private void resolvecorrespondenceModel(Integer id, Model model, Boolean transform) {
        CorrespondenceCollege correspondenceCollege = correspondenceCollegeService.findCorrespondenceCollegeById(id);
        String province = correspondenceCollege.getProvinceName();
        String city = correspondenceCollege.getCityName();
        String area = correspondenceCollege.getAreaName();
        String address = correspondenceCollege.getAddress();
        address = province+city+area+address;
        correspondenceCollege.setAddress(address);
        if (correspondenceCollege.getCreateTime() != null) {
            model.addAttribute("createDateTime", DateUtil.getDateFormat(correspondenceCollege.getCreateTime(), DateUtil.FULL_TIME_SPLIT_PATTERN));
        }
        model.addAttribute("correspondenceCollege", correspondenceCollege);
    }

    /**
     * 修改页面拼接省市字符串
     * @param id 函授站id
     * @param model
     * @param transform
     */
    private void resolvecorrespondenceUpdateModel(Integer id, Model model, Boolean transform) {
        CorrespondenceCollege correspondenceCollege = correspondenceCollegeService.findCorrespondenceCollegeById(id);
//        String address = correspondenceCollege.getAddress();
//        String province = address.substring(0,address.indexOf("省")+1);
//        String city = address.substring(address.indexOf("省")+1,address.indexOf("市")+1);
//        String area = address.substring(address.indexOf("市")+1,address.indexOf("区")+1);
//        address = address.substring(address.indexOf("区")+1);
//        log.info("截取的省为："+province);
//        log.info("截取的市为："+city);
//        log.info("截取的区为："+area);
//        log.info("截取的剩下为："+address);
//        Integer provinceId = provinceService.findProvinceId(province);
//        log.info("获得的id是："+provinceId);
//        correspondenceCollege.setProvince(provinceId);
//        correspondenceCollege.setCity(city);
//        correspondenceCollege.setArea(area);
        model.addAttribute("correspondenceCollege", correspondenceCollege);
    }
    /**赵佳伟结束**/

    /**郭志坤开始**/
    private final ISubjectCategoryService subjectCategoryService;
    private final IMajorService majorService;
    //学科类别列表
    @GetMapping(FebsConstant.VIEW_PREFIX + "subjectCategory/list")
//    @RequiresPermissions("subjectCategory:view")
    public String subjectCategory() {
        return FebsUtil.businessesView("subjectCategory/subjectCategory");
    }
    //学科类别修改
    @GetMapping(FebsConstant.VIEW_PREFIX + "subjectCategory/update/{id}")
    public String subjectCategoryUpdate(@PathVariable Integer id, Model model) {
        resolveSubjectCategoryModel(id, model, false);
        return FebsUtil.businessesView("subjectCategory/subjectCategoryUpdate");
    }
    //学科类别新增
    @GetMapping(FebsConstant.VIEW_PREFIX + "subjectCategory/add")
    public String subjectCategoryAdd(Model model) {
        User principal = userService.findByName(getCurrentUser().getUsername());
        model.addAttribute("user", principal);
        return FebsUtil.businessesView("subjectCategory/subjectCategoryAdd");
    }
    //类别findById
    private void resolveSubjectCategoryModel(Integer id, Model model, boolean b) {
        SubjectCategory subjectCategory = subjectCategoryService.findById(id);

        if(subjectCategory.getCreateTime() != null){
            model.addAttribute("createDateTime",DateUtil.getDateFormat(subjectCategory.getCreateTime(),DateUtil.FULL_TIME_SPLIT_PATTERN));
        }
        model.addAttribute("subjectCategory",subjectCategory);
    }
    //学科类别详情
    @GetMapping(FebsConstant.VIEW_PREFIX + "subjectCategory/detail/{id}")
    @RequiresPermissions("user:view")
    public String subjectCategoryDetail(@PathVariable Integer id, Model model) {
        resolveSubjectCategoryModel(id, model, true);
        return FebsUtil.businessesView("subjectCategory/subjectCategoryDetail");
    }


    //院校专业列表
    @GetMapping(FebsConstant.VIEW_PREFIX + "major/list")
//    @RequiresPermissions("major:view")
    public String major() {
        return FebsUtil.businessesView("major/major");
    }
    //院校专业修改
    @GetMapping(FebsConstant.VIEW_PREFIX + "major/update/{id}")
    public String majorUpdate(@PathVariable Integer id, Model model) {
        resolveMajorModel(id, model, false);
        return FebsUtil.businessesView("major/majorUpdate");
    }
    //院校专业新增
    @GetMapping(FebsConstant.VIEW_PREFIX + "major/add")
    public String majorAdd(Model model) {
        User principal = userService.findByName(getCurrentUser().getUsername());
        model.addAttribute("user", principal);
        return FebsUtil.businessesView("major/majorAdd");
    }
    //院校专业详情
    @GetMapping(FebsConstant.VIEW_PREFIX + "major/detail/{id}")
    @RequiresPermissions("user:view")
    public String majorDetail(@PathVariable Integer id, Model model) {
        resolveMajorModel(id, model, true);
        return FebsUtil.businessesView("major/majorDetail");
    }
    //院校专业findById
    private void resolveMajorModel(Integer id, Model model, boolean b) {
        Major major = majorService.findById(id);
        System.out.println(major);
        if(major.getCreateTime() != null){
            model.addAttribute("createDateTime",DateUtil.getDateFormat(major.getCreateTime(),DateUtil.FULL_TIME_SPLIT_PATTERN));
        }
        model.addAttribute("major",major);
    }
    /**郭志坤结束**/

    /**杨东豪开始**/
    //跳转选择员工页面
    @GetMapping(FebsConstant.VIEW_PREFIX + "businesses/user")
    public String systemUser() {
        return FebsUtil.view("system/user/selectUser");
    }

    //跳转通知列表页面
    @GetMapping(FebsConstant.VIEW_PREFIX + "businesses/Notice")
    public String Notice() {
        return FebsUtil.businessesView("notice/notice");
    }

    //跳转通知
    @GetMapping(FebsConstant.VIEW_PREFIX + "businesses/noticeEdit")
    public String noticeEdit() {
        return FebsUtil.businessesView("notice/noticeEdit");
    }

    @GetMapping(FebsConstant.VIEW_PREFIX + "noticeUser/statistics/{id}")
//    @RequiresPermissions("major:view")
    public String statistics(@PathVariable Integer id, Model model) {
        //System.out.println(id+"============");
        model.addAttribute("noticeId",id);
        return FebsUtil.businessesView("noticeUser/noticeUser");
    }

    /**杨东豪结束**/

/**刘润雨开始**/

	  @ApiOperation("学生列表view")
    @GetMapping(FebsConstant.VIEW_PREFIX + "student/todolist")
    public String students() {
        System.out.println("到达学生列表");
        return FebsUtil.businessesView("students/studentsList");
    }

    @ApiOperation("回收站列表view")
    @GetMapping(FebsConstant.VIEW_PREFIX + "recycle/list")
   // @RequiresPermissions("recycle:view")
    public String recycleList() {
        return FebsUtil.businessesView("recycle/recycleList");
    }


    @ApiOperation("创建标签view")
    @GetMapping(FebsConstant.VIEW_PREFIX + "recycle/label")
//    @RequiresPermissions("college:view")
    public String recLabelAdd() {
        return FebsUtil.businessesView("recycle/recycleLabelAdd");
    }
    @ApiOperation("创建标签view")
    @GetMapping(FebsConstant.VIEW_PREFIX + "student/label")
//    @RequiresPermissions("college:view")
    public String stuLabelAdd() {
        return FebsUtil.businessesView("students/studentLabelAdd");
    }
	  //公用方法 获得学生
    private Student resolveStudentModel(Integer id, Model model) {
        Student student = studentsService.findById(id);
        try {
            Date graduDate = student.getGraduDate();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String graduDateStr = simpleDateFormat.format(graduDate);
            model.addAttribute("graduDateStr", graduDateStr);

        }
        catch (NullPointerException e){
            System.out.println("缺少日期信息");
        }
        model.addAttribute("student", student);
        return student;
    }
    //去学生详情页,传入学生ID
    @GetMapping(FebsConstant.VIEW_PREFIX+"student/detail/{id}")
    @ApiOperation("去学生详情页")
    public String goStudentDetai(@PathVariable Integer id,Model model){
       model.addAttribute("id",id);
        return FebsUtil.businessesView("students/studentDetail");
    }

    //去学生报考信息页面
    @GetMapping(FebsConstant.VIEW_PREFIX+"student/examinfo")
    @ApiOperation("去学生报考信息页")
    public String goStudentExamInfo(Integer id,Model model){
        resolveStudentModel(id, model);
        //获得学习形式
        List<Dic> studyType = dicService.findDicByType("study_type");
        model.addAttribute("studyType",studyType);
        return FebsUtil.businessesView("students/examInfo");
    }

    //去学生个人信息页面
    @GetMapping(FebsConstant.VIEW_PREFIX+"student/studentinfo")
    @ApiOperation("去学生报考信息页")
    public String goStudentInfo(Integer id,Model model){
        resolveStudentModel(id, model);
        //获得学习形式
        return FebsUtil.businessesView("students/studentInfo");
    }

    //去学生现场确认页面
    @GetMapping(FebsConstant.VIEW_PREFIX+"student/examlocationconfirm")
    @ApiOperation("去学生现场确认页面")
    public String goExamLocationConfirm(Integer id,Model model)  {
        //是否需要审核
        int check;
        Student student = studentsService.findById(id);
        Integer examLocationId = student.getExamLocationId();

        Map<String, Object> task = studentsService.findTaskByStuidAndType(id, 5);
        System.out.println(task);

        if(task!=null){
            Object after_update = task.get("AFTER_UPDATE");
           // System.out.println(after_update);
            try {
                JSONObject jsonObject = new JSONObject((String)after_update);
                String examLocationId1 = jsonObject.getString("examLocationId");
                //System.out.println("examLocationId为"+examLocationId);
                check=1;
                model.addAttribute("student",student);
                model.addAttribute("check",check);
                ConfirmAddress confirmAddress = confirmAddressService.findById(Integer.parseInt(examLocationId1));
               // System.out.println(confirmAddress);
                int taskId = (int)task.get("ID");
                model.addAttribute("taskId",taskId);
                model.addAttribute("examLocationId",confirmAddress);
                return FebsUtil.businessesView("students/examLocationConfirm");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        else  if(examLocationId!=null&&examLocationId!=0){
            System.out.println("进入check2界面"+examLocationId);
            check=2;
            model.addAttribute("student",student);
            model.addAttribute("check",check);
            ConfirmAddress confirmAddress =confirmAddressService.findById(examLocationId);
            System.out.println("查到的为"+confirmAddress);
            model.addAttribute("examLocationId",confirmAddress);
            model.addAttribute("taskId",null);
            return FebsUtil.businessesView("students/examLocationConfirm");
        }
        else {
             check=3;
            model.addAttribute("student",student);
            model.addAttribute("check",check);
            model.addAttribute("examLocationId",null);
            model.addAttribute("taskId",null);
            return FebsUtil.businessesView("students/examLocationConfirm");
        }

        return FebsUtil.businessesView("students/examLocationConfirm");
    }



    @GetMapping(FebsConstant.VIEW_PREFIX + "student/update/{id}")
    public String studentUpdate(@PathVariable Integer id, Model model) {
        System.out.println("进入student");
        resolveStudentModel(id, model, false);
        return FebsUtil.businessesView("students/identityConfirm");
    }

    private void resolveStudentModel(Integer id, Model model, Boolean transform) {
        Student student = studentsService.findStudentById(id);
        model.addAttribute("student", student);
    }
    @ApiOperation("去学生身份证确认页")
    @GetMapping(FebsConstant.VIEW_PREFIX + "student/affirm/identity")
    public String studentAffIdentity(Integer id,Integer typeId, Model model) {
        resolveStudentModel(id, model);
        Map<String, Object> indenDetail = studentsService.findTaskDetail(id,typeId);
        if (indenDetail!=null){
            Set<Map.Entry<String, Object>> entries = indenDetail.entrySet();
            for (Map.Entry<String, Object> entry : entries) {
                if (entry.getKey().equals("AFTER_UPDATE")){
                    entry.getValue();
                    JSONObject jsonObject = null;
                    try {
                        jsonObject = new JSONObject((String) entry.getValue());
                        String idenFront = jsonObject.getString("iden_Front_Img_URL");
                        String idenBack = jsonObject.getString("iden_Back_Img_URL");
                        model.addAttribute("idenFront", idenFront);
                        model.addAttribute("idenBack", idenBack);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            model.addAttribute("indetity",indenDetail);
        }
        return FebsUtil.businessesView("students/identityConfirm");
    }

    @GetMapping(FebsConstant.VIEW_PREFIX + "student/affirm/nextIden")
    @ApiOperation("去下一个身份证确认页")
    public String nextIden(Integer id, Integer typeId,String classId, Model model) {
        Integer ids=id;
        Integer[] allId = studentsService.findStuIdBuClassId(classId);
        Map<String, Object> photoDetail;
        do {
            id = id + 1;
            photoDetail = studentsService.findTaskDetail(id, typeId);
            try {
                resolveStudentModel(id, model);
            }catch (Exception e){

            }
        }while (photoDetail==null&&id<allId[allId.length-1]);
        if (id>=allId[allId.length-1]) {
            id = ids;
            photoDetail = studentsService.findTaskDetail(id, typeId);
            resolveStudentModel(id, model);
        }
        Set<Map.Entry<String, Object>> entries = photoDetail.entrySet();
        for (Map.Entry<String, Object> entry : entries) {
            if (entry.getKey().equals("AFTER_UPDATE")){
                entry.getValue();
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject((String) entry.getValue());
                    String idenFront = jsonObject.getString("iden_Front_Img_URL");
                    String idenBack = jsonObject.getString("iden_Back_Img_URL");
                    model.addAttribute("idenFront", idenFront);
                    model.addAttribute("idenBack", idenBack);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return FebsUtil.businessesView("students/identityConfirm");
    }

    @GetMapping(FebsConstant.VIEW_PREFIX + "student/affirm/preIden")
    @ApiOperation("去上一个身份证确认页")
    public String preIden(Integer id, Integer typeId,String classId, Model model) {
        Integer ids=id;
        Integer[] allId = studentsService.findStuIdBuClassId(classId);
        Map<String, Object> photoDetail;
        do {
            id = id - 1;
            photoDetail = studentsService.findTaskDetail(id, typeId);
            try {
                resolveStudentModel(id, model);
            }catch (Exception e){

            }
        }while (photoDetail==null&&id>allId[0]);
        if (id<=allId[0]) {
            id = ids;
            photoDetail = studentsService.findTaskDetail(id, typeId);
            resolveStudentModel(id, model);
        }
        Set<Map.Entry<String, Object>> entries = photoDetail.entrySet();
        for (Map.Entry<String, Object> entry : entries) {
            if (entry.getKey().equals("AFTER_UPDATE")){
                entry.getValue();
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject((String) entry.getValue());
                    String idenFront = jsonObject.getString("iden_Front_Img_URL");
                    String idenBack = jsonObject.getString("iden_Back_Img_URL");
                    model.addAttribute("idenFront", idenFront);
                    model.addAttribute("idenBack", idenBack);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        return FebsUtil.businessesView("students/identityConfirm");
    }


    @ApiOperation("去学生照片确认页")
    @GetMapping(FebsConstant.VIEW_PREFIX + "student/affirm/photo")
    public String studentAffPhoto(Integer id,Integer typeId, Model model) {
        resolveStudentModel(id, model);
        Map<String, Object> photoDetail = studentsService.findTaskDetail(id,typeId);
        if (photoDetail!=null){
            Set<Map.Entry<String, Object>> entries = photoDetail.entrySet();
            for (Map.Entry<String, Object> entry : entries) {
                if (entry.getKey().equals("AFTER_UPDATE")){
                    entry.getValue();
                    JSONObject jsonObject = null;
                    try {
                        jsonObject = new JSONObject((String) entry.getValue());
                        String headImgUrl = jsonObject.getString("Head_Img_Url");
                        model.addAttribute("photo", headImgUrl);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return FebsUtil.businessesView("students/photoConfirm");
    }

    @GetMapping(FebsConstant.VIEW_PREFIX + "student/affirm/nextPhoto")
    @ApiOperation("去下一个学生照片确认页")
    public String nextPhoto(Integer id, Integer typeId,String classId, Model model) {
        Integer ids=id;
        Integer[] allId = studentsService.findStuIdBuClassId(classId);
        Map<String, Object> photoDetail;

        do {
            id = id + 1;
            photoDetail = studentsService.findTaskDetail(id, typeId);
            try {
                resolveStudentModel(id, model);
            }catch (Exception e){

            }
        }while (photoDetail==null&&id<allId[allId.length-1]);
        if (id>=allId[allId.length-1]) {
            id = ids;
            photoDetail = studentsService.findTaskDetail(id, typeId);
            resolveStudentModel(id, model);
        }
        Set<Map.Entry<String, Object>> entries = photoDetail.entrySet();
        for (Map.Entry<String, Object> entry : entries) {
            if (entry.getKey().equals("AFTER_UPDATE")){
                entry.getValue();
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject((String) entry.getValue());
                    String headImgUrl = jsonObject.getString("Head_Img_Url");
                    model.addAttribute("photo", headImgUrl);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return FebsUtil.businessesView("students/photoConfirm");
    }

    @GetMapping(FebsConstant.VIEW_PREFIX + "student/affirm/prePhoto")
    @ApiOperation("去上一个学生照片确认页")
    public String prePhoto(Integer id, Integer typeId,String classId, Model model) {
        Integer ids=id;
        Integer[] allId = studentsService.findStuIdBuClassId(classId);
        Map<String, Object> photoDetail;
        do {
            id = id - 1;
            photoDetail = studentsService.findTaskDetail(id, typeId);
            try {
                resolveStudentModel(id, model);
            }catch (Exception e){

            }
        }while (photoDetail==null&&id>allId[0]);
        if (id<=allId[0]) {
            id = ids;
            photoDetail = studentsService.findTaskDetail(id, typeId);
            resolveStudentModel(id, model);
        }
            Set<Map.Entry<String, Object>> entries = photoDetail.entrySet();
            for (Map.Entry<String, Object> entry : entries) {
                if (entry.getKey().equals("AFTER_UPDATE")){
                    entry.getValue();
                    JSONObject jsonObject = null;
                    try {
                        jsonObject = new JSONObject((String) entry.getValue());
                        String headImgUrl = jsonObject.getString("Head_Img_Url");
                        model.addAttribute("photo", headImgUrl);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

        return FebsUtil.businessesView("students/photoConfirm");
    }

    @ApiOperation("去毕业证确认页")
    @GetMapping(FebsConstant.VIEW_PREFIX + "student/affirm/diploma")
    public String studentAffDiploma(Integer id,Integer typeId, Model model) {
        resolveStudentModel(id, model);
        Map<String, Object> photoDetail = studentsService.findTaskDetail(id,typeId);
        if (photoDetail!=null){
            Set<Map.Entry<String, Object>> entries = photoDetail.entrySet();
            for (Map.Entry<String, Object> entry : entries) {
                if (entry.getKey().equals("AFTER_UPDATE")){
                    entry.getValue();
                    JSONObject jsonObject = null;
                    try {
                        jsonObject = new JSONObject((String) entry.getValue());
                        String diplomaImgUrl = jsonObject.getString("Diploma_Img_Url");
                        model.addAttribute("diploma", diplomaImgUrl);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return FebsUtil.businessesView("students/diplomaConfirm");
    }

    //去学生个人信息页面
    @GetMapping(FebsConstant.VIEW_PREFIX+"student/cost")
    @ApiOperation("去学生缴费信息页")
    public String cost(Integer id,Model model){
        resolveStudentModel(id, model);
        //获得学习形式
        return FebsUtil.businessesView("students/payDetail");
    }
	/**刘润雨结束**/
	
	
	/*
    *
    * 裴昊东
    *
    * */
    @GetMapping(FebsConstant.VIEW_PREFIX + "reqInfo/list")
//    @RequiresPermissions("college:view")
    public String reqInfo() {
        return FebsUtil.businessesView("reqInfo/reqInfo");
    }



    @GetMapping(FebsConstant.VIEW_PREFIX + "reqInfo/test")
//    @RequiresPermissions("college:view")
    public ModelAndView reqInfo(ModelAndView modelAndView) {
//        modelAndView.setViewName("forward:/reqInfo/test");
        modelAndView.setViewName("forward:/studentpool/add");
        return modelAndView;
    }


    /*
    *  李旭
    *
    * */

    private final IResolveExceptionService resolveExceptionService;

    @GetMapping(FebsConstant.VIEW_PREFIX + "resolveException/list")
//    @RequiresPermissions("college:view")
    public String resolveException() {
        return FebsUtil.businessesView("resolveException/resolveException");
    }


    @GetMapping(FebsConstant.VIEW_PREFIX+"resolveException/detail/{id}")
//    @RequiresPermissions("college:view")
    public String resolveExceptionDetail(@PathVariable Integer id, Model model) {
        resolveExceptionModel(id, model, true);
        return FebsUtil.businessesView("resolveException/resolveExceptionDetail");
    }

    private void resolveExceptionModel(Integer id, Model model, Boolean transform) {
        ResolveException resolveException = resolveExceptionService.findById(id);
        if (resolveException.getExceptionTime() != null) {
            model.addAttribute("exceptionTime", DateUtil.getDateFormat(resolveException.getExceptionTime(), DateUtil.FULL_TIME_SPLIT_PATTERN));
        }
        model.addAttribute("resolveException", resolveException);
    }

	//********************************李文龙印章跳转区间********************************************

    /**
     * 印章跳转主页面
     * @return
     */
    @GetMapping(FebsConstant.VIEW_PREFIX + "seal/list")
    @RequiresPermissions("seal:view")
    public String seal() {
        return FebsUtil.businessesView("seal/seal");
    }

    @GetMapping(FebsConstant.VIEW_PREFIX + "seal/add")
    public String sealAdd() {
        return FebsUtil.businessesView("seal/sealAdd");
    }

    /**
     * 用于添加功能信息中的返回
     * @param id
     * @param model
     * @return
     */
    @GetMapping(FebsConstant.VIEW_PREFIX + "seal/detail/{id}")
    public String sealdetail(@PathVariable Integer id, Model model) {
        System.out.println("ViewController.sealdetail"+id);
        resolveSealModel(id, model, false);
        return FebsUtil.businessesView("seal/sealUpdate");
    }

    /**查看详细功能的
     *
     * @param id
     * @param model
     * @return
     */
    @GetMapping(FebsConstant.VIEW_PREFIX + "seal/details/{id}")
    public String sealdetails(@PathVariable Integer id, Model model) {
        resolveSealModel(id, model, false);
        return FebsUtil.businessesView("seal/sealDetail");
    }

    /**
     *
     * @param id
     * @param model
     * @param transform
     */
    private void resolveSealModel(Integer id, Model model, Boolean transform) {
        College college = sealService.findSealById(id);
        model.addAttribute("college", college);
    }


    /**
     * 套内缴费管理页面
     * @return
     */
    @GetMapping(FebsConstant.VIEW_PREFIX + "standardFee/list")
    @RequiresPermissions("standardFee:view")
    public String standardFeeIndex() {
        return FebsUtil.businessesView("standardFee/standardFee");
    }

    /**
     * 创建套内缴费
     * @return
     */
    @GetMapping(FebsConstant.VIEW_PREFIX + "standardFee/add")

    public String standardFeeAdd() {
          return FebsUtil.businessesView("standardFee/standardFeeAdd");
    }

       @GetMapping(FebsConstant.VIEW_PREFIX + "standardFee/update/{id}")
    public String standardFeeUpdate(@PathVariable Integer id, Model model) {
           System.out.println("进入套内缴费修改页面");
        resolveStandardFeeModel(id, model, false);
        return FebsUtil.businessesView("standardFee/standardFeeUpdate");
    }


//
//      /**
//     * 批量复制批次套内缴费信息
//     * @return
//     */
//    @GetMapping(FebsConstant.VIEW_PREFIX + "standardFee/copy")
//
//    public String batchStandardFeeAdd() {
//          return FebsUtil.businessesView("standardFee/batchStandardFeeAdd");
//    }


        private void resolveStandardFeeModel(Integer id, Model model, Boolean transform) {

            StandardFee standardFee = standardFeeService.findById(id);
//            if (college.getCreatedate() != null) {
//            model.addAttribute("createDateTime", DateUtil.getDateFormat(college.getCreatedate(), DateUtil.FULL_TIME_SPLIT_PATTERN));
//        }
//        if (college.getUpdatedate() != null) {
//            model.addAttribute("updateDateTime", DateUtil.getDateFormat(college.getUpdatedate(), DateUtil.FULL_TIME_SPLIT_PATTERN));
//        }
        model.addAttribute("standardFee", standardFee);
    }


    //********************************李文龙印章跳转区间********************************************



	//********************************刘恒start********************************************

	@GetMapping(FebsConstant.VIEW_PREFIX + "transferApplication/list")
    @RequiresPermissions("transferApplication:view")
    public String transferApplication() {
        return FebsUtil.businessesView("transferApplication/transferApplication");
    }

    @GetMapping(FebsConstant.VIEW_PREFIX + "transferApplication/update/{id}")
    public String transferApplicationUpdate(@PathVariable Integer id, Model model) {
        int transferTypeId = resolveTransferApplicationModel(id, model, true);
        if (transferTypeId == 24) {
            return FebsUtil.businessesView("transferApplication/suspensionCollegeDetail");
        } else if (transferTypeId == 25) {
            return FebsUtil.businessesView("transferApplication/backCollegeDetail");
        } else if (transferTypeId == 26) {
            return FebsUtil.businessesView("transferApplication/dropOutDetail");
        } else if (transferTypeId == 27) {
            return FebsUtil.businessesView("transferApplication/majorChangeDetail");
        } else {
            return FebsUtil.businessesView("transferApplication/studyTypeChangeDetail");
        }
    }


	//li
    @GetMapping(FebsConstant.VIEW_PREFIX + "group/grouping")
    @RequiresPermissions("group:view")
    public String grouping() {
        return FebsUtil.businessesView("group/grouping");
    }

    @GetMapping(FebsConstant.VIEW_PREFIX + "class/classGrouping")
    @RequiresPermissions("class:view")
    public String classGrouping() {
        return FebsUtil.businessesView("class/classGrouping");
    }

    @GetMapping(FebsConstant.VIEW_PREFIX + "class/detail/{id}")
    public String classDetail(@PathVariable Integer id, Model model) {
        resolveClassModel(id, model, true);
        return FebsUtil.businessesView("class/classDetail");
    }

    private void resolveClassModel(Integer id, Model model, Boolean transform) {
        System.out.println(id);
        Classes classes = this.classesService.findClassesById(id);
        if (classes.getCreateTime() != null) {
            model.addAttribute("createDateTime", DateUtil.getDateFormat(classes.getCreateTime(), DateUtil.FULL_TIME_SPLIT_PATTERN));
        }
        model.addAttribute("classes", classes);
    }

    //hou
    @GetMapping(FebsConstant.VIEW_PREFIX + "class/add")
    public String classAdd() {
        return FebsUtil.businessesView("class/classAdd");
    }

    @GetMapping(FebsConstant.VIEW_PREFIX + "classes/list")
    @RequiresPermissions("class:view")
    public String classes() {
        return FebsUtil.businessesView("class/class");
    }

    @GetMapping(FebsConstant.VIEW_PREFIX + "group/add")
    public String groupAdd() {
        return FebsUtil.businessesView("group/groupAdd");
    }

    @GetMapping(FebsConstant.VIEW_PREFIX + "studentGroup/list")
    @RequiresPermissions("studentGroup:view")
    public String group() {
        return FebsUtil.businessesView("group/group");
    }


    @GetMapping(FebsConstant.VIEW_PREFIX + "teacher/update/{id}")
    public String teacherUpdate(@PathVariable Integer id, Model model) {
        resolveTeacherModel(id, model, false);
        return FebsUtil.businessesView("class/teacherUpdate");
    }

    private void resolveTeacherModel(Integer id, Model model, Boolean transform) {
        Classes classes = classesService.findById(id);
        if (classes.getCreateTime() != null) {
            model.addAttribute("createTime", DateUtil.getDateFormat(classes.getCreateTime(), DateUtil.FULL_TIME_SPLIT_PATTERN));
        }
        model.addAttribute("classes", classes);
    }

    private Integer resolveTransferApplicationModel(Integer id, Model model, Boolean transform) {
        TransferApplicationVo transferApplication = transferApplicationService.findTransferDetailById(id);
        model.addAttribute("transferApplication", transferApplication);
        return transferApplication.getTransferTypeId();
    }


	//********************************刘恒end********************************************
	
	//********************************夏凯start********************************************
	 @GetMapping(FebsConstant.VIEW_PREFIX + "video/list")
    public String video() {
        return FebsUtil.businessesView("video/video");
    }
    @GetMapping(FebsConstant.VIEW_PREFIX + "video/add")
    public String videoadd() {
        return FebsUtil.businessesView("video/videoAdd");
    }
    @GetMapping(FebsConstant.VIEW_PREFIX + "video/update/{id}")
    public String videoUpdate(@PathVariable Integer id, Model model) {
        resolveVideoModel1(id, model, false);
        return FebsUtil.businessesView("video/videoUpdate");
    }
    @GetMapping(FebsConstant.VIEW_PREFIX + "video/updateSort/{id}")
    public String updateBySort(@PathVariable Integer id, Model model) {
        resolveVideoModel(id, model, false);
        return FebsUtil.businessesView("video/videoSort");
    }
    
    private void resolveVideoModel(@PathVariable Integer id, Model model, Boolean transform) {
        Video video = videoService.findById(id);
        
        model.addAttribute("video", video);
    }
    @GetMapping(FebsConstant.VIEW_PREFIX + "cType/add")
    public String cTypeAdd() {
        return FebsUtil.businessesView("video/cTypeAdd");
    }
    @GetMapping(FebsConstant.VIEW_PREFIX + "cType/list")

    public String cType() {
        return FebsUtil.businessesView("video/cType");
    }
    @GetMapping(FebsConstant.VIEW_PREFIX + "cType/sort/{id}")

    public String cTypeSort(@PathVariable Integer id, Model model, Boolean transform) {
        resolveCTypeModel(id,model,false);
        return FebsUtil.businessesView("video/cTypeSort");
    }
    private void resolveCTypeModel(@PathVariable Integer id, Model model, Boolean transform) {
        CType cType = cTypeService.findById(id);
        if (cType.getCreateTime() != null) {
            model.addAttribute("createTime", DateUtil.getDateFormat(cType.getCreateTime(), DateUtil.FULL_TIME_SPLIT_PATTERN));
        }
        if (cType.getUpdateTime() != null) {
            model.addAttribute("updateTime", DateUtil.getDateFormat(cType.getUpdateTime(), DateUtil.FULL_TIME_SPLIT_PATTERN));
        }
        model.addAttribute("cType", cType);
    }
    private void resolveVideoModel1(Integer id, Model model, Boolean transform) {
        Video video = videoService.findById(id);
        int typeId = videoService.findTypeIdByVid(id);
        //int courseId = videoService.findCourseIdByVid(id);
        model.addAttribute("video", video);
        model.addAttribute("typeId", typeId);
        //model.addAttribute("courseId", courseId);
    }
    @GetMapping(FebsConstant.VIEW_PREFIX + "video/detail/{id}")
    public String videoDetail(@PathVariable Integer id, Model model) {
        resolveVideoModel1(id, model, false);
        return FebsUtil.businessesView("video/videoDetail");
    }
    @GetMapping(FebsConstant.VIEW_PREFIX + "cType/update/{id}")
    public String cTypeUpdate(@PathVariable Integer id, Model model) {
        resolveCTypeModel(id, model, false);
        return FebsUtil.businessesView("video/cTypeUpdate");
    }
    
    //********************************夏凯end********************************************

}
