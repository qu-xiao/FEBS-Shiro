package cc.zy.base.businesses.controller;

import cc.zy.base.businesses.entity.StudentGroup;
import cc.zy.base.businesses.service.IStudentGroupService;
import cc.zy.base.common.annotation.ControllerEndpoint;
import cc.zy.base.common.controller.BaseController;
import cc.zy.base.common.entity.FebsResponse;
import cc.zy.base.common.entity.QueryRequest;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.wuwenze.poi.ExcelKit;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.Map;

/**
 * 学生组表	 Controller
 *
 * @author LiPeng
 * @date 2021-01-26 16:24:15
 */
@Slf4j
@Validated
@Controller
@RequiredArgsConstructor
@RequestMapping("group")
public class StudentGroupController extends BaseController {

    private final IStudentGroupService studentGroupService;

    @GetMapping("select")
    @ResponseBody
    public FebsResponse getAllStudentGroups(StudentGroup studentGroup) {
        return new FebsResponse().success().data(studentGroupService.findStudentGroups(studentGroup));
    }

    @GetMapping("list")
    @ResponseBody
    @RequiresPermissions("studentGroup:view")
    public FebsResponse studentGroupList(QueryRequest request, StudentGroup studentGroup) {
        Map<String, Object> dataTable = getDataTable(this.studentGroupService.findStudentGroups(request, studentGroup));
        return new FebsResponse().success().data(dataTable);
    }

    @ControllerEndpoint(operation = "删除学生组", exceptionMessage = "删除学生组失败")
    @GetMapping("delete/{groupIds}")
    @ResponseBody
    public FebsResponse deleteStudentGroup(@NotBlank(message = "{required}") @PathVariable String groupIds) {
        String[] ids = groupIds.split(StringPool.COMMA);
        this.studentGroupService.deleteStudentGroup(ids);
        return new FebsResponse().success();
    }
    @PostMapping("add")
    @ResponseBody
    @ControllerEndpoint(operation = "新增组", exceptionMessage = "新增组失败")
    public FebsResponse addCollege(@Valid StudentGroup studentGroup) {
        this.studentGroupService.createGroup(studentGroup);
        return new FebsResponse().success();
    }
    @GetMapping("excel")
    @ControllerEndpoint(exceptionMessage = "导出Excel失败")
    public void export(QueryRequest queryRequest, StudentGroup studentGroup, HttpServletResponse response) {
        List<StudentGroup> studentGroups= this.studentGroupService.findStudentGroups(queryRequest,studentGroup).getRecords();
        ExcelKit.$Export(StudentGroup.class, response).downXlsx(studentGroups, false);
    }

}
