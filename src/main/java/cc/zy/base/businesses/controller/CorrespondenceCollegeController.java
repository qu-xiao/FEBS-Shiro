package cc.zy.base.businesses.controller;


import cc.zy.base.businesses.entity.Area;
import cc.zy.base.businesses.entity.City;
import cc.zy.base.businesses.entity.CorrespondenceCollege;
import cc.zy.base.businesses.entity.Province;
import cc.zy.base.businesses.service.IAreaService;
import cc.zy.base.businesses.service.ICityService;
import cc.zy.base.businesses.service.ICorrespondenceCollegeService;
import cc.zy.base.businesses.service.IProvinceService;
import cc.zy.base.common.annotation.ControllerEndpoint;
import cc.zy.base.common.controller.BaseController;
import cc.zy.base.common.entity.FebsConstant;
import cc.zy.base.common.entity.FebsResponse;

import cc.zy.base.common.entity.QueryRequest;
import cc.zy.base.common.utils.FebsUtil;

import cc.zy.base.system.entity.User;
import cc.zy.base.system.service.IUserService;
import lombok.extern.slf4j.Slf4j;
import lombok.RequiredArgsConstructor;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


import javax.validation.Valid;

import java.util.List;
import java.util.Map;

/**
 * 函授站表 Controller
 *
 * @author Jiangjinlin
 * @date 2021-01-25 19:37:31
 */
@Slf4j
@Validated
@Controller
@RequiredArgsConstructor
public class CorrespondenceCollegeController extends BaseController {

    private final ICorrespondenceCollegeService correspondenceCollegeService;
    private final IProvinceService provinceService;
    private final ICityService cityService;
    private final IAreaService areaService;
    private final IUserService userService;


    @GetMapping("correspondenceCollege/list")
    @ResponseBody
    //@RequiresPermissions("correspondenceCollege:list")
    public FebsResponse correspondenceCollegeList(QueryRequest request,String province,String city,String txtCollege) {
        log.info("province"+province);
        log.info("city"+city);
//        if(province==null || "".equals(province)){
//           province = "";
//       }else{
//           List<Province> provinces = provinceService.findProvinces(Integer.parseInt(province));
//           province = provinces.get(0).getProvinceName();
//       }
//       if(city == null || "".equals(city)){
//           city ="";
//       }else{
//           City cityById = cityService.findCityById(Integer.parseInt(city));
//           city = cityById.getCityName();
//       }

//        String address = province+city;
//        log.info("address"+address);
        Map<String, Object> dataTable = getDataTable(this.correspondenceCollegeService.findCorrespondenceColleges(request,province,city,txtCollege));
        log.info("返回的集合为"+dataTable);
        return new FebsResponse().success().data(dataTable);
    }

    @ControllerEndpoint(operation = "新增CorrespondenceCollege", exceptionMessage = "新增CorrespondenceCollege失败")
    @PostMapping("correspondenceCollege/add")
    @ResponseBody
    //@RequiresPermissions("correspondenceCollege:add")
    public FebsResponse addCorrespondenceCollege(@Valid CorrespondenceCollege correspondenceCollege,String province,String city,String area) {
        log.info("前台传来的省为"+province);
        log.info("前台传来的市为"+city);
        log.info("前台传来的地区为"+area);
        correspondenceCollege.setProvinceId(Integer.parseInt(province));
        correspondenceCollege.setCityId(Integer.parseInt(city));
        correspondenceCollege.setAreaId(Integer.parseInt(area));
        User principal = userService.findByName(getCurrentUser().getUsername());
        int userId = Integer.parseInt(principal.getUserId()+"");
        log.info("转换后的userid为："+userId);
        correspondenceCollege.setCreateUserId(userId);
        this.correspondenceCollegeService.createCorrespondenceCollege(correspondenceCollege);
        return new FebsResponse().success();
    }



    @ControllerEndpoint(operation = "修改CorrespondenceCollege", exceptionMessage = "修改CorrespondenceCollege失败")
    @PostMapping("correspondenceCollege/update")
    @ResponseBody
    //@RequiresPermissions("correspondenceCollege:update")
    public FebsResponse updateCorrespondenceCollege(CorrespondenceCollege correspondenceCollege) {
        log.info("前台给的id为："+correspondenceCollege.getId());
        this.correspondenceCollegeService.updateCorrespondenceCollege(correspondenceCollege);
        return new FebsResponse().success();
    }


}
