package cc.zy.base.businesses.controller;


import cc.zy.base.businesses.entity.Batch;
import cc.zy.base.businesses.entity.TeachProgram;
import cc.zy.base.businesses.service.ITeachProgramService;
import cc.zy.base.common.annotation.ControllerEndpoint;
import cc.zy.base.common.controller.BaseController;
import cc.zy.base.common.entity.FebsConstant;
import cc.zy.base.common.entity.FebsResponse;
import cc.zy.base.common.entity.QueryRequest;
import cc.zy.base.common.exception.FebsException;
import cc.zy.base.common.utils.FebsUtil;
import com.wuwenze.poi.ExcelKit;
import lombok.extern.slf4j.Slf4j;
import lombok.RequiredArgsConstructor;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.Map;

/**
 *  Controller
 *
 * @author pzj
 * @date 2021-01-25 09:38:13
 */
@Slf4j
@Validated
@Controller
@RequiredArgsConstructor
@RequestMapping("teachProgram")
public class TeachProgramController extends BaseController {

    @Resource
    private final ITeachProgramService teachProgramService;

    @GetMapping(FebsConstant.VIEW_BUS_PREFIX + "teachProgram")
    public String teachProgramIndex(){
        return FebsUtil.view("teachProgram/teachProgram");
    }

    /**
     * 页面展示
     * @param teachProgram
     * @return
     */
    @GetMapping("teachProgram")
    @ResponseBody
    @RequiresPermissions("teachProgram:list")
    public FebsResponse getAllTTeachPrograms(TeachProgram teachProgram) {
        return new FebsResponse().success().data(teachProgramService.findTeachPrograms(teachProgram));
    }

    @GetMapping("list")
    @ResponseBody
//    @RequiresPermissions("teachProgram:view")
    public FebsResponse teachProgramList(QueryRequest request, TeachProgram teachProgram) {
        Map<String, Object> dataTable = getDataTable(this.teachProgramService.findTeachPrograms(request, teachProgram));
        return new FebsResponse().success().data(dataTable);
    }

    @ControllerEndpoint(operation = "新增TeachProgram", exceptionMessage = "新增TeachProgram失败")
    @PostMapping("teachProgram")
    @ResponseBody
    @RequiresPermissions("teachProgram:add")
    public FebsResponse addTTeachProgram( TeachProgram teachProgram) {
        this.teachProgramService.createTeachProgram(teachProgram);
        return new FebsResponse().success();
    }

    @GetMapping("batchList")
    @ResponseBody
    public FebsResponse BatchList(Batch batch) {
        return new FebsResponse().success().data(teachProgramService.selectAllBatch(batch));
    }

    @GetMapping("newBatchList")
    @ResponseBody
    public FebsResponse newBatchList(String batchName) {
        System.out.println(batchName);
        return new FebsResponse().success().data(teachProgramService.selectMoreBatch(batchName));
    }

    @ControllerEndpoint(operation = "批量增加TeachProgram", exceptionMessage = "批量增加TeachProgram失败")
    @GetMapping("add")
    @ResponseBody
//    @RequiresPermissions("teachProgram:add")
    public FebsResponse addTeachProgram(String batchName,Integer newBatchId) {
        System.out.println(batchName);
        System.out.println(newBatchId);
        String info;
        List<TeachProgram> teachPrograms = teachProgramService.selectBatchByBatchName(batchName);
        for(TeachProgram program : teachPrograms){
            program.setBatchId(newBatchId);
            info = program.getBatchName()+ ">" +program.getLevel() + ">" +program.getTypeName() + ">"
                    + program.getStudyMode() + ">" + program.getCollegeName() + ">" + program.getMajorName() + ">"
                    +program.getSchool() + ">" +program.getYear() + ">" +program.getSemester() + ">" +
                    program.getCourseName();
            program.setInfo(info);
            teachProgramService.addNewTeachProgram(program);
        }
        return new FebsResponse().success();
    }

    @ControllerEndpoint(operation = "删除TeachProgram", exceptionMessage = "删除TeachProgram失败")
    @GetMapping("teachProgram/delete")
    @ResponseBody
    @RequiresPermissions("TeachProgram:delete")
    public FebsResponse deleteTeachProgram(TeachProgram teachProgram) {
        this.teachProgramService.deleteTeachProgram(teachProgram);
        return new FebsResponse().success();
    }

    /**
     * 单批次停用
     * @param id
     * @return
     */
    @ControllerEndpoint(operation = "修改TeachProgram", exceptionMessage = "修改TeachProgram失败")
    @GetMapping("remove/{id}")
    @ResponseBody
//    @RequiresPermissions("teachProgram:remove")
    public FebsResponse updateStatus(@PathVariable Integer id) {
        System.out.println(id);
        this.teachProgramService.updateStatus(id);
        return new FebsResponse().success();
    }
    /**
     * 修改批次
     * @param
     * @return
     */
    @ControllerEndpoint(operation = "修改TeachProgram", exceptionMessage = "修改TeachProgram失败")
    @PostMapping("update")
    @ResponseBody
//    @RequiresPermissions("teachProgram:update")
    public FebsResponse updateTeachProgram(@Valid TeachProgram teachProgram) {
        System.out.println(teachProgram.getId());
        System.out.println(teachProgram.getLevelId());
        System.out.println(teachProgram.getBatchId());
        if (teachProgram.getId() == null) {
            throw new FebsException("计划ID为空");
        }

        this.teachProgramService.updateTeachPrograms(teachProgram);

        return new FebsResponse().success();
    }

    @ControllerEndpoint(operation = "修改TeachProgram", exceptionMessage = "导出Excel失败")
    @PostMapping("teachProgram/excel")
    @ResponseBody
    @RequiresPermissions("teachProgram:export")
    public void export(QueryRequest queryRequest, TeachProgram teachProgram, HttpServletResponse response) {
        List<TeachProgram> teachPrograms = this.teachProgramService.findTeachPrograms(queryRequest, teachProgram).getRecords();
        ExcelKit.$Export(TeachProgram.class, response).downXlsx(teachPrograms, false);
    }

	
    /**
     * 学院联动专业
     * @param collegeName
     * @return
     */
    @GetMapping("findCollegeByMajor")
    @ResponseBody
    public FebsResponse findCollegeByMajor(String collegeName){
        List<TeachProgram> collegeByMajor = teachProgramService.findCollegeByMajor(collegeName);
        return new FebsResponse().success().data(collegeByMajor);
    }
}
