package cc.zy.base.businesses.controller;


import cc.zy.base.businesses.entity.Advert;
import cc.zy.base.businesses.entity.AdvertLoop;
import cc.zy.base.businesses.service.IAdvertService;
import cc.zy.base.common.annotation.ControllerEndpoint;
import cc.zy.base.common.controller.BaseController;
import cc.zy.base.common.entity.FebsConstant;
import cc.zy.base.common.entity.FebsResponse;
import cc.zy.base.common.entity.QueryRequest;
import cc.zy.base.common.exception.FebsException;
import cc.zy.base.common.utils.FebsUtil;
import com.wuwenze.poi.ExcelKit;
import lombok.extern.slf4j.Slf4j;
import lombok.RequiredArgsConstructor;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *  Controller
 *
 * @author lijian
 * @date 2021-01-25 16:12:53
 */
@Slf4j
@Validated
@Controller
@RequestMapping("advert")
@RequiredArgsConstructor
public class AdvertController extends BaseController {

    private final IAdvertService advertService;

    @GetMapping(FebsConstant.VIEW_PREFIX + "advert")
    public String advertIndex(){
        return FebsUtil.BusinessView("advert/advert");
    }

    @GetMapping("list")
    @ResponseBody
    @RequiresPermissions("advert:view")
    public FebsResponse advertList(QueryRequest request, Advert advert) {
        Map<String, Object> dataTable = getDataTable(this.advertService.findAdverts(request, advert));
        return new FebsResponse().success().data(dataTable);
    }
    @GetMapping("looplist")
    @ResponseBody
    public FebsResponse advertLoopList(QueryRequest request) {
        Map<String, Object> dataTable = getDataTable(this.advertService.findAdvertLoops(request));
        return new FebsResponse().success().data(dataTable);
    }

    @ControllerEndpoint(operation = "新增广告", exceptionMessage = "新增广告失败")
    @PostMapping("add")
    @ResponseBody
//    @RequiresPermissions("advert:add")
    public FebsResponse addAdvert(@Valid Advert advert,@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:SS") Date time) {

        advert.setCreateTime(time);
      //  System.out.println(advert);
        this.advertService.createAdvert(advert);
        return new FebsResponse().success();
    }

    @ControllerEndpoint(operation = "删除Advert", exceptionMessage = "删除Advert失败")
    @GetMapping("delete/{advertId}")
    @ResponseBody
//    @RequiresPermissions("advert:delete")
    public FebsResponse deleteAdvert(@PathVariable Integer advertId) {
        Advert advertDetailById = advertService.findAdvertDetailById(advertId);
        if(advertDetailById.getStatusId()==1){
            throw new FebsException("该广告未下架，无法删除");
        }
        this.advertService.deleteAdvert(advertId);
        return new FebsResponse().success();
    }

    @ControllerEndpoint(operation = "修改Advert", exceptionMessage = "修改Advert失败")
    @PostMapping("update")
    @ResponseBody
//    @RequiresPermissions("advert:update")loopAdvertSort
    public FebsResponse updateAdvert(@Valid Advert advert) {
        if (advert.getId() == null) {
            throw new FebsException("广告ID为空");
        }
        Advert adById = advertService.findAdvertDetailById(advert.getId());
        if(adById.getStatusId()!=advert.getStatusId() && advert.getStatusId()==1){
            Integer advertLoopId;
            try {
                advertLoopId = advertService.findAdvertLoopId();
            }catch (Exception e){
                throw new FebsException("轮播广告列表无空位");
            }
        }
            this.advertService.updateAdvert(advert);
        return new FebsResponse().success();
    }

    @ControllerEndpoint(operation = "修改广告状态", exceptionMessage = "修改广告状态失败")
    @GetMapping("changeStatus/{advertId}")
    @ResponseBody
//    @RequiresPermissions("advert:update")
    public FebsResponse changeAdvertStatus(@PathVariable Integer advertId) {
        if (advertId == null) {
            throw new FebsException("广告ID为空");
        }
        Advert advertDetail = this.advertService.findAdvertDetailById(advertId);
        if(advertDetail.getStatusId()==1){
            advertService.delAdvrertToAdvertLoop(advertId);
            advertDetail.setStatusId(2);
        }else {
            Integer advertLoopId;
            try {
                advertLoopId = advertService.findAdvertLoopId();
            }catch (Exception e){
                throw new FebsException("轮播广告列表无空位");
            }
            advertService.addAdvertToAdvertLoop(advertId,advertLoopId);
            advertDetail.setStatusId(1);
        }
        this.advertService.updateAdvertStatus(advertDetail);
        return new FebsResponse().success();
    }

    @ControllerEndpoint(operation = "修改轮播广告位置", exceptionMessage = "修改广告位置失败")
    @PostMapping("loopAdvertSort")
    @ResponseBody
//    @RequiresPermissions("advert:update")
    public FebsResponse updateloopAdvertSort(@Valid AdvertLoop advertLoop) {
        if (advertLoop.getId() == null || advertLoop.getAdvertId()==null) {
            throw new FebsException("轮播广告ID为空");
        }
        System.out.println(advertLoop);
        this.advertService.updateAdvertLoopOrder(advertLoop);
        return new FebsResponse().success();
    }

    @ControllerEndpoint(operation = "修改Advert", exceptionMessage = "导出Excel失败")
    @PostMapping("advert/excel")
    @ResponseBody
    @RequiresPermissions("advert:export")
    public void export(QueryRequest queryRequest, Advert advert, HttpServletResponse response) {
        List<Advert> adverts = this.advertService.findAdverts(queryRequest, advert).getRecords();
        ExcelKit.$Export(Advert.class, response).downXlsx(adverts, false);
    }


}
