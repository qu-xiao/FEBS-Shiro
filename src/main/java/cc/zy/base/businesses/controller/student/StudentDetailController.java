package cc.zy.base.businesses.controller.student;

import cc.zy.base.businesses.entity.Batch;
import cc.zy.base.businesses.entity.College;
import cc.zy.base.businesses.entity.Major;
import cc.zy.base.businesses.entity.Student;
import cc.zy.base.businesses.service.IBatchService;
import cc.zy.base.businesses.service.ICollegeService;
import cc.zy.base.businesses.service.IMajorService;
import cc.zy.base.businesses.service.IStudentService;
import cc.zy.base.common.annotation.ControllerEndpoint;
import cc.zy.base.common.controller.BaseController;
import cc.zy.base.common.entity.FebsResponse;
import cc.zy.base.common.exception.FebsException;
import cc.zy.base.system.entity.Role;
import cc.zy.base.system.entity.User;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

import static org.apache.shiro.web.filter.mgt.DefaultFilter.user;

/**
 * Controller
 *
 * @author JiangWeiguang
 * @date 2021\1\22 0022 16:50
 */
@Slf4j
@Validated
@RestController
@RequiredArgsConstructor
@RequestMapping("student")
public class StudentDetailController extends BaseController {
    private final IStudentService studentsService;
    private final ICollegeService collegeService;
    private final IMajorService majorService;
    private final IBatchService batchService;

    @GetMapping("collegeList")
    public FebsResponse getAllCollege(College college) {
        return new FebsResponse().success().data(collegeService.findColleges(college));
    }
    @GetMapping("majorList")
    public FebsResponse getAllMajor(Major major) {
        List<Major> majors = majorService.findMajors(major);
        return new FebsResponse().success().data(majors);
    }

    @GetMapping("batchList")
    public FebsResponse getAllMajor(Batch batch) {
        List<Batch> batches = batchService.findBatchs(batch);
        return new FebsResponse().success().data(batches);
    }

    @PostMapping("updateExamInfo")
    //@RequiresPermissions("studentExamInfo:update")
    @ControllerEndpoint(operation = "修改学生报考信息", exceptionMessage = "修改学生报考信息失败")
    public FebsResponse updateExamInfo(@Valid Student student) {
//        System.out.println(student);
        boolean b = this.studentsService.updateById(student);
        if(b==true)
        return new FebsResponse().success();
        else{
            throw new FebsException("修改学生信息失败");
        }
    }

    @PostMapping("passExamLocation")
    @ControllerEndpoint(operation = "学生考场审核通过", exceptionMessage = "学生考场审核通过报错")
    public  FebsResponse passExamLocation(Integer taskId,Integer stuId,Integer examLocationId){
//        System.out.println("taskId:"+taskId+"stuid:"+stuId+"examLocationId:"+examLocationId+"-------------------------------");
        studentsService.passExamLocation(taskId,stuId,examLocationId);
        return new FebsResponse().success();
    }

    @PostMapping("noPassExamLocation")
    @ControllerEndpoint(operation = "学生考场审核不通过", exceptionMessage = "学生考场审核不通过报错")
    public  FebsResponse noPassExamLocation(Integer taskId,String remark){
        System.out.println("taskId:"+taskId+"remark"+remark);
        studentsService.noPassExamLocation(taskId,remark);
        return new FebsResponse().success();
    }
}
