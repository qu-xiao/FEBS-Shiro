package cc.zy.base.businesses.controller;


import cc.zy.base.businesses.entity.AdvertLog;
import cc.zy.base.businesses.service.IAdvertLogService;
import cc.zy.base.common.annotation.ControllerEndpoint;
import cc.zy.base.common.controller.BaseController;
import cc.zy.base.common.entity.FebsConstant;
import cc.zy.base.common.entity.FebsResponse;
import cc.zy.base.common.entity.QueryRequest;
import cc.zy.base.common.utils.FebsUtil;
import com.wuwenze.poi.ExcelKit;
import lombok.extern.slf4j.Slf4j;
import lombok.RequiredArgsConstructor;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 广告日志表 Controller
 *
 * @author zhaojw
 * @date 2021-01-28 09:46:04
 */
@Slf4j
@Validated
@Controller
@RequiredArgsConstructor
public class AdvertLogController extends BaseController {

    private final IAdvertLogService advertLogService;

    @GetMapping(FebsConstant.VIEW_PREFIX + "advertLog")
    public String advertLogIndex(){
        return FebsUtil.view("advertLog/advertLog");
    }

    @GetMapping("advertLog")
    @ResponseBody
    //@RequiresPermissions("advertLog:list")
    public FebsResponse getAllAdvertLogs(AdvertLog advertLog) {
        return new FebsResponse().success().data(advertLogService.findAdvertLogs(advertLog));
    }

    @GetMapping("advertLog/list")
    @ResponseBody
    //@RequiresPermissions("advertLog:list")
    public FebsResponse advertLogList(QueryRequest request, AdvertLog advertLog) {
        log.info("前台传来的对象为："+advertLog);
        Map<String, Object> dataTable = getDataTable(this.advertLogService.findAdvertLogs(request, advertLog));
        return new FebsResponse().success().data(dataTable);
    }

    /**
     *前台点击广告记录广告日志的方法
     * @param openId 小程序关联用户的id
     * @param advertId 点开广告的id
     * @return
     */
    @ControllerEndpoint(operation = "新增AdvertLog", exceptionMessage = "新增AdvertLog失败")
    @PostMapping("advertLog/add")
    @ResponseBody
    //@RequiresPermissions("advertLog:add")
    public FebsResponse addAdvertLog(@Valid Integer openId,Integer advertId) {
        Integer stuId = advertLogService.findStuIdByOpenId(openId);
        log.info("前台传来的openid为："+openId);
        log.info("前台传来的advertId为："+advertId);
        if(stuId != null && stuId != 0){
            this.advertLogService.insertAdvertLogData(stuId,advertId);
            return new FebsResponse().success();
        }
        return new FebsResponse().fail();

    }


    /**
     * 观看广告退出时执行此方法记录退出时间
     * @param openId 用户关联小程序的id
     *        advertId 退出的广告id
     * @return
     */
    @ControllerEndpoint(operation = "修改AdvertLog", exceptionMessage = "修改AdvertLog失败")
    @PostMapping("advertLog/update")
    @ResponseBody
    //@RequiresPermissions("advertLog:update")
    public FebsResponse updateAdvertLog(Integer openId,Integer advertId) {
        log.info("前台传来的openid为："+openId);
        log.info("前台传来的advertId为："+advertId);
        Integer stuId = advertLogService.findStuIdByOpenId(openId);

        this.advertLogService.updateAdvertLog(stuId,advertId);
        return new FebsResponse().success();
    }


    @ControllerEndpoint(operation = "修改AdvertLog", exceptionMessage = "导出Excel失败")
    @PostMapping("advertLog/excel")
    @ResponseBody
    @RequiresPermissions("advertLog:export")
    public void export(QueryRequest queryRequest, AdvertLog advertLog, HttpServletResponse response) {
        List<AdvertLog> advertLogs = this.advertLogService.findAdvertLogs(queryRequest, advertLog).getRecords();
        ExcelKit.$Export(AdvertLog.class, response).downXlsx(advertLogs, false);
    }
}
