package cc.zy.base.businesses.mapper;

import cc.zy.base.businesses.entity.AdvertLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

/**
 * 广告日志表 Mapper
 *
 * @author zhaojw
 * @date 2021-01-28 09:46:04
 */
public interface AdvertLogMapper extends BaseMapper<AdvertLog> {
    <T> IPage<AdvertLog> findAdvertLogNew(@Param("page") Page<T> page,@Param("advertLog") AdvertLog advertLog);

    Long countAdvertLog(AdvertLog advertLog);

    void insertAdvertLogData(@Param("userId") Integer userId,@Param("advertId") Integer advertId);

    /**
     * 根据openid查stuid
     */
    Integer findStuIdByOpenId(@Param("openId") Integer openId);

    void updateExitTime(@Param("userId") Integer userId,@Param("advertId") Integer advertId);

}
