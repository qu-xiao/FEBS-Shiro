package cc.zy.base.businesses.mapper;

import cc.zy.base.businesses.entity.StudentPool;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 *  Mapper
 *
 * @author Jiangjinlin
 * @date 2021-01-25 10:55:03
 */
public interface StudentPoolMapper extends BaseMapper<StudentPool> {

    /**
     * 添加学生信息到公海
     *
     * @param list 添加的学生集合
     * @return 添加成功数量
     */
    public  int   AddStudentPoolList(@Param("list") List<StudentPool> studentPools);
    /**
     * 判断学生信息是否重复
     *
     * @param studentPool 学生对象
     * @return 重复的对象
     */

    StudentPool  ifStudentPool(StudentPool studentPool);
}
