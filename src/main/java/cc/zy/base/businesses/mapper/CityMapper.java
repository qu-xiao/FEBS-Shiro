package cc.zy.base.businesses.mapper;

import cc.zy.base.businesses.entity.City;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 *  Mapper
 *
 * @author Jiangjinlin
 * @date 2021-01-26 11:50:10
 */
public interface CityMapper extends BaseMapper<City> {
    /**
     * 根据省份查询城市情况
     * @Param pid 省份id
     */
    List<City> findCityByPid(@Param("pid") String pid);

    City findCityByid(@Param("id")Integer id);

    List<City> findAllCity();

}
