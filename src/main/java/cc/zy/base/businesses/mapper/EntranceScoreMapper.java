package cc.zy.base.businesses.mapper;

import cc.zy.base.businesses.entity.EntranceScore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 *  Mapper
 *[sdwdwrqrwerqwe rwrewqwrwr w rrwqr wefddw wrwqre qq w qqqwA"A
 *
 * @author Jiangjinlin
 * @date 2021-01-23 23:11:18
 */
public interface EntranceScoreMapper extends BaseMapper<EntranceScore> {
    String getSequence();
    String getAllSequence();
    //列表查询
    <T> IPage<EntranceScore> findList(@Param("page") Page<EntranceScore> page, @Param("sql") String sql, @Param("entranceScore") EntranceScore entranceScore);
    //详情查询

    Long countEntranceScoreDetail(@Param("entranceScore") EntranceScore entranceScore);

    //获取所有有效批次
    List<EntranceScore> getBatchs();

    /**
     * 通过批次Id获取院校名称
     * @return
     */
    List<EntranceScore> getCollegesByBatchId(Integer batchId);

    /**
     * 获取学生所考科目总成绩
     * @param entranceScore
     * @return
     */
    Integer getStuTotalScore(@Param("entranceScore") EntranceScore entranceScore);

    /**
     * 增加入学成绩数据
     * @param entranceScore
     * @param score
     * @return
     */
    int insertEntrance(@Param("entranceScore") EntranceScore entranceScore,@Param("score") String score);

    /**
     * 检查数据库是否有同批次，同一个人
     * @param identity
     * @param batchId
     * @return
     */
    List<EntranceScore> checkUnique(@Param("identity")String identity,@Param("batchName")String batchName);
}
