package cc.zy.base.businesses.mapper;

import cc.zy.base.businesses.entity.College;
import cc.zy.base.businesses.entity.Seal;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

/**
 *  Mapper
 *
 * @author Jiangjinlin
 * @date 2021-01-27 09:56:39
 */
public interface SealMapper extends BaseMapper<Seal> {


    <T> IPage<College> findSealDetailPage(Page<T> page, @Param("college") College college);

    long countSealDetail(@Param("college") College college);

    /**
     * 根据院校id查询当前院校和印章信息
     * @param id
     * @return
     */
    College findById(Integer id);

    void updateSealImgurlById(Integer collegeId);
}
