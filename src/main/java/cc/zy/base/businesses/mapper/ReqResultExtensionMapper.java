package cc.zy.base.businesses.mapper;

import cc.zy.base.businesses.entity.ReqResultExtension;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 *  Mapper
 *
 * @author Jiangjinlin
 * @date 2021-01-25 16:06:07
 */
public interface ReqResultExtensionMapper extends BaseMapper<ReqResultExtension> {

}
