package cc.zy.base.businesses.mapper;

import cc.zy.base.businesses.entity.TeacherChangeLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

/**
 * 班主任变更记录表 Mapper
 *
 * @author LiPeng
 * @date 2021-01-27 16:44:35
 */
public interface TeacherChangeLogMapper extends BaseMapper<TeacherChangeLog> {

    /**
     * 查：根据班级id查询班主任更换记录
     *
     * @param page
     * @param classId
     * @param <T>
     * @return
     */
    <T> IPage<TeacherChangeLog> selectTeacherChangeLogByClassId(Page<T> page, @Param("classId") Integer classId);

    /**
     * 查：根据班级id查询班主任更换记录总数
     *
     * @param classId
     * @return
     */
    long countTeacherChangeLog(@Param("classId") Integer classId);
}
