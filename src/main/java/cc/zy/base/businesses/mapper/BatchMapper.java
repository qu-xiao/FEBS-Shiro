package cc.zy.base.businesses.mapper;

import cc.zy.base.businesses.entity.Batch;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

public interface BatchMapper extends BaseMapper<Batch> {

   public List<Batch> findBatchs();

   public Batch findBatchsById(Integer id);

   /**
     * 查：查询所有有效批次
     *
     * @return
     */
    List<Batch> selectBatchForSelect();
}
