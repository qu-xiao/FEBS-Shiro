package cc.zy.base.businesses.mapper;

import cc.zy.base.businesses.entity.Post;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 *  Mapper
 *
 * @author Jiangjinlin
 * @date 2021-01-22 12:18:04
 */
public interface PostMapper extends BaseMapper<Post> {

}
