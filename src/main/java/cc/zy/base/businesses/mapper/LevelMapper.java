package cc.zy.base.businesses.mapper;

import cc.zy.base.businesses.entity.Level;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 *  Mapper
 *
 * @author Jiangjinlin
 * @date 2021-01-26 10:43:23
 */
public interface LevelMapper extends BaseMapper<Level> {

    List<Level> getLevelList();

	/**
     *
     *
     * @param collegeId
     * @return
     */
    List<Level> selectLevelForSelect(@Param("collegeId") Integer collegeId);
}
