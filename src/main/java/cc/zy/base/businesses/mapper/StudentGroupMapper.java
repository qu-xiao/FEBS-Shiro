package cc.zy.base.businesses.mapper;

import cc.zy.base.businesses.entity.StudentGroup;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

/**
 * 学生组表	 Mapper
 *
 * @author LiPeng
 * @date 2021-01-26 16:24:15
 */
public interface StudentGroupMapper extends BaseMapper<StudentGroup> {

    /**
     * 查找学生分组详细信息
     * @param page 分页对象
     * @param studentGroup 学生分组对象，用于传递查询条件
     * @return IPage
     */
    <T> IPage<StudentGroup> findStudentGroupDetailPage(Page<T> page, @Param("studentGroup") StudentGroup studentGroup);

    long countStudentGroupDetail(@Param("studentGroup") StudentGroup studentGroup);
}
