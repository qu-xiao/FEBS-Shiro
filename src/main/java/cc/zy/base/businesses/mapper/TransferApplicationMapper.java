package cc.zy.base.businesses.mapper;

import cc.zy.base.businesses.entity.TransferApplication;
import cc.zy.base.businesses.entity.TransferApplicationVo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 *  Mapper
 *
 * @author Liuheng
 * @date 2021-01-18 10:51:13
 */
public interface TransferApplicationMapper extends BaseMapper<TransferApplication> {

    /**
     * 查找用户详细信息
     *
     * @param page 分页对象
     * @param transferApplication 用户对象，用于传递查询条件
     * @return Ipage
     */
    <T> IPage<TransferApplication> findTransferApplicationPage(Page<T> page, @Param("transferApplication") TransferApplication transferApplication);
    TransferApplicationVo findTransferDetailById(@Param("id") Integer id);
    long countTransferApplication(@Param("transferApplication") TransferApplication transferApplication);
    String findBackCollegeByTransferApplicationId(@Param("id") Integer id);
    Integer findSuspensionCollegeByTransferApplicationId(@Param("id") Integer id);
    String findStudyTypeChangeByTransferApplicationId(@Param("id") Integer id);
    String findMajorChangeByTransferApplicationId(@Param("id") Integer id);
    List<String> findTransferAttachment(@Param("id") Integer id);


}
