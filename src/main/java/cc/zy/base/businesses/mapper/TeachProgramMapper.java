package cc.zy.base.businesses.mapper;

import cc.zy.base.businesses.entity.Batch;
import cc.zy.base.businesses.entity.Level;
import cc.zy.base.businesses.entity.TeachProgram;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

/**
 *  Mapper
 *
 * @author pzj
 * @date 2021-01-25 09:38:13
 */
public interface TeachProgramMapper extends BaseMapper<TeachProgram> {
    /**
     * 找到对应的层次id的名称
     * @param collegeId
     * @return
     */
    public List<TeachProgram> findLevelName(Integer collegeId);

    public List<TeachProgram> getMajoName(Integer collegeId);
    /**
     * @param page 分页对象
     * @param teachProgram 计划对象，用于传递查询条件
     * @return Ipage
     */
    <T> IPage<TeachProgram> findTeachProgramDetailPage(@Param("page") Page<T> page, @Param("teachProgram") TeachProgram teachProgram);

    long countTeachProgramDetail(@Param("teachProgram") TeachProgram teachProgram);

    /**
     * 查找计划详细信息
     *
     * @param teachProgram 计划对象，用于传递查询条件
     * @return List<TeachProgram>
     */
    List<TeachProgram> findTeachProgramDetail(@Param("teachProgram") TeachProgram teachProgram);

    /**
     * 单批次停用
     * @param id
     */
    void updateStatus(@Param("id") Integer id);

    /**
     * 通过ID查找批次
     *
     * @param  id
     * @return 批次
     */
    TeachProgram findById(@Param("id") Integer id);

    /**
     * 修改批次
     * @param teachProgram
     * @return
     */
    int updateTeachPrograms(@Param("teachProgram")TeachProgram teachProgram);

    /**
     * 拼接info信息
     * @param id
     */
    void updateInfo(@Param("id")Integer id,@Param("info") String info);

    /**
     * 批量新增教学计划
     * @param teachProgram
     * @return
     */
    int addNewTeachProgram(@Param("teachProgram")TeachProgram teachProgram);

    /**
     *
     */
    Batch selectBatch(@Param("batchName")String batchName);

    /**
     * 根据batchId查教学计划
     * @param batchId
     * @return
     */
    List<TeachProgram> selectBatchByBatchId(@Param("batchId")Integer batchId);

    /**
     * 根据batchName查教学计划
     * @param batchName
     * @return
     */
    List<TeachProgram> selectBatchByBatchName(@Param("batchName")String batchName);


    /**
     * 修改教学计划中的批次
     * @param batchId
     */
    void updateBatchId(@Param("batchId") Integer batchId);

    /**
     * 层次下拉框
     * @param level
     * @return
     */
    Level selectLevel(@Param("level")Level level);

    List<Batch> selectMoreBatch(@Param("batchName") String batchName);
    List<Batch> selectAllBatch(@Param("batch") Batch batch);

	/**
     * 学院联动专业
     * @param collegeName
     * @return
     */
    List<TeachProgram> findCollegeByMajor(String collegeName);
}
