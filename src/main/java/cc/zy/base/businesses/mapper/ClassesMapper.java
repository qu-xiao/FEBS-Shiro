package cc.zy.base.businesses.mapper;

import cc.zy.base.businesses.entity.Classes;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

/**
 * 班级表 Mapper
 *
 * @author Jiangjinlin
 * @date 2021-01-25 19:29:08
 */
public interface ClassesMapper extends BaseMapper<Classes> {
	/**
     * 查：班级列表
     * @param page 分页对象
     * @param classes 班级对象，用于传递查询条件
     * @return IPage
     */
    <T> IPage<Classes> findClassesDetailPage(Page<T> page, @Param("classes") Classes classes);

    long countClassesDetail(@Param("classes") Classes classes);

    /**
     * 通过ID查找班级
     *
     * @param classesId id
     * @return 班级
     */
    Classes findById(Integer classesId);

    /**
     * 查：根据班级id查询班级
     *
     * @param classesId
     * @return
     */
    Classes selectClassesById(@Param("classesId") Integer classesId);
}
