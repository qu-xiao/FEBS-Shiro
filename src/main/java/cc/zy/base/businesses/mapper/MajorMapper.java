package cc.zy.base.businesses.mapper;

import cc.zy.base.businesses.entity.Major;
import cc.zy.base.businesses.entity.SubjectCategory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 专业表 Mapper
 *
 * @author Jiangjinlin
 * @date 2021-01-26 19:41:01
 */
public interface MajorMapper extends BaseMapper<Major> {

    /**
     *
     * 院校专业详情信息
     * @param page 分页对象
     * @param Major 用户对象，用于传递查询条件
     * @param <T>
     * @return
     */
    <T> IPage<Major> findMajorDetailPage(Page<T> page, @Param("major") Major major);

    long countMajorDetail(@Param("major") Major major);

    /**
     * 通过ID查找专业类别
     *
     * @param  majorId
     * @return
     */
    Major findById(Integer majorId);

	/**
     * 查：根据院校id和层次id获取专业信息
     *
     * @param levelId
     * @return
     */
    List<Major> selectMajorForSelect(@Param("collegeId") Integer collegeId, @Param("levelId") Integer levelId);

}
