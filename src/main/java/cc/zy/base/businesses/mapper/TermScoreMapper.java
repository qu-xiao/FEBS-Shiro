package cc.zy.base.businesses.mapper;

import cc.zy.base.businesses.entity.*;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 *  Mapper
 *
 * @author wangpin
 * @date 2021-01-28 16:13:11
 */
public interface TermScoreMapper extends BaseMapper<TermScore> {
    /**
     * 查询列表
     * @author wangpin
     * @param termScore
     * @return
     */
    IPage<TermScore> getTermScoreList(@Param("page") Page<TermScore> page, @Param("termScore")TermScore termScore);

    Long countTermScoreDetail(@Param("termScore")TermScore termScore);

    /**
     * 通过学校id获取层次名称
     * @param collegeId
     * @return
     */
    List<Level> getLevelListByCollegeId(@Param("collegeId") Integer collegeId);

    /**
     * 通过学校和层次获取对应专业
     * @param collegeId
     * @param levelId
     * @return
     */
    List<Major> getMajorListByIds(@Param("collegeId")Integer collegeId,@Param("levelId")Integer levelId);

    /**
     * 通过学校和层次及专业获取对应课程
     * @param collegeId
     * @param levelId
     * @param majorId
     * @return
     */
    List<TeachProgram> getCourseNameByIds(@Param("collegeId")Integer collegeId,@Param("levelId")Integer levelId,@Param("majorId")Integer majorId,@Param("termId")Integer termId);

    /**
     * 根据层次id获取对应学期
     * @param levelId
     * @return
     */
    List<CollegeTerm> getTermsByLevelId(@Param("levelId")Integer levelId);
}
