package cc.zy.base.businesses.mapper;



import cc.zy.base.businesses.entity.Notice;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

/**
 * 通知表 Mapper
 *
 * @author Jiangjinlin
 * @date 2021-01-26 11:52:14
 */
public interface NoticeMapper extends BaseMapper<Notice> {
    /**
     * 查找用户详细信息
     *
     * @param page 分页对象
     * @param notice 用户对象，用于传递查询条件
     * @return Ipage
     */
    <T> IPage<Notice> findNoticeDetailPage(Page<T> page, @Param("notice") Notice notice);

    /**
     * 查询通知总数
     * @param notice
     * @return
     */
    long countNoticeDetail(@Param("notice") Notice notice);


}
