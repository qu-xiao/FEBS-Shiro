package cc.zy.base.businesses.mapper;

import cc.zy.base.businesses.entity.CType;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Date;
import java.util.List;

/**
 *  Mapper
 *
 * @author Jiangjinlin
 * @date 2021-01-25 10:44:35
 */
public interface CTypeMapper extends BaseMapper<CType> {
    /**
     * 查找类别详细信息
     *
     * @param CType 类别对象，用于传递查询条件
     * @return List<College>
     */
    <T> IPage<CType> findCTypeDetail(Page<T> page, @Param("cType") CType cType);

    long countCTypeDetail(@Param("cType") CType cType);
    int updateByIdStatus(@Param("id") int id,@Param("updateTime") Date updateTime,@Param("updateUserId")int updateUserId);
    int updateByIdStatusSon(@Param("id") int id,@Param("updateTime") Date updateTime,@Param("updateUserId")int updateUserId);
    int updateByIdStatusUp(@Param("id") int id,@Param("updateTime") Date updateTime,@Param("updateUserId")int updateUserId);
    int findPid(@Param("id" )int id);
     int updateByIdStatusSonDown(@Param("id") int id,@Param("updateTime") Date updateTime,@Param("updateUserId")int updateUserId);
    Integer selectByIdPid(@Param("sort1") int sort1,@Param("sort2") int sort2);
     int  updateByIdSort(@Param("id") int id,@Param("sort2") int sort2 );
    CType findById(@Param("id") int id);
    int  findStatus(@Param("id") int id);
    /**
     * 查找sort1类别的最大值
     *
     * @return List<College>
     */
    int findCTypeSORT1MAX();
    /**
     * 查找sort1类别的当前值
     *
     * @return List<College>
     */
    int  findCTypeSort1Now(@Param("ctypeId") int ctypeId);

    /**
     * 查找sort2类别的最大值
     *
     * @return List<College>
     */
    int findCTypeSORT2MAX(@Param("ctypeSort1Id") int ctypeSort1Id);

    /**
     * 通过ID查找类别
     *
     * @param id id
     * @return 院校
     */
    CType findById(@Param("cTypeId") Integer cTypeId);
    /**
     *查找所有一级分类
     * @return
     */
    List<CType> selectCTypeList1();
}
