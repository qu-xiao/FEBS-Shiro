package cc.zy.base.businesses.mapper;

import cc.zy.base.businesses.entity.Task;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 *  Mapper
 *
 * @author zzz
 * @date 2021-01-28 17:29:15
 */
public interface TaskMapper extends BaseMapper<Task> {

   int findTotalTaskByStuId(@Param("userId")Integer userId);
}
