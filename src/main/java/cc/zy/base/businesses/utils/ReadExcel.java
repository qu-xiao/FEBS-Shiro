package cc.zy.base.businesses.utils;

import cc.zy.base.businesses.entity.EntranceScore;
import cc.zy.base.businesses.service.IEntranceScoreService;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class ReadExcel {

    public static List<EntranceScore> readData(IEntranceScoreService iEntranceScoreService) throws NoSuchFieldException, IllegalAccessException {
        File xlsFile = new File("H:/321.xlsx");
        List<EntranceScore> entranceScores = new ArrayList<>();
        // 工作表
        Workbook workbook = null;
        try {
            workbook = WorkbookFactory.create(xlsFile);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        }
        // 表个数。
        int numberOfSheets = workbook.getNumberOfSheets();
        // 遍历表。
        for (int i1 = 0; i1 < numberOfSheets; i1++) {
            Sheet sheet = workbook.getSheetAt(i1);
            // Excel第一行。
            Row temp = sheet.getRow(0);
            if (temp == null) {
                continue;
            }
            // 读数据。
            //4、循环读取表格数据
            for (Row row : sheet) {
                //首行（即表头）不读取
                if (row.getRowNum() == 0) {
                    continue;
                }
                EntranceScore score = new EntranceScore();
                //读取当前行中单元格数据，索引从0开始
                String levelName = row.getCell(4).getStringCellValue();
                String subtypeName = row.getCell(5).getStringCellValue();
                //设置课程
                    List<EntranceScore> aliasAndNum = iEntranceScoreService.getSubjectsAliasAndNum(levelName, subtypeName);
                    Class aClass = score.getClass();
                    for (EntranceScore entranceScore : aliasAndNum) {
                        Field field = aClass.getDeclaredField(entranceScore.getAlias());
                        field.setAccessible(true);
                        field.set(score, row.getCell(entranceScore.getNum()).getStringCellValue());
                    }

                String batchName = row.getCell(0).getStringCellValue();
                String stuName = row.getCell(1).getStringCellValue();
                String identity = row.getCell(2).getStringCellValue();
                String collegeName = row.getCell(3).getStringCellValue();
                score.setIdentity(identity);
                score.setStuName(stuName);
                score.setBatchName(batchName);
                score.setCollegeName(collegeName);
                score.setLevelName(levelName);
                score.setFullName(subtypeName);
                score.setFullName(subtypeName);
                List<EntranceScore> entranceIds = iEntranceScoreService.getEntranceIds(levelName, subtypeName, batchName);
                System.out.println(entranceIds);
                for (EntranceScore entranceId : entranceIds) {
                    entranceId.setIdentity(identity);
                    Class aClass1 = entranceId.getClass();
                    Field field = aClass.getDeclaredField(entranceId.getAlias());
                    field.setAccessible(true);
                    String sc =(String) field.get(score);
                    //先判断数据库是否有同批次的同一个人
                    List<EntranceScore> list = iEntranceScoreService.checkUnique(identity, batchName);
                    if (list==null){
                        int i = iEntranceScoreService.insertEntrance(entranceId, sc);
                        System.out.println("添加成功");
                    }else {
                        System.out.println("添加失败");
                    }

                }
                entranceScores.add(score);
            }
            //5、关闭流
            try {
                workbook.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
//        System.out.println(entranceScores);
        return entranceScores;
    }
}
